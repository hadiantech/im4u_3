<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProjectReward extends Model
{
    use LogsActivity;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $logName = 'projects';
    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $fillable = [
        'detail'
    ];
}
