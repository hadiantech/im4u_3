<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Permission  extends \Spatie\Permission\Models\Permission
{
    public static function defaultPermissions()
    {
        return [

            'view_users',
            'add_users',
            'edit_users',
            'delete_users',

            'view_roles',
            'add_roles',
            'edit_roles',
            'delete_roles',

            'view_posts',
            'add_posts',
            'edit_posts',
            'delete_posts',

            'view_categories',
            'add_categories',
            'edit_categories',
            'delete_categories',

            'view_projects',
            'add_projects',
            'edit_projects',
            'delete_projects',

            'view_banners',
            'add_banners',
            'edit_banners',
            'delete_banners',

            'view_journals',
            'add_journals',
            'edit_journals',
            'delete_journals',

            'view_presses',
            'add_presses',
            'edit_presses',
            'delete_presses',

            'edit_journal_theme',

            'view_dashboard',
        ];
    }
}
