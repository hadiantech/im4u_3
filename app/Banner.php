<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //for soft delete

use Spatie\Activitylog\Traits\LogsActivity; //for log

use Spatie\MediaLibrary\HasMedia\HasMediaTrait; //for image
use Spatie\MediaLibrary\HasMedia\HasMedia; //for image

class Banner extends Model implements HasMedia //for image
{
    use LogsActivity; //for log
    use SoftDeletes; //for soft delete
    use HasMediaTrait; //for image

    protected $dates = ['deleted_at']; //for soft delete
//    protected $fillable = [
//        'order', 'hidden'
//    ]; //control request
    protected $guarded = []; //allow all request accepted
    protected static $logFillable = true; //for log

}
