<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class Press extends Model implements HasMedia
{
    use LogsActivity;
    use SoftDeletes;
    use HasMediaTrait;

    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected static $logFillable = true;

    public function scopeShow($query){
        return $query->where('hidden', 0);
    }
}
