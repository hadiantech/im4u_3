<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use LogsActivity;    
    use SoftDeletes;

    protected static $logAttributes = ['name'];
    protected static $logOnlyDirty = true;
    protected static $logName = 'categories';
    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $fillable = [
        'name',
    ];

    protected $dates = ['deleted_at'];


    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
