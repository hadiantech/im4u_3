<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Organisation extends Model
{
    use LogsActivity;    
    use SoftDeletes;

    protected static $logAttributes = ['name', 'description', 'website', 'incharge_name', 'incharge_phone', 'incharge_email'];
    protected static $logOnlyDirty = true;
    protected static $logName = 'organisations';
    protected static $ignoreChangedAttributes = ['updated_at'];

    protected $fillable = [
        'name', 'description', 'website', 'incharge_name', 'incharge_phone', 'incharge_email'
    ];

    protected $dates = ['deleted_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
