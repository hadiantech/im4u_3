<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; //for soft delete

use Spatie\Activitylog\Traits\LogsActivity; //for log

use Spatie\MediaLibrary\HasMedia\HasMediaTrait; //for image
use Spatie\MediaLibrary\HasMedia\HasMedia; //for image

use Carbon\Carbon;


class Journal extends Model implements HasMedia //for image
{
    use LogsActivity; //for log
    use SoftDeletes; //for soft delete
    use HasMediaTrait; //for image

    protected $dates = ['deleted_at']; //for soft delete
    protected $guarded = []; //allow all request accepted
    protected static $logFillable = true; //for log

    public function scopeCurrent($query){
        return $query->whereBetween('released_at', [Carbon::today()->startOfMonth(), Carbon::today()]);
    }
    
    public function scopeShow($query){
        return $query->where('hidden', 0);
    }

    public function scopeFeatured($query){
        return $query->where('featured', 1);
    }

    public function scopeArchive($query){
        return $query->where('released_at', '<', Carbon::today()->startOfMonth());
    }

}
