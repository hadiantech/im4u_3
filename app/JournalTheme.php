<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity; //for log

class JournalTheme extends Model
{
    use LogsActivity; //for log

    protected $guarded = []; //allow all request accepted
    protected static $logFillable = true; //for log
}
