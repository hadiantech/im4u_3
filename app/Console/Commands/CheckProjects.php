<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Project;
use Carbon\Carbon;

class CheckProjects extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check all projects that published.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //tukar status project yg dah lepas ended_at kepada 'ended'

        Project::where('status', 'published')->whereDate('ended_at', '<=', Carbon::today()->toDateString())
            ->update(['status' => 'ended']);
    }
}
