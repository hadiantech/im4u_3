<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\User;
use App\Category;
use App\Project;
use App\Press;

use Auth;

class DatatablesController extends Controller
{
    public function getUser(){
        return Datatables::eloquent(User::with('roles'))
        ->addColumn('action', function($model){
            $url = 'users';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addColumn('roles', function($model){
            return $model->roles->map(function($roles) {
                return $roles->name;
            })->implode(', ');
        })
        ->addIndexColumn()
        ->make(true);
    }
    
    public function getJoined($project){
        return Datatables::eloquent(User::whereHas('projects',function($q) use ($project){
            $q->where ('id', $project);
        }))
            ->addIndexColumn()
            ->make(true);
    }

    public function getCategories(){
        return Datatables::eloquent(Category::query())
        ->addColumn('action', function($model){
            $url = 'categories';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getPresses(){
        $query = Press::query();

        return Datatables::eloquent($query)
            ->addColumn('hidden_check', function($query) {
                    return view('shared._check', ['check' => $query->hidden]);
                })
            ->addColumn('highlight_check', function($query) {
                    return view('shared._check', ['check' => $query->highlight]);
                })
            ->addColumn('action', function($model){
                $url = 'presses';
                $show_hide = true;
                return view('shared._table_action', compact('model', 'url', 'show_hide'))->render();
            })
            ->addIndexColumn()
            ->escapeColumns([])
            ->make(true);
    }

    public function getProjects(Datatables $datatables, Request $request){

        $query = Project::query();
        if($request->status){
            $query->where('status', $request->status);
        }

        return Datatables::eloquent($query)
        ->addColumn('action', function($model){
            $url = 'projects';
            return view('shared._table_action', compact('model', 'url'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }

    public function getMyProjects(Datatables $datatables, Request $request){

        $query = Project::where('user_id', Auth::user()->id);
        if($request->status){
            $query->where('status', $request->status);
        }
        
        return Datatables::eloquent($query)
        ->addColumn('action', function($model){
            $url = 'projects';
            $joined = false;
            return view('projects.me.action', compact('model', 'url', 'joined'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }


    public function getMyJoinedProjects(Datatables $datatables, Request $request){
        $user = Auth::user();

        $query = Datatables::eloquent(Project::whereHas('volunteers',function($q) use ($user){
            $q->where ('user_id', $user->id);
        }));

        if($request->status){
            $query->where('status', $request->status);
        }

        return Datatables::eloquent($query)
        ->addColumn('action', function($model){
            $url = 'projects';
            $joined = true;
            return view('projects.me.action', compact('model', 'url', 'joined'))->render();
        })
        ->addIndexColumn()
        ->make(true);
    }
}
