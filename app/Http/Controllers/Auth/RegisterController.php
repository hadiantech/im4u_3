<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Jobs\SendVerifyEmail;
use App\Jobs\SendVerifySMS;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/sms/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'identification_type' => 'required|string',
            'identification_number' => 'required|string',
            'marital_status' => 'required|string',
            'phone_number' => 'required|string',
            'state' => 'required|string',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'identification_type' => $data['identification_type'],
            'identification_number' => $data['identification_number'],
            'marital_status' => $data['marital_status'],
            'phone_number' => $data['phone_number'],
            'state' => $data['state'],
        ]);


        $sms_code = strtoupper(str_random(5));

        $user->verifications()->create([
            'name' => 'sms',
            'code' => $sms_code,
        ]);

        //send sms code
        SendVerifySMS::dispatch($user);

        $code = $user->id.str_random(40);

        $user->verifications()->create([
            'name' => 'email',
            'code' => $code,
        ]);

        SendVerifyEmail::dispatch($user);

        return $user;
    }
}