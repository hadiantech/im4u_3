<?php

namespace App\Http\Controllers;
use App\User;
use App\Project;
use App\Banner;
use App\Journal;
use App\JournalTheme;
use App\Verification;
use App\Press;
use Carbon\Carbon;
use Auth;
use Spatie\Activitylog\Models\Activity;
use App\Jobs\SendVerifyEmail;
use App\Jobs\SendVerifySMS;
use App\Jobs\SendSubscriberEmail;
use App\Jobs\SendProjectCreatedEmail;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Promise\Promise;
use GuzzleHttp\Client;
use Alaouy\Youtube\Facades\Youtube;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $client = new Client();
        $response = $client->request('GET', 'http://more.im4u.my/wp-json/wp/v2/posts?filter[posts_per_page]=5&_embed');
        $blogs = json_decode($response->getBody());

        $youtube = Youtube::listChannelVideos('UCdzKT8BJn225_0UdGt1NaeA', 6);
        // $instagram = new Instagram(config('services.instagram.access-token'));
        $banners = Banner::where('hidden', 0)->orderBy('order')->get();
        $projects = Project::upcoming()->where('status', 'published')->get();

        return view('home', compact('banners', 'projects', 'instagram', 'youtube', 'blogs'));
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    public function resend_email(){
        SendVerifyEmail::dispatch(Auth::user());
        flash()->success('New email sent. ');
        return redirect()->route('profile');
    }

    public function about(){
        return view('about');
    }

    public function dreamfund(){
        return view('dreamfund');
    }

    public function happening(){
        return view('happening');
    }

    public function media(){
        $presses = Press::where('highlight', 1)->get();
        return view('media', compact('presses'));
    }

    public function media_press(){
        return view('press_archive');
    }
    
    public function photos(){
        return view('photos');
    }
    
    public function videos(){
        return view('videos');
    }

    public function verify($code){
        $verifyUser = Verification::where('code', $code)->where('name', 'email')->first();
        if(isset($verifyUser) ){
            if($verifyUser->verified == false) {
                $verifyUser->verified = true;
                $verifyUser->save();
                $status = "Your e-mail is verified. You can now login.";
            }else{
                $status = "Your e-mail is already verified. You can now login.";
            }
        }else{
            $status = "Sorry your email cannot be identified.";
        }

        return view('auth.verify', compact('status'));
    }

    public function fill(){
        $user = Auth::user();
        return view('auth.fill', compact('user'));
    }

    public function auth_update(Request $request){
        $this->validate($request, [
            'identification_type' => 'required|string',
            'identification_number' => 'required|string',
            'marital_status' => 'required|string',
            'phone_number' => 'required|string',
            'state' => 'required|string',
        ]);

        $user = Auth::user();
        $user->identification_type = $request->identification_type;
        $user->identification_number = $request->identification_number;
        $user->marital_status = $request->marital_status;
        $user->phone_number = $request->phone_number;
        $user->state = $request->state;
        $user->save();

        $sms_code = strtoupper(str_random(5));

        $user->verifications()->create([
            'name' => 'sms',
            'code' => $sms_code,
        ]);

        //send sms code
        SendVerifySMS::dispatch($user);

        flash()->success('Profile has been updated.');
        return redirect()->route('profile');
    }

    public function verify_sms(){
        $user = Auth::user();
        return view('auth.sms', compact('user'));
    }

    public function resend_code(){
        SendVerifySMS::dispatch(Auth::user());

        flash()->success('New sms code sent.');
        return redirect()->route('sms.confirm');
    }

    public function confirm_sms(Request $request){
        $check = Verification::where('verified',0)->where('user_id', Auth::user()->id)->where('code', $request->code)->first();

        if($check){
            $check->verified = 1;
            $check->save();
            flash()->success('Phone verified');
            return redirect()->route('profile');
        }else{
            flash()->error('Code not found');
            return redirect()->route('sms.confirm');
        }
    }

    public function phone_change(){
        $user = Auth::user();
        return view('auth.phone', compact('user'));
    }

    public function phone_update(Request $request){
        $this->validate($request, [
            'phone_number' => 'required|string',
        ]);

        $user = Auth::user();
        $user->phone_number = $request->phone_number;
        $user->save();

        $user->verifications()->where('name','sms')->delete();

        $sms_code = strtoupper(str_random(5));

        $user->verifications()->create([
            'name' => 'sms',
            'code' => $sms_code,
        ]);

        SendVerifySMS::dispatch($user);

        flash()->success('Phone has been updated.');
        return redirect()->route('profile');
    }

    public function journal()
    {
        $theme = JournalTheme::first();
        $featured = Journal::show()->current()->featured()->first();
        $journals = Journal::show()->current()->orderBy('released_at', 'desc')->get();
        $archieves = Journal::show()->archive()->orderBy('released_at', 'desc')->get();
        return view('journal', compact('journals', 'featured', 'archieves', 'theme'));
    }

    public function journal_archive(Request $request, $year = null, $month = null){
        $year_data = [];
        for($i = 2017; $i <= Carbon::now()->format('Y'); $i++){
            $year_data[$i] = $i;
        }

        if(!$year){
            $year = Carbon::now()->year;
        }

        if(!$month){
            $month = Carbon::now()->month;
        }

        if($month < 4){
            $month = 1;
        }else if($month < 7){
            $month = 4;
        }else if($month < 10){
            $month = 7;
        }else{
            $month = 10;
        }

        return view('journal_archive', compact('year', 'month', 'year_data'));
    }

    public function journal_detail(){
       
        return view('journal_detail');
    }

    public function send(Project $project){
        SendSubscriberEmail::dispatch($project);
    }

    public function email_test(Project $project){
        SendProjectCreatedEmail::dispatch($project);
    }

    public function contactus(){
        return view('contactus');
    }

    public function partnership(){
        return view('partnership');
    }

    public function getinvolved(){
        return view('getinvolved');
    }

    public function test(){
        
    }
}
