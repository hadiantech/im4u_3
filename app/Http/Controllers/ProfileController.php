<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Verification;
use App\Category;
use Auth;
use Spatie\Activitylog\Models\Activity;
use GuzzleHttp\Client;

use App\Jobs\SendVerifyEmail;
use App\Jobs\SendVerifySMS;
use JavaScript;

class ProfileController extends Controller
{
    public function profile()
    {
        $notverify = Verification::where('verified', 0)->where('user_id', Auth::user()->id)->where('name', 'email')->first();
        $categories = Category::all();
        $user = Auth::user();
        $logs = Activity::where('causer_id', $user->id)->latest()->limit(100)->get();

        $interests = [];
        foreach($user->interests as $i){
            $interests[] = $i->name;
        }

        JavaScript::put([
            'interests' => $interests
        ]);
    
        return view('profile.show', compact('user', 'logs','notverify','categories'));
    }

    public function profile_edit()
    {
        $user = Auth::user();
        return view('profile.edit', compact('user'));
    }

    public function profile_edit_pw()
    {
        $user = Auth::user();
        return view('profile.edit_pw', compact('user'));
    }

    public function account_update(Request $request)
    {
        // Get the user
        $user = Auth::user();

        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'phone_number' => 'required|string',
        ]);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

        $user->save();

        if($user->wasChanged('email')){
            $user->verifications()->where('name','email')->delete();

            $code = $user->id.str_random(40);

            $user->verifications()->create([
                'name' => 'email',
                'code' => $code,
            ]);

            SendVerifyEmail::dispatch($user);
        }

        if($user->wasChanged('phone_number')){
            $user->verifications()->where('name','sms')->delete();
            $sms_code = strtoupper(str_random(5));

            $user->verifications()->create([
                'name' => 'sms',
                'code' => $sms_code,
            ]);

            SendVerifySMS::dispatch($user);
        }
        
        flash()->success('Profile has been updated.');
        return redirect()->route('profile');
    }

    public function profile_update(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|min:2',
        ]);

        // Get the user
        $user = Auth::user();

        $interests = $request->interest;
        $toCreate = [];
        foreach ($interests as $interest){
            $toCreate[] = ['name' => $interest];
        }
        $user->interests()->delete();
        $user->interests()->createMany($toCreate);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password', 'interest'));
        $user->save();

        flash()->success('Profile has been updated.');
        return redirect()->route('profile');
    }

    public function profile_disaster(Request $request){
        // Get the user
        $user = Auth::user();
        $user->emergency = $request->emergency;
        $user->save();
        flash()->success('Profile has been updated.');
        return redirect()->route('profile');
    }

    public function profile_pw_update(Request $request){
        $this->validate($request, [
            'old_password' => 'required|old_password:' . Auth::user()->password,
            'password' => 'required|confirmed|min:6|different:old_password',
        ]);

        // Get the user
        $user = Auth::user();

        // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->save();
        flash()->success('Password has been updated.');
        return redirect()->route('profile');
    }

    public function update_categories(Request $request){
        $user = Auth::user();
        $user->categories()->sync($request->category);

        if($user->save()){
            flash('Profile has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
    }
}
