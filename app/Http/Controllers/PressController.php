<?php

namespace App\Http\Controllers;

use App\Press;
use Illuminate\Http\Request;

class PressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('view')=='trash'){
            $presses = Press::onlyTrashed()->get();
        }else{
            $presses = Press::all();
        }
        return view('presses.index', compact('presses'));
    }

    public function list($year=null){
        if($year){
            return Press::show()->whereYear('released_at', $year)->get();
        }
        return Press::show()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('presses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'=>'image',
            'title' =>'required',
            'link' =>'required|url',
            'released_at' =>'required|date',
            ]
        );

        if($request->image){
            $press = Press::create($request->only('title', 'link', 'released_at', 'highlight', 'hidden'))
                ->addMediaFromRequest('image')
                ->toMediaCollection('press');
        }else{
            $press = Press::create($request->only('title', 'link', 'released_at', 'highlight', 'hidden'));
        }


        return redirect()->route('presses.index')
            ->with('flash_message', 'Press added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Press  $press
     * @return \Illuminate\Http\Response
     */
    public function show(Press $press)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Press  $press
     * @return \Illuminate\Http\Response
     */
    public function edit(Press $press)
    {
        return view('presses.edit', compact('press'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Press  $press
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Press $press)
    {
        $this->validate($request, [
            'title' =>'required',
            'link' =>'required|url',
            'released_at' =>'required|date',
        ]);
 
        $press->title = $request->input('title');
        $press->link = $request->input('link');
        $press->released_at = $request->input('released_at');
        $press->highlight = $request->input('highlight');
        $press->hidden = $request->input('hidden');
        $press->save();

        return redirect()->route('presses.edit',
            $press->id)->with('flash_message', 'Press updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Press  $press
     * @return \Illuminate\Http\Response
     */
    public function destroy(Press $press)
    {
        $press->delete();
        return redirect()->route('presses.index')
            ->with('flash_message','Press successfully deleted');
    }

    public function image_update(Request $request, $id){
        $this->validate($request, [
            'image'=>'required|image'
            ]
        );

        $press = Press::findOrFail($id);
        $press->clearMediaCollection('press');
        $press->addMediaFromRequest('image')->toMediaCollection('press');
        return redirect()->route('presses.edit',$press->id)->with('flash_message', 'Image updated!');
    }

    public function image_delete(Request $request, $id){
        $press = Press::findOrFail($id);
        $press->clearMediaCollection('press');
        return redirect()->route('presses.edit',$press->id)->with('flash_message', 'Image deleted!');
    }
}
