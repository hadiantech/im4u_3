<?php

namespace App\Http\Controllers;

use App\Project;
use App\Organisation;
use App\Category;
use App\Verification;
use App\ProjectAssignment;
use App\ProjectReward;
use Illuminate\Http\Request;
use App\Jobs\SendSubscriberEmail;
use App\Jobs\SendProjectCreatedEmail;
use App\Jobs\SendProjectApprovedEmail;
use App\Jobs\SendProjectRejectedEmail;
use App\Jobs\SendProjectCancelledEmail;

use Auth;
use JavaScript;
use SEO;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if($request->status){
            JavaScript::put(['defaultStatus' => $request->status]);
        }

        JavaScript::put([
            'statusesjs' => [
                ['name' => 'Published', 'value' => 'published'],
                ['name' => 'Pending', 'value' => 'pending'],
                ['name' => 'Suspended', 'value' => 'suspended'],
                ['name' => 'Ended', 'value' => 'ended'],
            ]
        ]);
        return view('projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $check = Verification::where('name', 'email')->where('user_id', Auth::user()->id)->where('verified', 0)->first();
        if($check){
            flash()->error('Please verify your email first.');
            return redirect()->back();
        }

        $user = Auth::user();
        $organisations = Organisation::where('user_id', $user->id)->pluck('name','id');
        $categories = Category::all();
        return view('projects.create', compact('user', 'organisations', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_org = false;
        if($request->org_selector == 'org'){
            if(!$request->organisation_id){
                $this->validate($request, [
                    'name' => 'required',
                    'description' => 'required',
                    'incharge_name' => 'required',
                    'incharge_phone' => 'required',
                    'incharge_email' => 'required|email',
                ]);
                $new_org = true;
            }else{
                $this->validate($request, [
                    'organisation_id' => 'required',
                ]);
            }
        }

        $this->validate($request, [
            'title' => 'required',
            'tagline' => 'required',
            'information' => 'required',
            'started_at' => 'required',
            'ended_at' => 'required',
            'time_started_at' => 'required',
            'time_ended_at' => 'required',
            'hours' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            // 'country' => 'required',
            'postcode' => 'required',
            'number_volunteer' => 'required',
            'banner' => 'required|image',
            'thumbnail' => 'required|image',
        ]);

        if($new_org == true){
            $organisation = new Organisation;
            $organisation->user_id = Auth::user()->id;
            $organisation->name = $request->name;
            $organisation->description = $request->description;
            $organisation->website = $request->website;
            $organisation->incharge_name = $request->incharge_name;
            $organisation->incharge_phone = $request->incharge_phone;
            $organisation->incharge_email = $request->incharge_email;
            $organisation->save();
            $org_id = $organisation->id;
        }else{
            $org_id = $request->organisation_id;
        }

        $project = new Project;
        $project->user_id  = Auth::user()->id;
        $project->organisation_id  = $org_id;
        $project->title  = $request->title;
        $project->tagline  = $request->tagline;
        $project->information  = $request->information;
        $project->started_at  = $request->started_at;
        $project->ended_at  = $request->ended_at;
        $project->time_started_at  = $request->time_started_at;
        $project->time_ended_at  = $request->time_ended_at;
        $project->hours  = $request->hours;
        $project->address_1  = $request->address_1;
        $project->address_2  = $request->address_2;
        $project->city  = $request->city;
        $project->state  = $request->state;
        // $project->country  = $request->country;
        $project->country  = 'Malaysia';
        $project->postcode  = $request->postcode;
        $project->number_volunteer  = $request->number_volunteer;
        $project->save();

        $project->addMediaFromRequest('banner')->toMediaCollection('banner');
        $project->addMediaFromRequest('thumbnail')->toMediaCollection('thumbnail');

        $project->categories()->sync($request->category);

        $asgnms = [];
        foreach ($request->assignments as $assignment){
            if($assignment){
                $asgnms[] = ['detail' => $assignment];
            }
        }

        $project->assignments()->createMany($asgnms);

        $rwds = [];
        foreach ($request->rewards as $reward){
            if($reward){
                $rwds[] = ['detail' => $reward];
            }
        }
        $project->rewards()->createMany($rwds);

        SendProjectCreatedEmail::dispatch($project);
        return redirect()->route('projects.show', $project->id)->with('popup', 'project_created');
     
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $joined = null;
        if (Auth::check()) {
            $joined = $project->volunteers()->where('user_id', Auth::user()->id)->first();
            if(empty($joined)){
                $subscribed = $project->subscribers()->where('user_id', Auth::user()->id)->first();
            }
        }

        SEO::setTitle(config('app.name'). ' | ' .$project->title);
        SEO::setDescription($project->tagline);
        SEO::opengraph()->setUrl(route('projects.show', $project->id));
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::opengraph()->addImage($project->getMedia('thumbnail')->first()->getFullUrl());
        SEO::opengraph()->addImage($project->getMedia('banner')->first()->getFullUrl());
        
        return view('projects.show', compact('project', 'joined', 'subscribed'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $user = Auth::user();

        if($user->can('edit_projects') or $user->id == $project->user_id){
            //owner or admin
        }else{
            return abort(404);
        }

        $organisations = Organisation::where('user_id', $user->id)->pluck('name','id');
        $categories = Category::all();
        return view('projects.edit',compact('project', 'categories', 'organisations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
       
    }

    public function update_detail(Request $request, Project $project){
        $this->validate($request, [
            'title' => 'required',
            'tagline' => 'required',
            'information' => 'required',
            'started_at' => 'required',
            'ended_at' => 'required',
            'time_started_at' => 'required',
            'time_ended_at' => 'required',
            'hours' => 'required',
        ]);

        $project->title  = $request->title;
        $project->tagline  = $request->tagline;
        $project->information  = $request->information;
        $project->started_at  = $request->started_at;
        $project->ended_at  = $request->ended_at;
        $project->time_started_at  = $request->time_started_at;
        $project->time_ended_at  = $request->time_ended_at;
        $project->hours  = $request->hours;

        $project->status = 'pending';
        if($project->save()){
            flash('Project has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
        
    }

    public function update_location(Request $request, Project $project){
        $this->validate($request, [
            'address_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            // 'country' => 'required',
            'postcode' => 'required',
        ]);

        $project->address_1  = $request->address_1;
        $project->address_2  = $request->address_2;
        $project->city  = $request->city;
        $project->state  = $request->state;
        $project->postcode  = $request->postcode;

        $project->status = 'pending';
        if($project->save()){
            flash('Project has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
    }

    public function update_volunteer(Request $request, Project $project){
        // $this->validate($request, [
        //     'number_volunteer' => 'required',
        // ]);

        // $project->number_volunteer  = $request->number_volunteer;

        ProjectReward::where('project_id', $project->id)->delete();
        ProjectAssignment::where('project_id', $project->id)->delete();

        $asgnms = [];
        foreach ($request->assignments as $assignment){
            if($assignment){
                $asgnms[] = ['detail' => $assignment];
            }
        }

        $project->assignments()->createMany($asgnms);

        $rwds = [];
        foreach ($request->rewards as $reward){
            if($reward){
                $rwds[] = ['detail' => $reward];
            }
        }
        $project->rewards()->createMany($rwds);

        $project->status = 'pending';
        if($project->save()){
            flash('Project has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
    }

    public function update_categories(Request $request, Project $project){
        $project->categories()->sync($request->category);

        $project->status = 'pending';
        if($project->save()){
            flash('Project has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
    }
    
    public function update_type(Request $request, Project $project){
        $new_org = false;
        if($request->org_selector == 'org'){
            if(!$request->organisation_id){
                $this->validate($request, [
                    'name' => 'required',
                    'description' => 'required',
                    'incharge_name' => 'required',
                    'incharge_phone' => 'required',
                    'incharge_email' => 'required|email',
                ]);
                $new_org = true;
            }else{
                $this->validate($request, [
                    'organisation_id' => 'required',
                ]);
            }

            if($new_org == true){
                $organisation = new Organisation;
                $organisation->user_id = Auth::user()->id;
                $organisation->name = $request->name;
                $organisation->description = $request->description;
                $organisation->website = $request->website;
                $organisation->incharge_name = $request->incharge_name;
                $organisation->incharge_phone = $request->incharge_phone;
                $organisation->incharge_email = $request->incharge_email;
                $organisation->save();
                $org_id = $organisation->id;
            }else{
                $org_id = $request->organisation_id;
            }
            
            $project->organisation_id  = $org_id;
        }else{
            $project->organisation_id  =  null;
        }

        
        $project->status = 'pending';
        if($project->save()){
            flash('Project has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
    }
    
    public function update_template(Request $request, Project $project){
        $this->validate($request, [
            
        ]);

        $project->status = 'pending';
        if($project->save()){
            flash('Project has been updated.');
            return redirect()->back();
        }else{
            flash()->error('Error. Please try again');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        flash( $project -> title . ' has been deleted.');
        return redirect()->back();
        // return redirect()->route('projects.index');
    }

    public function explore(Request $request){

        if(!empty($request->category_id)){
            $projects = Project::upcoming()->whereHas('categories', function($q) use ($request){
                $q->where('category_id', $request->category_id);
            })->where('status', 'published')->get();
        }else{
            $projects = Project::upcoming()->where('status', 'published')->get();
        }
        
        $featured = null;
        $categories = Category::pluck('name','id');
        return view('projects.explore', compact('projects', 'featured', 'categories'));
    }

    public function join_project(Project $project){
        $user = Auth::user();
        $user->projects()->attach($project->id);
        $project->subscribers()->detach(Auth::user()->id);
        return redirect()->back();
    }

    public function leave_project(Project $project){
        $user = Auth::user();
        $user->projects()->detach($project->id);
        SendSubscriberEmail::dispatch($project);
        return redirect()->back();
    }

    public function subscribe_project(Project $project){
        $project->subscribers()->attach(Auth::user()->id);
        return redirect()->back();
    }

    public function unsubscribe_project(Project $project){
        $project->subscribers()->detach(Auth::user()->id);
        return redirect()->back();
    }

    public function update_status(Project $project, $status){
        $project->status = $status;
        $project->save();

        if($status == 'published'){
            SendProjectApprovedEmail::dispatch($project);
        }elseif($status == 'rejected'){
            SendProjectRejectedEmail::dispatch($project);
        }elseif($status == 'suspended'){
            SendProjectCancelledEmail::dispatch($project);
        }
        
        flash('Project has been '.$status);
        return redirect()->route('projects.edit', $project->id);
    }


    public function index_me(Request $request){
        if($request->status){
            JavaScript::put(['defaultStatus' => $request->status]);
        }

        JavaScript::put([
            'statusesjs' => [
                ['name' => 'Published', 'value' => 'published'],
                ['name' => 'Pending', 'value' => 'pending'],
                ['name' => 'Suspended', 'value' => 'suspended'],
                ['name' => 'Ended', 'value' => 'ended'],
            ]
        ]);
        return view('projects.me.index');
    }

    public function index_joined(Request $request){
        if($request->status){
            JavaScript::put(['defaultStatus' => $request->status]);
        }

        JavaScript::put([
            'statusesjs' => [
                ['name' => 'Ended', 'value' => 'ended'],
            ]
        ]);
        return view('projects.me.joined');
    }
}
