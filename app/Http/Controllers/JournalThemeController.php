<?php

namespace App\Http\Controllers;

use App\JournalTheme;
use Illuminate\Http\Request;

class JournalThemeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $theme = JournalTheme::first();
        return view('journals.themes.index', compact('theme'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'=>'required',
        ]);

        $theme = JournalTheme::create($request->only('title'));

        return redirect()->route('themes.index')
            ->with('flash_message', 'Theme updated!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JournalTheme  $journalTheme
     * @return \Illuminate\Http\Response
     */
    public function show(JournalTheme $journalTheme)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JournalTheme  $journalTheme
     * @return \Illuminate\Http\Response
     */
    public function edit(JournalTheme $journalTheme)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JournalTheme  $journalTheme
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'=>'required',
        ]);

        $theme = JournalTheme::first();
        $theme->title = $request->input('title');
        $theme->save();

        return redirect()->route('themes.index')
            ->with('flash_message', 'Theme updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JournalTheme  $journalTheme
     * @return \Illuminate\Http\Response
     */
    public function destroy(JournalTheme $journalTheme)
    {
        //
    }
}
