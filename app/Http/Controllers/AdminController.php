<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard(){
        return view('admin/dashboard');
    }

    public function user(){
        return view('admin/user');
    }

    public function user_create(){
        return view('admin/user_create');
    }

    public function user_update(){
        return view('admin/user_update');
    }

    public function user_activity(){
        return view('admin/user_activity');
    }

    public function user_project(){
        return view('admin/user_project');
    }
}
