<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('view')=='trash'){
            $banners = Banner::onlyTrashed()->get();
        }else{
            $banners = Banner::orderBy('order')->get();
        }
        return view('banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'banner'=>'required|image',
            'order' =>'required',
            ]
        );

        $banner = Banner::create($request->only('order', 'hidden', 'link'))
            ->addMediaFromRequest('banner')
            ->toMediaCollection('banners');

        flash('Banner Added');
        return redirect()->route('banners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('banners.edit', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::findOrFail($id);

        return view('banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'order'=>'required',
        ]);

        $banner = Banner::findOrFail($id);
        $banner->link = $request->input('link');
        $banner->order = $request->input('order');
        $banner->hidden = $request->input('hidden');
        $banner->save();

        flash('Banner Updated');
        return redirect()->route('banners.edit', $banner->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$banner = Banner::findOrFail($id);
        $banner = Banner::withTrashed()
                ->where('id', $id)->first();

        if($banner->trashed()) {
            $banner->forceDelete();
        }else{
            $banner->delete();
        }

        flash('Banner successfully deleted');
        return redirect()->route('banners.index');
    }

    public function restore($id)
    {
        $banner = Banner::withTrashed()
                    ->where('id', $id)
                    ->restore();

        flash('Banner successfully restored');

        return redirect()->route('banners.index');
    }
}
