<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Journal;
use SEO;
use DB;

class JournalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request('view')=='trash'){
            $journals = Journal::onlyTrashed()->get();
        }else{
            $journals = Journal::get();
        }
        return view('journals.index', compact('journals'));
    }

    public function list($year=null, $month=null){
        $journal = new Journal;
        if($year){
            $journal = $journal->whereYear('released_at', $year);
        }
        if($month){
            $j = 1;
            $months = [];
            for($i=$month;;$i++){
                array_push($months, $i);
                if($j==3){
                    break;
                }
                $j++;
            }

            $journal = $journal->whereIn(DB::raw('MONTH(released_at)'), $months);
        }
        // return $months;
        return $journal->show()->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('journals.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image'=>'required|image',
            'author_image'=>'required|image',
            'title' =>'required',
            'name' =>array(
                'required',
                'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'unique:journals,name'
            ),
            'author_name' =>'required',
            'author_detail' =>'required',
            'content' =>'required',
            'released_at' =>'required',
            ]
        );

        $journal = Journal::create($request->only('title','name','author_name','author_detail','content', 'featured', 'hidden', 'released_at'));
        $journal->addMediaFromRequest('image')->toMediaCollection('journals');
        $journal->addMediaFromRequest('author_image')->toMediaCollection('authors');

        flash('Journal added');
        return redirect()->route('journals.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $journal = Journal::where('name', $name)->first();
        if(!$journal){
            $journal = Journal::find($name);
        }

        SEO::setTitle(config('app.name'). ' | ' .$journal->title);
        SEO::setDescription($journal->content);
        SEO::opengraph()->setUrl(route('journals.show', $journal->name));
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::opengraph()->addImage($journal->getMedia('journals')->first()->getFullUrl());

        return view('journals.show', compact('journal'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Journal $journal)
    {
        return view('journals.edit', compact('journal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' =>'required',
            'name' =>array(
                'required',
                'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'unique:journals,name,'.$id
            ),
            'author_name' =>'required',
            'author_detail' =>'required',
            'content' =>'required',
            'released_at' =>'required',
        ]);

        $journal = Journal::findOrFail($id);
        $journal->title = $request->input('title');
        $journal->name = $request->input('name');
        $journal->author_name = $request->input('author_name');
        $journal->author_detail = $request->input('author_detail');
        $journal->content = $request->input('content');
        $journal->featured = $request->input('featured');
        $journal->hidden = $request->input('hidden');
        $journal->released_at = $request->input('released_at');
        
        if($journal->isDirty('featured')){
            if($journal->featured == 1){
                Journal::where('featured', 1)
                ->update(['featured' => 0]);
            }
        }
        
        $journal->save();

        flash('Journal updated');
        return redirect()->route('journals.edit', $journal->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $journal = Journal::withTrashed()
                ->where('id', $id)->first();

        if($journal->trashed()) {
            $journal->forceDelete();
        }else{
            $journal->delete();
        }

        flash('Journal successfully deleted');
        return redirect()->route('journals.index');
    }

    public function restore($id)
    {
        $journal = Journal::withTrashed()
                    ->where('id', $id)
                    ->restore();

        flash('Journal successfully restored');
        return redirect()->route('journals.index');
    }

    public function journal_author_image_update(Request $request, $id){

        $this->validate($request, [
            'author_image'=>'required|image'
            ]
        );

        $journal = Journal::findOrFail($id);
        $journal->clearMediaCollection('authors');
        $journal->addMediaFromRequest('author_image')->toMediaCollection('authors');
        flash('Journal author image updated');
        return redirect()->route('journals.edit',$journal->id);

    }

    public function journal_main_image_update(Request $request, $id){

        $this->validate($request, [
            'image'=>'required|image'
            ]
        );

        $journal = Journal::findOrFail($id);
        $journal->clearMediaCollection('journals');
        $journal->addMediaFromRequest('image')->toMediaCollection('journals');
        flash('Journal main image updated');
        return redirect()->route('journals.edit',$journal->id);

    }

}
