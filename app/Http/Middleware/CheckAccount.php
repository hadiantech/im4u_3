<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Verification;

class CheckAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $check = Verification::where('name', 'sms')->where('user_id', Auth::user()->id)->where('verified', 1)->first();
        if(!$check){
            if (Auth::user()->check() == false) {
                return redirect()->route('fill');
            }else{
                return redirect()->route('sms.confirm');
            }
        }
        return $next($request);
    }
}
