<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Verification extends Model
{
    protected $fillable = [
        'name', 'code', 'status'
    ];

    function scopeEmail($query){
        return $query->where('name', 'email')->where('verified', 0);
    }

    function scopeSms($query){
        return $query->where('name', 'sms')->where('verified', 0);
    }

}
