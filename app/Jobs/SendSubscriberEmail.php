<?php

namespace App\Jobs;
use App\Project;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Mail\Mailer;

class SendSubscriberEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $project;
    public $tries = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        if(count($this->project->subscribers)){
            foreach($this->project->subscribers as $user){
                $mailer->send('emails.subscribe', ['user' => $user, 'project' => $this->project], function($message) use ($user)
                {
                    $message->from('no-reply@im4u.my', "IM4U");
                    $message->subject("Project Registration is Available");
                    $message->to($user->email);
                }); 
            }
        }
    }
}
