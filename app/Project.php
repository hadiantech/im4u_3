<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

use Carbon\Carbon;

class Project extends Model implements HasMedia
{
    use LogsActivity;
    use HasMediaTrait;  
    use SoftDeletes;

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
    protected static $logName = 'projects';
    protected static $ignoreChangedAttributes = ['updated_at'];

    public function organisation()
    {
        return $this->belongsTo('App\Organisation');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function assignments()
    {
        return $this->hasMany('App\ProjectAssignment');
    }

    public function rewards()
    {
        return $this->hasMany('App\ProjectReward');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function volunteers()
    {
        return $this->belongsToMany('App\User');
    }

    public function subscribers()
    {
        return $this->belongsToMany('App\User', 'project_subscribe');
    }

    public function scopePending($query)
    {
        return $query->where('status', 'pending');
    }

    public function scopeUpcoming($query)
    {
        return $query->whereDate('started_at', '>=', Carbon::today()->toDateString());
    }
}
