<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\SoftDeletes;

use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasRoles;
    use LogsActivity;
    use HasMediaTrait;
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected static $logAttributes = ['name', 'email', 'password'];
    protected static $logOnlyDirty = true;
    protected static $logName = 'users';
    protected static $ignoreChangedAttributes = ['updated_at', 'remember_token'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'identification_type', 'identification_number', 'marital_status', 'phone_number', 'state', 'facebook_id', 'google_id',
        'emergency', 'date_of_birth', 'address_1', 'address_2', 'city', 'postcode', 'country', 'education', 'profession', 'interest'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function verifications()
    {
        return $this->hasMany('App\Verification');
    }

    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    public function subscriptions()
    {
        return $this->belongsToMany('App\Project', 'project_subscribe');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function interests()
    {
        return $this->hasMany('App\Interest');
    }

    public function addNew($input, $drive)
    {
        if($drive == 'facebook'){
            $check = static::where('facebook_id',$input['facebook_id'])->first();
        } else if($drive == 'google'){
            $check = static::where('google_id',$input['google_id'])->first();
        }
        
        if(is_null($check)){
            return static::create($input);
        }
        return $check;
    }

    function check(){
        $none = 0;
        if(empty($this->name)){
            $none++;
        }
        if(empty($this->email)){
            $none++;
        }
        if(empty($this->identification_type)){
            $none++;
        }
        if(empty($this->identification_number)){
            $none++;
        }
        if(empty($this->marital_status)){
            $none++;
        }
        if(empty($this->phone_number)){
            $none++;
        }
        if(empty($this->state)){
            $none++;
        }

        if($none == 0){
            return true;
        }
        return false;
    }

    function checkOptional(){
        $none = 0;
        if(empty($this->date_of_birth)){
            $none++;
        }
        if(empty($this->address_1)){
            $none++;
        }
        if(empty($this->city)){
            $none++;
        }
        if(empty($this->postcode)){
            $none++;
        }
        if(empty($this->country)){
            $none++;
        }
        if(empty($this->education)){
            $none++;
        }
        if(empty($this->profession)){
            $none++;
        }
        if(!count($this->interests)){
            $none++;
        }

        if($none == 0){
            return true;
        }
        return false;
    }

    public function emailVerify(){
        return $this->verifications()->email();
    }

    public function smsVerify(){
        return $this->verifications()->sms();
    }

}
