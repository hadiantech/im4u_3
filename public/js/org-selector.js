//selector for organisation radio btn
$('#radio-org').click(function() {
    $('#org-form').removeClass('hide');
    $('#org-form').addClass('animated fadeIn');
    $("html, body").delay(100).animate({scrollTop: $('#org-form').offset().top }, 800);
    $("[name='organisation_id']").prop("required", true);
    
});

$('#radio-ind').click(function() {
    $("[name='organisation_id']").removeAttr('required');
    $('#org-form').addClass('hide');
    $('#org-form').removeClass('fadeIn');
    $('#proj-detail-form').removeClass('hide');
    $('#proj-detail-form').addClass('animated fadeIn');
    $("html, body").delay(100).animate({scrollTop: $('#proj-detail-form').offset().top }, 800);
});

//select old-new organisation
$('#addnew-org').click(function() {
    $("[name='organisation_id']").removeAttr('required');
    $('#new-org').removeClass('hide');
    $('#new-org').addClass('animated fadeIn');
    $('#old-org').addClass('hide');
    $('#addnew-org').addClass('hide');
    $('#organisation_id').val('');
    $("[name='name']").prop("required", true);
    $("[name='description']").prop("required", true);
    $("[name='incharge_name']").prop("required", true);
    $("[name='incharge_phone']").prop("required", true);
    $("[name='incharge_email']").prop("required", true);
})

$('#cancel-add-new').click(function() {
    $("[name='name']").removeAttr('required');
    $("[name='description']").removeAttr('required');
    $("[name='incharge_name']").removeAttr('required');
    $("[name='incharge_phone']").removeAttr('required');
    $("[name='incharge_email']").removeAttr('required');
    $('#new-org').removeClass('animated fadeIn');
    $('#new-org').addClass('hide');
    $('#old-org').removeClass('hide');
    $("[name='organisation_id']").prop("required", true);
})

// load next form when press next
$('#next-proj-detail').click(function() {
    $('#proj-detail-form').removeClass('hide');
    $('#proj-detail-form').addClass('animated fadeIn');
    $("html, body").delay(100).animate({scrollTop: $('#proj-detail-form').offset().top }, 800);
})

$('#next-proj-location').click(function() {
    $('#proj-location').removeClass('hide');
    $('#proj-location').addClass('animated fadeIn');
    $("html, body").delay(100).animate({scrollTop: $('#proj-location').offset().top }, 800);
})

$('#next-vol-details').click(function() {
    $('#vol-details').removeClass('hide');
    $('#vol-details').addClass('animated fadeIn');
    $("html, body").delay(100).animate({scrollTop: $('#vol-details').offset().top }, 800);
})

$('#next-proj-photo').click(function() {
    $('#proj-photo').removeClass('hide');
    $('#proj-photo').addClass('animated fadeIn');
    $("html, body").delay(100).animate({scrollTop: $('#proj-photo').offset().top }, 800);
})


//preview image
function readURL(t) {
    var i = URL.createObjectURL(t.target.files[0]); 
    $("#bg-image-" + t.target.id).css("background-image", "url(" + i + ")");
   
} 
