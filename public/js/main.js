//TEMPAT INIT/CONFIG JS PLUGINS


//bigslide (sidebar menu)
//doc here https://github.com/ascott1/bigSlide.js?files=1


//slick
//doc here http://kenwheeler.github.io/slick/
$(document).ready(function () {
    $('.slick-item').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

});

$(document).ready(function () {
    $('.slick-people-journal').slick({
        dots: true,
        arrow: true,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrow: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrow: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

});

$(document).ready(function () {
    $('.slick-press').slick({
        slidesToShow: 4,
        arrow: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrows: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            }
        ]
    });

});
$(document).ready(function () {
    $('.slick-featured-project').slick({
        slidesToShow: 1,
        arrow: true,
        dots: true
    });

});


$(document).ready(function () {
    $('.slick-item-2').slick({
        slidesToShow: 1,
        arrow: true
    });

});

$(document).ready(function () {
    $('.slick-item-success').slick({
        slidesToShow: 1,
        arrow: true,
        dots: true
    });

});


$(document).ready(function () {
    $('.slick-photo-gallery').slick({
        slidesToShow: 1,
        arrow: true
    });

});

$(document).ready(function () {
    $('.slick-up-project').slick({
        slidesToShow: 4,
        arrow: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrows: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            }
        ]
    });

});

$(document).ready(function () {
    $('.slick-collab').slick({
        slidesToShow: 4,
        arrow: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrows: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            }
        ]
    });

});

$(document).ready(function () {
    $('.slick-amba').slick({
        slidesToShow: 4,
        arrow: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrows: true,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }
            }
        ]
    });

});


$(document).ready(function () {
    $('.slick-fullscreen').slick({
        slidesToShow: 1,
        dots: true,
        centerMode: true,
        centerPadding: '0px',
        arrows: 'false'
    });

});



//dotdotdot
//doc here http://dotdotdot.frebsite.nl/
$(document).ready(function () {
    $(".dotdotdot-text").dotdotdot({

        ellipsis: "\u2026 ",
        height: 100,
        truncate: "word"

    });
});

$(document).ready(function () {
    $(".dotdotdot-text-m").dotdotdot({

        ellipsis: "\u2026 ",
        height: 100,
        truncate: "word"

    });
});

$(document).ready(function () {
    $(".dotdotdot-text-s").dotdotdot({

        ellipsis: "\u2026 ",
        height: 90,
        truncate: "word"

    });
});

$(document).ready(function () {
    $(".dotdotdot-title").dotdotdot({
        ellipsis: "\u2026 ",
        height: 200,
        truncate: "word"
    });
});

$(document).ready(function () {
    $(".dotdotdot-title-md").dotdotdot({
        ellipsis: "\u2026 ",
        height: 100,
        truncate: "word"
    });
});

$(document).ready(function () {
    $(".dotdotdot-title-s").dotdotdot({
        ellipsis: "\u2026 ",
        height: 60,
        truncate: "word"
    });
});

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: true,
    centerMode: true,
    centerPadding: '100px',
    focusOnSelect: true
});

$('.slider-for-video').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.slider-nav-video'
});
$('.slider-nav-video').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.slider-for-video',
    dots: true,
    centerMode: true,
    centerPadding: '100px',
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true,
                dots: true
            }
            },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
        ]
});

//select2
$(document).ready(function() {
    $('.js-example-basic-single').select2(
    );
});

//masongram
//https://github.com/mladenplavsic/masongram

//bootstrap carousel
//$('.carousel').carousel()
