<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/send/{project}', 'HomeController@send')->name('send');
Route::get('/test', 'HomeController@test')->name('test');

Route::get('/about', 'HomeController@about')->name('about');
Route::get('/dreamfund', 'HomeController@dreamfund')->name('dreamfund');
Route::get('/happening', 'HomeController@happening')->name('happening');
Route::get('/media', 'HomeController@media')->name('media');
Route::get('/photos', 'HomeController@photos')->name('photos');
Route::get('/videos', 'HomeController@videos')->name('videos');
Route::any('/explore', 'ProjectController@explore')->name('explore');
Route::any('/journal', 'HomeController@journal')->name('journal');
Route::any('/journal/archive/{year?}/{month?}', 'HomeController@journal_archive')->name('journal.archive');
Route::any('/journal/detail', 'HomeController@journal_detail')->name('journal.detail');

Route::get('/journal/year/{year?}/{month?}', 'JournalController@list');

Route::any('/media/press', 'HomeController@media_press')->name('media.press');
Route::get('/presses/year/{year?}', 'PressController@list');

Route::resource('themes', 'JournalThemeController');

Route::any('/contactus', 'HomeController@contactus')->name('contactus');
Route::any('/partnership', 'HomeController@partnership')->name('partnership');
Route::any('/getinvolved', 'HomeController@getinvolved')->name('getinvolved');

Route::get('/verify/{code}', 'HomeController@verify')->name('verify');
Route::get('/sms/verify', 'HomeController@verify_sms')->name('sms.confirm');
Route::get('/sms/resend', 'HomeController@resend_code')->name('sms.resend');
Route::post('/sms/confirm', 'HomeController@confirm_sms')->name('sms.confirm.code');
Route::get('/email/resend', 'HomeController@resend_email')->name('email.resend');
Route::get('/email/test/{project}', 'HomeController@email_test')->name('email.test');



//facebook auth
Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');

//google auth
Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

Route::group( ['middleware' => ['auth']], function() {
    Route::get('/fill', 'HomeController@fill')->name('fill');
    Route::post('/auth/update', 'HomeController@auth_update')->name('auth.update');
    Route::post('/phone/update', 'HomeController@phone_update')->name('phone.update');
    Route::get('/phone/change', 'HomeController@phone_change')->name('phone.change');
});

Route::group( ['middleware' => ['auth', 'checkAccount']], function() {
    Route::get('/dashboard',  'HomeController@dashboard')->name('dashboard');

    Route::get('/projects/me',  'ProjectController@index_me')->name('projects.me');
    Route::get('/projects/joined',  'ProjectController@index_joined')->name('projects.joined');

    Route::get('/profile',  'ProfileController@profile')->name('profile');
    Route::get('/profile/edit',  'ProfileController@profile_edit')->name('profile.edit');
    Route::get('/profile/password',  'ProfileController@profile_edit_pw')->name('profile.edit_pw');
    Route::put('/profile/update',  'ProfileController@profile_update')->name('profile.update');
    Route::put('/account/update',  'ProfileController@account_update')->name('account.update');
    Route::put('/profile/password/update',  'ProfileController@profile_pw_update')->name('profile.pw_update');
    Route::put('/profile/disaster',  'ProfileController@profile_disaster')->name('profile.disaster');
    Route::put('/profile/update/categories', 'ProfileController@update_categories')->name('profile.update.categories');
    
    Route::resource('users', 'UserController');
    Route::get('/users/password/{id}/edit', 'UserController@edit_pw')->name('users.edit_pw');    
    Route::put('/users/password/{id}', 'UserController@update_pw')->name('users.update_pw');    

    Route::resource('roles', 'RoleController');
    Route::resource('posts', 'PostController');
    Route::resource('categories', 'CategoryController');
    Route::resource('projects', 'ProjectController');

    Route::resource('presses', 'PressController');
    Route::get('/presses/{id}/restore', 'PressController@restore')->name('presses.restore');
    Route::put('/presses/{id}/banner', 'PressController@image_update')->name('presses.image.update');
    Route::delete('/presses/{id}/banner', 'PressController@image_delete')->name('presses.image.delete');

    Route::get('/projects/{project}/status/{status}', 'ProjectController@update_status')->name('projects.update.status');

    Route::get('/projects/join/{project}', 'ProjectController@join_project')->name('projects.join');
    Route::get('/projects/leave/{project}', 'ProjectController@leave_project')->name('projects.leave');

    Route::get('/projects/subscribe/{project}', 'ProjectController@subscribe_project')->name('projects.subscribe');
    Route::get('/projects/unsubscribe/{project}', 'ProjectController@unsubscribe_project')->name('projects.unsubscribe');

    Route::put('/projects/update/{project}/detail', 'ProjectController@update_detail')->name('projects.update.detail');
    Route::put('/projects/update/{project}/location', 'ProjectController@update_location')->name('projects.update.location');
    Route::put('/projects/update/{project}/volunteer', 'ProjectController@update_volunteer')->name('projects.update.volunteer');
    Route::put('/projects/update/{project}/categories', 'ProjectController@update_categories')->name('projects.update.categories');
    Route::put('/projects/update/{project}/type', 'ProjectController@update_type')->name('projects.update.type');


    //cms
    Route::resource('banners', 'BannerController');
    Route::get('/banners/{id}/restore', 'BannerController@restore')->name('banners.restore');

    Route::resource('journals', 'JournalController')->except([
        'show'
    ]);

    Route::get('/journals/{id}/restore', 'JournalController@restore')->name('journals.restore');
    Route::put('/journals/{id}/banner/main/image', 'JournalController@journal_main_image_update')->name('journals.main.image.update');
    Route::put('/journals/{id}/banner/author/image', 'JournalController@journal_author_image_update')->name('journals.author.image.update');

    //datatables
    Route::get('datatables/getUser', 'DatatablesController@getUser')->name('datatables.users');
    Route::get('datatables/getCategories', 'DatatablesController@getCategories')->name('datatables.categories');
    Route::get('datatables/getProjects', 'DatatablesController@getProjects')->name('datatables.projects');
    Route::get('datatables/getPress', 'DatatablesController@getPresses')->name('datatables.presses');
    Route::get('datatables/getMyProjects', 'DatatablesController@getMyProjects')->name('datatables.projects.me');
    Route::get('datatables/getMyJoinedProjects', 'DatatablesController@getMyJoinedProjects')->name('datatables.projects.joined');
    Route::get('datatables/getJoined/{project}', 'DatatablesController@getJoined')->name('datatables.joined');


    //upload 
    Route::post('upload/image', 'UploadController@upload_image')->name('upload.image');


});

Route::resource('projects', 'ProjectController')->only([
    'show'
]);

Route::resource('journals', 'JournalController')->only([
    'show'
]);

