@extends('layouts.front.master')
@section('content')

<div class="container-fluid p-0 ">
        <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/about/background.jpg)">
            <div class="col-md-12 text-center">
                <h1 class="white-text text-shadow">ABOUT VOLUNTEER MALAYSIA</h1>
                <h5 class="white-text text-shadow">“Growing You for The Better Us”.</h5>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  quote  -->
    <div class="container-fluid p-0 blue-bg pt-5 pb-5">
        <div class="row justify-content-center pt-4 pb-4 box-shadow" style="background-color: rgba(0,0,0,0.4)">
            <div class="col-md-10 col-lg-8 d-flex m-4 justify-content-center align-items-center">
                <i class="fas fa-quote-left white-text fa-3x"></i>
                <h1 class="text-center white-text m-4 futura-bold yellow-text">We come together and volunteer for a greater us and a better Malaysia.</h1>
                <i class="fas fa-quote-right white-text fa-3x"></i>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  who we are  -->
    <div class="container-fluid p-0 blue-bg pt-3 pb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xs-12 col-sm-8 col-lg-4 mb-4">
                    <div class="col-sm-12 green-bg rounded box-shadow p-4">
                        <h3 class="white-text m-0 text-center">WHO ARE WE</h3>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                        <div class="col-sm-12 rounded-bottom white-bg box-shadow p-3 who-box d-flex justify-content-center">
                            <p class="m-0 bold text-justify text-center blue-text align-self-center mb-2">Malaysia’s leading community volunteer platform.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-lg-4  mb-4">
                    <div class="col-sm-12 blue3-bg rounded box-shadow p-4">
                        <h3 class="white-text m-0 text-center">WHAT WE DO</h3>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                        <div class="col-sm-12 rounded-bottom white-bg box-shadow p-3 who-box d-flex justify-content-center">
                            <p class="m-0 bold text-justify text-center blue-text align-self-center mb-2">We spread the spirit of volunteerism amongst people from all walks of life through unconventional and impactful social community activities.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-lg-4 mb-5">
                    <div class="col-sm-12 red-bg rounded box-shadow p-4">
                        <h3 class="white-text m-0 text-center">WHY WE DO IT</h3>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex justify-content-center">
                        <div class="col-sm-12 rounded-bottom white-bg box-shadow p-3 who-box d-flex justify-content-center">
                            <p class="m-0 bold text-justify text-center blue-text align-self-center mb-2">To encourage, motivate and inspire the Malaysian communities to adopt best practices and become role model for others in giving back to the society.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  vision mission  -->
    <div id="vision_mission" class="container-fluid bluesea-bg pt-5 pb-5">
        <div class="container">
            <div class="row mb-4">
                <div class="col-md-12 pt-5">
                    <h1 class="blue-text text-sm-left">VISION & MISSION</h1>
                </div>
            </div>
            <div class="row d-flex">
                <div class="col-sm-12 col-lg-6">
                    <div class="d-flex white-bg rounded p-4 box-shadow m-2">
                        <div class="col-sm-4 d-flex">
                            <div class="col-sm-12 d-flex flex-column justify-content-center"><img class="img-fluid" src="img/about/vision.png" /></div>
                        </div>

                        <div class="col-sm-8 d-flex flex-column justify-content-center">
                            <h4 class="blue-text">Our Vision</h4>
                            <p>To build a united and caring society that will give rise to leaders of tomorrow</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6 d-flex">
                    <div class="d-flex white-bg rounded p-4 box-shadow m-2">
                        <div class="col-sm-4 d-flex">
                            <div class="col-sm-12 d-flex flex-column justify-content-center"><img class="img-fluid" src="img/about/mission-01.png" /></div>
                        </div>
                        <div class="col-sm-8 d-flex flex-column justify-content-center">
                            <h4 class="blue-text">Our Mission</h4>
                            <p>To nurture and encourage the growth of volunteerism in the hearts of all Malaysians.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 p-4 mt-4">
                    <h1 class="blue-text text-sm-left">OUR VALUES</h1>
                </div>
            </div>
            <div class="row p-1 pb-5">
                <div class="col-sm-12 col-lg-4">
                    <div class="values-box box-shadow white-bg rounded p-5">
                        <div class="row justify-content-center">
                            <div class="col-sm-12 text-xs-center text-sm-center pt-2">
                                <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-3 mb-3"><img class="img-fluid animated infinite pulse" src="img/about/passionate-01.png" /></div>
                                <div class="col-sm-12">
                                    <h4 class="blue-text">Passionate</h4>
                                    <p class="blue-text">We are <span class="bold">PASSIONATE</span> about volunteering <br/><br/>We spread the spirit and value of volunteerism</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-4 ">
                    <div class="values-box box-shadow white-bg rounded p-5">
                        <div class="row justify-content-center">
                            <div class="col-sm-12 text-xs-center text-sm-center">
                                <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-3 mb-3"><img class="img-fluid animated infinite bounce" src="img/about/innovative-01.png" /></div>
                                <div class="col-sm-12">
                                    <h4 class="blue-text">Innovative</h4>
                                    <p class="blue-text">We are <span class="bold">INNOVATIVE </span> in creating volunteering activities <br/><br/>We seed creative and infectious volunteering ideas</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-sm-12 col-lg-4 ">
                    <div class="values-box box-shadow white-bg rounded pl-0 pr-0 p-5">
                        <div class="row justify-content-center">
                            <div class="col-sm-12 text-xs-center text-sm-center">
                                <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-3 mb-3"><img class="img-fluid animated infinite jello" src="img/about/inspiring-01.png" /></div>
                                <div class="col-sm-12">
                                    <h4 class="blue-text">Inspiring</h4>
                                    <p class="blue-text">We are <span class="bold">INSPIRING  </span> those around us through volunteerism <br/><br/>We inspire the nation through impactful volunteering activities </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  pillars  -->
    <div id="pillars" class="container-fluid blue-bg p-5">
        <div class="container pt-5 pb-5">
            <div class="row mb-4">
                <div class="col-sm-12">
                    <h1 class="white-text">THE PILLARS OF VOLUNTEER MALAYSIA</h1>
                    <p class="white-text">All our initiatives are centered on these four pillars:</p>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col-sm-12 col-lg-3 col-md-6">
                    <div class="pillar-box white-bg rounded p-3 m-2 box-shadow hover-zoom">
                        <div class="row justify-content-center text-center">
                            <div class="col-sm-5 m-3"><img src="img/about/arts-01.png" class="img-fluid" /></div>
                            <div class="col-sm-12 m-3">
                                <h3 class="red-text">Arts, Culture & Innovation</h3>
                            </div>
                            <div class="col-sm-12">
                                <p class="blue-text">We promote Malaysian culture and diversity, encouraging communities to pursue their interests in arts and culture which will drive innovation.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 col-md-6">
                    <div class="pillar-box white-bg rounded p-3 m-2 box-shadow hover-zoom">
                        <div class="row justify-content-center text-center">
                            <div class="col-sm-5 m-3"><img src="img/about/community-01.png" class="img-fluid" /></div>
                            <div class="col-sm-12 m-3">
                                <h3 class="red-text">Community Well-Being</h3>
                            </div>
                            <div class="col-sm-12">
                                <p class="blue-text">We promote inclusiveness and embrace harmony by reaching out to communities in need to build a better living environment.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 col-md-6">
                    <div class="pillar-box white-bg rounded p-3 m-2 box-shadow hover-zoom">
                        <div class="row justify-content-center text-center">
                            <div class="col-sm-5 m-3"><img src="img/about/sport-01.png" class="img-fluid" /></div>
                            <div class="col-sm-12 m-3">
                                <h3 class="red-text">Sports<br/><br/></h3>
                            </div>
                            <div class="col-sm-12">
                                <p class="blue-text">We promote physical and mental well-being while fostering team spirit to build a healthier and united nation.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-3 col-md-6">
                    <div class="pillar-box white-bg rounded p-3 m-2 box-shadow hover-zoom">
                        <div class="row justify-content-center text-center">
                            <div class="col-sm-5 m-3"><img src="img/about/knowledge-01.png" class="img-fluid" /></div>
                            <div class="col-sm-12 m-3">
                                <h3 class="red-text">Knowledge Building</h3>
                            </div>
                            <div class="col-sm-12">
                                <p class="blue-text">We engage in learning and educational activities to enhance knowledge and skills of the local communities.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  team  -->
 <!--   
    <div id="team" class="container-fluid bluesea-bg p-0 pt-5">

        <div class="row">
            <div class="container mt-4 mb-3 p-0">
                <div class="col-sm-10">
                    <h1 class="blue-text">MEET OUR TEAM</h1>
                   <p>Our dedicated, passionate and diverse team works together in unity for a common purpose, spreading the spirit of volunteerism to all.</p> 
                </div>

            </div>
        </div>

       <div class="row justify-content-center" style="position: relative; height: 600px">

            <div class="col-sm-12 p-0" style="overflow: hidden;">
                <div class="col-sm-12 col-lg-12 p-0 m-0 team-box hover-zoom" style="background-image: url(img/about/meetourteam.jpg);">

                </div>
            </div>
            <div class="shader"></div>
        </div>   
-->    


    </div>
    <!--  end  -->

    <!--  leader  -->
<!--
    <div class="container-fluid bluesea-bg pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 p-0 m-0 p-0 mb-4" style="overflow: hidden;">
                    <h4 class="blue-text">SENIOR LEADERSHIP TEAM</h4>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-10 offset-sm-1 col-md-6 offset-md-0 p-0 mb-3">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center justify-content-between mt-3">
                            <div class="col-sm-6 offset-sm-3 mb-4"><img src="img/about/rudy.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">AZLAN RUDY MALIK</h4>
                                <p class="">Chief Executive Officer</p>
                            </div>
                           <div class="col-sm-12"><a href="" data-toggle="modal" data-target="#rudymodal" class="btn btn-med btn-yellow">View Details</a></div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 offset-sm-1 col-md-6 offset-md-0 p-0 mb-3">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center justify-content-between mt-3">
                            <div class="col-sm-6 offset-sm-3 mb-4"><img src="img/about/wern.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">TAN SHYUE WERN</h4>
                                <p class="">Director of Brand Management Group</p>
                            </div>
                           <div class="col-sm-12"><a href="" data-toggle="modal" data-target="#wernmodal" class="btn btn-med btn-yellow">View Details</a></div> 
                        </div>
                    </div>
                </div>

              <div class="col-sm-10 offset-sm-1 col-md-4 offset-md-0 p-0 mb-3">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center ">
                            <div class="col-sm-8 offset-sm-2 mb-4"><img src="img/about/tini.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">KARTINI ARIFFIN</h4>
                                <p class="">Director of iM4U fm</p>
                            </div>
                            <div class="col-sm-12"><a href="" data-toggle="modal" data-target="#tinimodal" class="btn btn-med btn-yellow">View Details</a></div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">
                <div class="col-sm-10 offset-sm-1 col-md-3 offset-md-0 p-0 mb-3 mt-1">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center justify-content-between mt-4">
                            <div class="col-sm-12 offset-sm-0 mb-4"><img src="img/about/woman.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">Ee Lin</h4>
                                <p class="">Senior Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 offset-sm-1 col-md-3 offset-md-0 p-0 mb-3 mt-1">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center justify-content-between mt-4">
                            <div class="col-sm-12 offset-sm-0 mb-4"><img src="img/about/woman.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">Liyana</h4>
                                <p class="">Senior Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 offset-sm-1 col-md-3 offset-md-0 p-0 mb-3 mt-1">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center justify-content-between mt-4">
                            <div class="col-sm-12 offset-sm-0 mb-4"><img src="img/about/woman.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">Person Name</h4>
                                <p class="">Senior Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-10 offset-sm-1 col-md-3 offset-md-0 p-0 mb-3 mt-1">
                    <div class="leader-box border p-3 rounded box-shadow hover-zoom white-bg mx-3">
                        <div class="row text-center justify-content-between mt-4">
                            <div class="col-sm-12 offset-sm-0 mb-4"><img src="img/about/man.png" class="img-fluid"></div>
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-1">Person Name</h4>
                                <p class="">Senior Manager</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->
    <!--  end  -->

    <!--  modal  -->
    <div class="modal fade" id="rudymodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header blue-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-5 col-lg-7">
                            <h2 class="yellow-text">AZLAN RUDY MALIK</h2>
                            <p class="yellow-text">Chief Executive Officer</p>
                        </div>
                        <div class="col-sm-7 col-lg-4"><img src="img/about/rudy.png" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <span class="blue-text">
                            “ iM4U has grown by leaps and bounds. I have learned the importance of having passion in my work and leadership. Being part of this progressive family has given me a broader and deeper meaning of volunteerism and its sustainability. ”
                            </span>
                        </div>
                        <div class="col-sm-12">
                            <p>Rudy grew up in a family of five and is the eldest amongst his brothers. He came from a Malay and Chinese parentage of which he got the benefit of living through 2 cultures in his childhood years. He holds a Bach. of Arts (Honours) in Accounting and Finance from Oxford Brookes University.</p>

                            <p>He joined IBM Malaysia in the year 2000 as an Accounting Analyst. He was promoted to Team Leader and was then assigned to IBM Corp in New York for 6 months in 2007 handling Asia Pacific Expenses. When he returned back, he was promoted to Manager. He handled a team of 10 staff and was then later given an opportunity to project manage several divestiture projects.</p>

                            <p>In 2013, Rudy joined iM4U as the Director of Corporate Services, where he led the team that oversaw the organization’s finance, procurement and human resource.</p>

                            <p>He was promoted as iM4U’s Chief Operating Officer in 2014. Under his stewardship, iM4U launched its first radio station, iM4U fm. This is the station that reaches out to the youth and spearheads social causes. Besides setting up its first radio station, iM4U has also organized large-scale volunteer programs like Volunteer Malaysia, Reach Out Convention and Celebration and Reach Out Run that have successfully engaged volunteers of different age groups and background. Also, under his purview, iM4U initiated its first volunteer network program, ASEAN4U that brought together 70 ASEAN delegates.</p>

                            <p>Rudy is also known as the rock drummer for the popular band, Pop Shuvit. The band has has recorded 6 albums and toured around Asia from the years 2000 to 2012. They won the AIM Anugerah Kembara during their peak and also played on MTV World Stages concerts. They made it big in Japan in 2006. They were the first artist in Malaysia to break into the Japanese market, opening doors to other Malaysian artists and giving hope to the music industry.</p>

                            <p>Rudy, who is appointed as iM4U Chief Executive Officer in 2016, continues to be an inspiration to the youths who want to strike a balance between career and passion. He continues to strive by expanding the presence of iM4U in the heart of Malaysians.</p>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="wernmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header blue-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-5 col-lg-7">
                            <h2 class="yellow-text">TAN SHYUE WERN</h2>
                            <p class="yellow-text">Director of Brand Management Group</p>
                        </div>
                        <div class="col-sm-7 col-lg-4"><img src="img/about/wern.png" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <span class="blue-text">
                            “ I am proud to be part of iM4U, a brand that champions social causes and meaningful activities. It has given me a broader view on volunteerism and deepen my passion for it. ”
                            </span>
                        </div>
                        <div class="col-sm-12">
                            <p>Wern is the Brand Management Group Director who leads iM4U’s branding and corporate communications team.</p>

                            <p>The business administration graduate has more than 13 years of experience in brand management and communications. Started off as a management associate at OSK Holdings Bhd in 2002, he joined BlueScope Steel Asia in 2003 where he was part of the team tasked for marketing and brand management across the Asia Pacific region. In 2006, he moved to Temporal Brand Consulting as a Senior Brand Consultant where he had the opportunity to consult some of the industry leaders in Malaysia, China and Dubai on brand strategies.</p>

                            <p>In 2010, he had the opportunity to head MEASAT Satellite Systems corporate communications. His role involved planning and managing the internal and external communications for MEASAT. Besides that, he also oversaw the corporate social responsibility for the organization. In 2014, he joined InvestKL to propell Greater KL’s position in the international arena as the prefered investment destination. He led the international marketing and communications strategies in promoting Greater KL across Asia, Europe and USA. 2015 marked another progressive move for Wern when he was appointed to lead iM4U’s brand management and corporate communications team.</p>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tinimodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header blue-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-5 col-lg-7">
                            <h2 class="yellow-text">KARTINI ARIFFIN</h2>
                            <p class="yellow-text">Director of iM4U fm</p>
                        </div>
                        <div class="col-sm-7 col-lg-4"><img src="img/about/tini.png" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <span class="blue-text">
                            “ iM4U fm combines popular culture and music to attract youth to spearhead volunteerism and social causes that are close to their hearts. As a social entreprise, we strive to instil positivity thru creative and impactful programming and serve as the voice of all Malaysian youth. ”
                            </span>
                        </div>
                        <div class="col-sm-12">
                            <p>Tini is the Director of iM4U fm, a radio station that spearheads social causes, targeting the youth.</p>

                            <p>Graduated from the Goldsmiths College, University of London with a Master of Arts in Broadcast Journalism and a Bachelor of Science in Computerised Accountancy from the University of East Anglia, United Kingdom, Kartini was known as for her hosting on 3R (Respek, Relaks, Respon), a young women empowerment TV programs for 10 years. Prior to her stint on TV, she was an associate auditor at Price Waters House Coopers.</p>

                            <p>In 2006, she joined Media Prima as a radio announcer before she was promoted as the Music Director in 2010. After being with the station for 7 years, Kartini moved to The Cube, Celcom Bhd as the manager of Cube Radio. She was appointed as an Executive Producer at Red Communications Sdn Bhd, where she oversaw various projects for Astro channels and Media Prima Berhad such as Projek 3R, OME, Hijabista and OMG on Diva Universal.</p>

                            <p>She rejoined the radio industry in 2014 as the Director for iM4U fm. She was one of the pioneers that had helped to launch the station. Under her leadership, iM4U fm continues to path its way as the leading radio station that champions social causes for the youth. Kartini is also lecturing part time in Radio, Documentary and Leadership subjects in a local University.</p>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--  end  -->
   
    <!--  contact us  -->
 <!--
    <div id="contact" class="container-fluid border grey-bg pt-5 pb-5" style="background: url(img/about/contactus.jpg) no-repeat center; background-size:cover">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center p-5">
                    <h1 class="white-text">CONTACT US</h1>
                    <p class="white-text">Contact us for any inquiries of feedback.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-4 mb-4">
                    <div class="contact-box rounded border p-3 box-shadow">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-2 pl-0"><i class="fab fa-bandcamp fa-3x white-text"></i></div>
                            <div class="col-sm-10">
                                <h5 class="white-text mb-4">Branding & PR</h5>
                                <a class="white-text" href="mailto:BMG@im4u.my">Email: BMG@im4u.my</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 mb-4">
                    <div class="contact-box rounded border p-3 box-shadow">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-2 pl-0"><i class="fab fa-black-tie fa-3x white-text"></i></div>
                            <div class="col-sm-10">
                                <h5 class="white-text mb-4">Corporate Services</h5>
                                <a class="white-text" href="mailto:CSD@im4u.my">Email: CSD@im4u.my</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 mb-4">
                    <div class="contact-box rounded border p-3 box-shadow">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-2 pl-0"><i class="fas fa-handshake fa-3x white-text"></i></div>
                            <div class="col-sm-10">
                                <h5 class="white-text">Regulatory & Government Relations</h5>
                                <a class="white-text" href="mailto:rgd@im4u.my">Email: rgd@im4u.my</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 mb-4">
                    <div class="contact-box rounded border p-3 box-shadow">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-2 pl-0"><i class="fab fa-creative-commons fa-3x white-text"></i></div>
                            <div class="col-sm-10">
                                <h5 class="white-text">Content Creation & Integration Department</h5>
                                <a class="white-text" href="mailto:content@im4u.my">Email: content@im4u.my</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 mb-4">
                    <div class="contact-box rounded border p-3 box-shadow">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-2 pl-"><i class="fas fa-male fa-3x white-text"></i></div>
                            <div class="col-sm-10">
                                <h5 class="white-text mb-4">Human Resource</h5>
                                <a class="white-text" href="mailto:hr@im4u.my">Email: hr@im4u.my</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-4 mb-4">
                    <div class="contact-box rounded border p-3 box-shadow">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-sm-2 pl-0"><i class="fas fa-headphones fa-3x white-text"></i></div>
                            <div class="col-sm-10">
                                <h5 class="white-text mb-4">Radio</h5>
                                <a class="white-text" href="mailto:im4uradio@im4u.my">Email: im4uradio@im4u.my</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
-->
    <!--  end  -->


@endsection
@section('js')
@parent

@endsection