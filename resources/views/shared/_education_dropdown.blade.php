<option {{ old('education', $model->education) == "" ? 'selected' : ''}} value="">Please Select</option>
<option {{ old('education', $model->education) == "High School (SPM & STPM)" ? 'selected' : ''}} value="High School (SPM & STPM)">High School (SPM & STPM)</option>
<option {{ old('education', $model->education) == "Matriculation" ? 'selected' : ''}} value="Matriculation">Matriculation</option>
<option {{ old('education', $model->education) == "Diploma" ? 'selected' : ''}} value="Diploma">Diploma</option>
<option {{ old('education', $model->education) == "Degree" ? 'selected' : ''}} value="Degree">Degree</option>
<option {{ old('education', $model->education) == "Master" ? 'selected' : ''}} value="Master">Master</option>
<option {{ old('education', $model->education) == "Doctoral (PHD)" ? 'selected' : ''}} value="Doctoral (PHD)">Doctoral (PHD)</option>
<option {{ old('education', $model->education) == "Other" ? 'selected' : ''}} value="Other">Other</option>
