@if(request('view') == 'trash')
    <a href="{{ route($url.'.restore', $model->id) }}" class="btn btn-info" role="button">Restore</a>
@else

    @can('view_'.$url)
    <a class="btn btn-xs btn-success" href="{{route($url.'.show', $model->id)}}">Show</a>
    @endcan

    @can('edit_'.$url)
    <a class="btn btn-xs btn-warning" href="{{route($url.'.edit', $model->id)}}">Edit</a>
    @endcan
    
    @can('delete_'.$url)
    <a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
    @endcan
@endif