@php
if(!isset($show_hide)){
    $show_hide = false;
}   

if(!isset($edit_hide)){
    $edit_hide = false;
}   

if(!isset($delete_hide)){
    $delete_hide = false;
}   
@endphp


@if($show_hide == false)
@can('view_'.$url)
<a class="btn btn-xs btn-success" href="{{route($url.'.show', $model->id)}}">Show</a>
@endcan
@endif

@if($edit_hide == false)
@can('edit_'.$url)
<a class="btn btn-xs btn-warning" href="{{route($url.'.edit', $model->id)}}">Edit</a>
@endcan
@endif

@if($delete_hide == false)
@can('delete_'.$url)
<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
@endcan
@endif