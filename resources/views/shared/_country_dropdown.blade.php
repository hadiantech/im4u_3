<option {{ old('country', $model->state) == "" ? 'selected' : ''}} value="country">Please Select</option>
<option {{ old('country', $model->country) == "Malaysia" ? 'selected' : ''}} value="Malaysia">Malaysia</option> 
<option {{ old('country', $model->country) == "United States" ? 'selected' : ''}} value="United States">United States</option> 
<option {{ old('country', $model->country) == "United Kingdom" ? 'selected' : ''}} value="United Kingdom">United Kingdom</option> 
<option {{ old('country', $model->country) == "Afghanistan" ? 'selected' : ''}} value="Afghanistan">Afghanistan</option> 
<option {{ old('country', $model->country) == "Albania" ? 'selected' : ''}} value="Albania">Albania</option> 
<option {{ old('country', $model->country) == "Algeria" ? 'selected' : ''}} value="Algeria">Algeria</option> 
<option {{ old('country', $model->country) == "American Samoa" ? 'selected' : ''}} value="American Samoa">American Samoa</option> 
<option {{ old('country', $model->country) == "Andorra" ? 'selected' : ''}} value="Andorra">Andorra</option> 
<option {{ old('country', $model->country) == "Angola" ? 'selected' : ''}} value="Angola">Angola</option> 
<option {{ old('country', $model->country) == "Anguilla" ? 'selected' : ''}} value="Anguilla">Anguilla</option> 
<option {{ old('country', $model->country) == "Antarctica" ? 'selected' : ''}} value="Antarctica">Antarctica</option> 
<option {{ old('country', $model->country) == "Antigua and Barbuda" ? 'selected' : ''}} value="Antigua and Barbuda">Antigua and Barbuda</option> 
<option {{ old('country', $model->country) == "Argentina" ? 'selected' : ''}} value="Argentina">Argentina</option> 
<option {{ old('country', $model->country) == "Armenia" ? 'selected' : ''}} value="Armenia">Armenia</option> 
<option {{ old('country', $model->country) == "Aruba" ? 'selected' : ''}} value="Aruba">Aruba</option> 
<option {{ old('country', $model->country) == "Australia" ? 'selected' : ''}} value="Australia">Australia</option> 
<option {{ old('country', $model->country) == "Austria" ? 'selected' : ''}} value="Austria">Austria</option> 
<option {{ old('country', $model->country) == "Azerbaijan" ? 'selected' : ''}} value="Azerbaijan">Azerbaijan</option> 
<option {{ old('country', $model->country) == "Bahamas" ? 'selected' : ''}} value="Bahamas">Bahamas</option> 
<option {{ old('country', $model->country) == "Bahrain" ? 'selected' : ''}} value="Bahrain">Bahrain</option> 
<option {{ old('country', $model->country) == "Bangladesh" ? 'selected' : ''}} value="Bangladesh">Bangladesh</option> 
<option {{ old('country', $model->country) == "Barbados" ? 'selected' : ''}} value="Barbados">Barbados</option> 
<option {{ old('country', $model->country) == "Belarus" ? 'selected' : ''}} value="Belarus">Belarus</option> 
<option {{ old('country', $model->country) == "Belgium" ? 'selected' : ''}} value="Belgium">Belgium</option> 
<option {{ old('country', $model->country) == "Belize" ? 'selected' : ''}} value="Belize">Belize</option> 
<option {{ old('country', $model->country) == "Benin" ? 'selected' : ''}} value="Benin">Benin</option> 
<option {{ old('country', $model->country) == "Bermuda" ? 'selected' : ''}} value="Bermuda">Bermuda</option> 
<option {{ old('country', $model->country) == "Bhutan" ? 'selected' : ''}} value="Bhutan">Bhutan</option> 
<option {{ old('country', $model->country) == "Bolivia" ? 'selected' : ''}} value="Bolivia">Bolivia</option> 
<option {{ old('country', $model->country) == "Bosnia and Herzegovina" ? 'selected' : ''}} value="Bosnia and Herzegovina">Bosnia and Herzegovina</option> 
<option {{ old('country', $model->country) == "Botswana" ? 'selected' : ''}} value="Botswana">Botswana</option> 
<option {{ old('country', $model->country) == "Bouvet Island" ? 'selected' : ''}} value="Bouvet Island">Bouvet Island</option> 
<option {{ old('country', $model->country) == "Brazil" ? 'selected' : ''}} value="Brazil">Brazil</option> 
<option {{ old('country', $model->country) == "British Indian Ocean Territory" ? 'selected' : ''}} value="British Indian Ocean Territory">British Indian Ocean Territory</option> 
<option {{ old('country', $model->country) == "Brunei Darussalam" ? 'selected' : ''}} value="Brunei Darussalam">Brunei Darussalam</option> 
<option {{ old('country', $model->country) == "Bulgaria" ? 'selected' : ''}} value="Bulgaria">Bulgaria</option> 
<option {{ old('country', $model->country) == "Burkina Faso" ? 'selected' : ''}} value="Burkina Faso">Burkina Faso</option> 
<option {{ old('country', $model->country) == "Burundi" ? 'selected' : ''}} value="Burundi">Burundi</option> 
<option {{ old('country', $model->country) == "Cambodia" ? 'selected' : ''}} value="Cambodia">Cambodia</option> 
<option {{ old('country', $model->country) == "Cameroon" ? 'selected' : ''}} value="Cameroon">Cameroon</option> 
<option {{ old('country', $model->country) == "Canada" ? 'selected' : ''}} value="Canada">Canada</option> 
<option {{ old('country', $model->country) == "Cape Verde" ? 'selected' : ''}} value="Cape Verde">Cape Verde</option> 
<option {{ old('country', $model->country) == "Cayman Islands" ? 'selected' : ''}} value="Cayman Islands">Cayman Islands</option> 
<option {{ old('country', $model->country) == "Central African Republic" ? 'selected' : ''}} value="Central African Republic">Central African Republic</option> 
<option {{ old('country', $model->country) == "Chad" ? 'selected' : ''}} value="Chad">Chad</option> 
<option {{ old('country', $model->country) == "Chile" ? 'selected' : ''}} value="Chile">Chile</option> 
<option {{ old('country', $model->country) == "China" ? 'selected' : ''}} value="China">China</option> 
<option {{ old('country', $model->country) == "Christmas Island" ? 'selected' : ''}} value="Christmas Island">Christmas Island</option> 
<option {{ old('country', $model->country) == "Cocos (Keeling) Islands" ? 'selected' : ''}} value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option> 
<option {{ old('country', $model->country) == "Colombia" ? 'selected' : ''}} value="Colombia">Colombia</option> 
<option {{ old('country', $model->country) == "Comoros" ? 'selected' : ''}} value="Comoros">Comoros</option> 
<option {{ old('country', $model->country) == "Congo" ? 'selected' : ''}} value="Congo">Congo</option> 
<option {{ old('country', $model->country) == "Congo, The Democratic Republic of The" ? 'selected' : ''}} value="Congo, The Democratic Republic of The">Congo, The Democratic Republic of The</option> 
<option {{ old('country', $model->country) == "Cook Islands" ? 'selected' : ''}} value="Cook Islands">Cook Islands</option> 
<option {{ old('country', $model->country) == "Costa Rica" ? 'selected' : ''}} value="Costa Rica">Costa Rica</option> 
<option {{ old('country', $model->country) == "Cote D'ivoire" ? 'selected' : ''}} value="Cote D'ivoire">Cote D'ivoire</option> 
<option {{ old('country', $model->country) == "Croatia" ? 'selected' : ''}} value="Croatia">Croatia</option> 
<option {{ old('country', $model->country) == "Cuba" ? 'selected' : ''}} value="Cuba">Cuba</option> 
<option {{ old('country', $model->country) == "Cyprus" ? 'selected' : ''}} value="Cyprus">Cyprus</option> 
<option {{ old('country', $model->country) == "Czech Republic" ? 'selected' : ''}} value="Czech Republic">Czech Republic</option> 
<option {{ old('country', $model->country) == "Denmark" ? 'selected' : ''}} value="Denmark">Denmark</option> 
<option {{ old('country', $model->country) == "Djibouti" ? 'selected' : ''}} value="Djibouti">Djibouti</option> 
<option {{ old('country', $model->country) == "Dominica" ? 'selected' : ''}} value="Dominica">Dominica</option> 
<option {{ old('country', $model->country) == "Dominican Republic" ? 'selected' : ''}} value="Dominican Republic">Dominican Republic</option> 
<option {{ old('country', $model->country) == "Ecuador" ? 'selected' : ''}} value="Ecuador">Ecuador</option> 
<option {{ old('country', $model->country) == "Egypt" ? 'selected' : ''}} value="Egypt">Egypt</option> 
<option {{ old('country', $model->country) == "El Salvador" ? 'selected' : ''}} value="El Salvador">El Salvador</option> 
<option {{ old('country', $model->country) == "Equatorial Guinea" ? 'selected' : ''}} value="Equatorial Guinea">Equatorial Guinea</option> 
<option {{ old('country', $model->country) == "Eritrea" ? 'selected' : ''}} value="Eritrea">Eritrea</option> 
<option {{ old('country', $model->country) == "Estonia" ? 'selected' : ''}} value="Estonia">Estonia</option> 
<option {{ old('country', $model->country) == "Ethiopia" ? 'selected' : ''}} value="Ethiopia">Ethiopia</option> 
<option {{ old('country', $model->country) == "Falkland Islands (Malvinas)" ? 'selected' : ''}} value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option> 
<option {{ old('country', $model->country) == "Faroe Islands" ? 'selected' : ''}} value="Faroe Islands">Faroe Islands</option> 
<option {{ old('country', $model->country) == "Fiji" ? 'selected' : ''}} value="Fiji">Fiji</option> 
<option {{ old('country', $model->country) == "Finland" ? 'selected' : ''}} value="Finland">Finland</option> 
<option {{ old('country', $model->country) == "France" ? 'selected' : ''}} value="France">France</option> 
<option {{ old('country', $model->country) == "French Guiana" ? 'selected' : ''}} value="French Guiana">French Guiana</option> 
<option {{ old('country', $model->country) == "French Polynesia" ? 'selected' : ''}} value="French Polynesia">French Polynesia</option> 
<option {{ old('country', $model->country) == "French Southern Territories" ? 'selected' : ''}} value="French Southern Territories">French Southern Territories</option> 
<option {{ old('country', $model->country) == "Gabon" ? 'selected' : ''}} value="Gabon">Gabon</option> 
<option {{ old('country', $model->country) == "Gambia" ? 'selected' : ''}} value="Gambia">Gambia</option> 
<option {{ old('country', $model->country) == "Georgia" ? 'selected' : ''}} value="Georgia">Georgia</option> 
<option {{ old('country', $model->country) == "Germany" ? 'selected' : ''}} value="Germany">Germany</option> 
<option {{ old('country', $model->country) == "Ghana" ? 'selected' : ''}} value="Ghana">Ghana</option> 
<option {{ old('country', $model->country) == "Gibraltar" ? 'selected' : ''}} value="Gibraltar">Gibraltar</option> 
<option {{ old('country', $model->country) == "Greece" ? 'selected' : ''}} value="Greece">Greece</option> 
<option {{ old('country', $model->country) == "Greenland" ? 'selected' : ''}} value="Greenland">Greenland</option> 
<option {{ old('country', $model->country) == "Grenada" ? 'selected' : ''}} value="Grenada">Grenada</option> 
<option {{ old('country', $model->country) == "Guadeloupe" ? 'selected' : ''}} value="Guadeloupe">Guadeloupe</option> 
<option {{ old('country', $model->country) == "Guam" ? 'selected' : ''}} value="Guam">Guam</option> 
<option {{ old('country', $model->country) == "Guatemala" ? 'selected' : ''}} value="Guatemala">Guatemala</option> 
<option {{ old('country', $model->country) == "Guinea" ? 'selected' : ''}} value="Guinea">Guinea</option> 
<option {{ old('country', $model->country) == "Guinea-bissau" ? 'selected' : ''}} value="Guinea-bissau">Guinea-bissau</option> 
<option {{ old('country', $model->country) == "Guyana" ? 'selected' : ''}} value="Guyana">Guyana</option> 
<option {{ old('country', $model->country) == "Haiti" ? 'selected' : ''}} value="Haiti">Haiti</option> 
<option {{ old('country', $model->country) == "Heard Island and Mcdonald Islands" ? 'selected' : ''}} value="Heard Island and Mcdonald Islands">Heard Island and Mcdonald Islands</option> 
<option {{ old('country', $model->country) == "Holy See (Vatican City State)" ? 'selected' : ''}} value="Holy See (Vatican City State)">Holy See (Vatican City State)</option> 
<option {{ old('country', $model->country) == "Honduras" ? 'selected' : ''}} value="Honduras">Honduras</option> 
<option {{ old('country', $model->country) == "Hong Kong" ? 'selected' : ''}} value="Hong Kong">Hong Kong</option> 
<option {{ old('country', $model->country) == "Hungary" ? 'selected' : ''}} value="Hungary">Hungary</option> 
<option {{ old('country', $model->country) == "Iceland" ? 'selected' : ''}} value="Iceland">Iceland</option> 
<option {{ old('country', $model->country) == "India" ? 'selected' : ''}} value="India">India</option> 
<option {{ old('country', $model->country) == "Indonesia" ? 'selected' : ''}} value="Indonesia">Indonesia</option> 
<option {{ old('country', $model->country) == "Iran, Islamic Republic of" ? 'selected' : ''}} value="Iran, Islamic Republic of">Iran, Islamic Republic of</option> 
<option {{ old('country', $model->country) == "Iraq" ? 'selected' : ''}} value="Iraq">Iraq</option> 
<option {{ old('country', $model->country) == "Ireland" ? 'selected' : ''}} value="Ireland">Ireland</option> 
<option {{ old('country', $model->country) == "Israel" ? 'selected' : ''}} value="Israel">Israel</option> 
<option {{ old('country', $model->country) == "Italy" ? 'selected' : ''}} value="Italy">Italy</option> 
<option {{ old('country', $model->country) == "Jamaica" ? 'selected' : ''}} value="Jamaica">Jamaica</option> 
<option {{ old('country', $model->country) == "Japan" ? 'selected' : ''}} value="Japan">Japan</option> 
<option {{ old('country', $model->country) == "Jordan" ? 'selected' : ''}} value="Jordan">Jordan</option> 
<option {{ old('country', $model->country) == "Kazakhstan" ? 'selected' : ''}} value="Kazakhstan">Kazakhstan</option> 
<option {{ old('country', $model->country) == "Kenya" ? 'selected' : ''}} value="Kenya">Kenya</option> 
<option {{ old('country', $model->country) == "Kiribati" ? 'selected' : ''}} value="Kiribati">Kiribati</option> 
<option {{ old('country', $model->country) == "Korea, Democratic People's Republic of" ? 'selected' : ''}} value="Korea, Democratic People's Republic of">Korea, Democratic People's Republic of</option> 
<option {{ old('country', $model->country) == "Korea, Republic of" ? 'selected' : ''}} value="Korea, Republic of">Korea, Republic of</option> 
<option {{ old('country', $model->country) == "Kuwait" ? 'selected' : ''}} value="Kuwait">Kuwait</option> 
<option {{ old('country', $model->country) == "Kyrgyzstan" ? 'selected' : ''}} value="Kyrgyzstan">Kyrgyzstan</option> 
<option {{ old('country', $model->country) == "Lao People's Democratic Republic" ? 'selected' : ''}} value="Lao People's Democratic Republic">Lao People's Democratic Republic</option> 
<option {{ old('country', $model->country) == "Latvia" ? 'selected' : ''}} value="Latvia">Latvia</option> 
<option {{ old('country', $model->country) == "Lebanon" ? 'selected' : ''}} value="Lebanon">Lebanon</option> 
<option {{ old('country', $model->country) == "Lesotho" ? 'selected' : ''}} value="Lesotho">Lesotho</option> 
<option {{ old('country', $model->country) == "Liberia" ? 'selected' : ''}} value="Liberia">Liberia</option> 
<option {{ old('country', $model->country) == "Libyan Arab Jamahiriya" ? 'selected' : ''}} value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option> 
<option {{ old('country', $model->country) == "Liechtenstein" ? 'selected' : ''}} value="Liechtenstein">Liechtenstein</option> 
<option {{ old('country', $model->country) == "Lithuania" ? 'selected' : ''}} value="Lithuania">Lithuania</option> 
<option {{ old('country', $model->country) == "Luxembourg" ? 'selected' : ''}} value="Luxembourg">Luxembourg</option> 
<option {{ old('country', $model->country) == "Macao" ? 'selected' : ''}} value="Macao">Macao</option> 
<option {{ old('country', $model->country) == "Macedonia, The Former Yugoslav Republic of" ? 'selected' : ''}} value="Macedonia, The Former Yugoslav Republic of">Macedonia, The Former Yugoslav Republic of</option> 
<option {{ old('country', $model->country) == "Madagascar" ? 'selected' : ''}} value="Madagascar">Madagascar</option> 
<option {{ old('country', $model->country) == "Malawi" ? 'selected' : ''}} value="Malawi">Malawi</option> 
<option {{ old('country', $model->country) == "Maldives" ? 'selected' : ''}} value="Maldives">Maldives</option> 
<option {{ old('country', $model->country) == "Mali" ? 'selected' : ''}} value="Mali">Mali</option> 
<option {{ old('country', $model->country) == "Malta" ? 'selected' : ''}} value="Malta">Malta</option> 
<option {{ old('country', $model->country) == "Marshall Islands" ? 'selected' : ''}} value="Marshall Islands">Marshall Islands</option> 
<option {{ old('country', $model->country) == "Martinique" ? 'selected' : ''}} value="Martinique">Martinique</option> 
<option {{ old('country', $model->country) == "Mauritania" ? 'selected' : ''}} value="Mauritania">Mauritania</option> 
<option {{ old('country', $model->country) == "Mauritius" ? 'selected' : ''}} value="Mauritius">Mauritius</option> 
<option {{ old('country', $model->country) == "Mayotte" ? 'selected' : ''}} value="Mayotte">Mayotte</option> 
<option {{ old('country', $model->country) == "Mexico" ? 'selected' : ''}} value="Mexico">Mexico</option> 
<option {{ old('country', $model->country) == "Micronesia, Federated States of" ? 'selected' : ''}} value="Micronesia, Federated States of">Micronesia, Federated States of</option> 
<option {{ old('country', $model->country) == "Moldova, Republic of" ? 'selected' : ''}} value="Moldova, Republic of">Moldova, Republic of</option> 
<option {{ old('country', $model->country) == "Monaco" ? 'selected' : ''}} value="Monaco">Monaco</option> 
<option {{ old('country', $model->country) == "Mongolia" ? 'selected' : ''}} value="Mongolia">Mongolia</option> 
<option {{ old('country', $model->country) == "Montserrat" ? 'selected' : ''}} value="Montserrat">Montserrat</option> 
<option {{ old('country', $model->country) == "Morocco" ? 'selected' : ''}} value="Morocco">Morocco</option> 
<option {{ old('country', $model->country) == "Mozambique" ? 'selected' : ''}} value="Mozambique">Mozambique</option> 
<option {{ old('country', $model->country) == "Myanmar" ? 'selected' : ''}} value="Myanmar">Myanmar</option> 
<option {{ old('country', $model->country) == "Namibia" ? 'selected' : ''}} value="Namibia">Namibia</option> 
<option {{ old('country', $model->country) == "Nauru" ? 'selected' : ''}} value="Nauru">Nauru</option> 
<option {{ old('country', $model->country) == "Nepal" ? 'selected' : ''}} value="Nepal">Nepal</option> 
<option {{ old('country', $model->country) == "Netherlands" ? 'selected' : ''}} value="Netherlands">Netherlands</option> 
<option {{ old('country', $model->country) == "Netherlands Antilles" ? 'selected' : ''}} value="Netherlands Antilles">Netherlands Antilles</option> 
<option {{ old('country', $model->country) == "New Caledonia" ? 'selected' : ''}} value="New Caledonia">New Caledonia</option> 
<option {{ old('country', $model->country) == "New Zealand" ? 'selected' : ''}} value="New Zealand">New Zealand</option> 
<option {{ old('country', $model->country) == "Nicaragua" ? 'selected' : ''}} value="Nicaragua">Nicaragua</option> 
<option {{ old('country', $model->country) == "Niger" ? 'selected' : ''}} value="Niger">Niger</option> 
<option {{ old('country', $model->country) == "Nigeria" ? 'selected' : ''}} value="Nigeria">Nigeria</option> 
<option {{ old('country', $model->country) == "Niue" ? 'selected' : ''}} value="Niue">Niue</option> 
<option {{ old('country', $model->country) == "Norfolk Island" ? 'selected' : ''}} value="Norfolk Island">Norfolk Island</option> 
<option {{ old('country', $model->country) == "Northern Mariana Islands" ? 'selected' : ''}} value="Northern Mariana Islands">Northern Mariana Islands</option> 
<option {{ old('country', $model->country) == "Norway" ? 'selected' : ''}} value="Norway">Norway</option> 
<option {{ old('country', $model->country) == "Oman" ? 'selected' : ''}} value="Oman">Oman</option> 
<option {{ old('country', $model->country) == "Pakistan" ? 'selected' : ''}} value="Pakistan">Pakistan</option> 
<option {{ old('country', $model->country) == "Palau" ? 'selected' : ''}} value="Palau">Palau</option> 
<option {{ old('country', $model->country) == "Palestinian Territory, Occupied" ? 'selected' : ''}} value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option> 
<option {{ old('country', $model->country) == "Panama" ? 'selected' : ''}} value="Panama">Panama</option> 
<option {{ old('country', $model->country) == "Papua New Guinea" ? 'selected' : ''}} value="Papua New Guinea">Papua New Guinea</option> 
<option {{ old('country', $model->country) == "Paraguay" ? 'selected' : ''}} value="Paraguay">Paraguay</option> 
<option {{ old('country', $model->country) == "Peru" ? 'selected' : ''}} value="Peru">Peru</option> 
<option {{ old('country', $model->country) == "Philippines" ? 'selected' : ''}} value="Philippines">Philippines</option> 
<option {{ old('country', $model->country) == "Pitcairn" ? 'selected' : ''}} value="Pitcairn">Pitcairn</option> 
<option {{ old('country', $model->country) == "Poland" ? 'selected' : ''}} value="Poland">Poland</option> 
<option {{ old('country', $model->country) == "Portugal" ? 'selected' : ''}} value="Portugal">Portugal</option> 
<option {{ old('country', $model->country) == "Puerto Rico" ? 'selected' : ''}} value="Puerto Rico">Puerto Rico</option> 
<option {{ old('country', $model->country) == "Qatar" ? 'selected' : ''}} value="Qatar">Qatar</option> 
<option {{ old('country', $model->country) == "Reunion" ? 'selected' : ''}} value="Reunion">Reunion</option> 
<option {{ old('country', $model->country) == "Romania" ? 'selected' : ''}} value="Romania">Romania</option> 
<option {{ old('country', $model->country) == "Russian Federation" ? 'selected' : ''}} value="Russian Federation">Russian Federation</option> 
<option {{ old('country', $model->country) == "Rwanda" ? 'selected' : ''}} value="Rwanda">Rwanda</option> 
<option {{ old('country', $model->country) == "Saint Helena" ? 'selected' : ''}} value="Saint Helena">Saint Helena</option> 
<option {{ old('country', $model->country) == "Saint Kitts and Nevis" ? 'selected' : ''}} value="Saint Kitts and Nevis">Saint Kitts and Nevis</option> 
<option {{ old('country', $model->country) == "Saint Lucia" ? 'selected' : ''}} value="Saint Lucia">Saint Lucia</option> 
<option {{ old('country', $model->country) == "Saint Pierre and Miquelon" ? 'selected' : ''}} value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option> 
<option {{ old('country', $model->country) == "Saint Vincent and The Grenadines" ? 'selected' : ''}} value="Saint Vincent and The Grenadines">Saint Vincent and The Grenadines</option> 
<option {{ old('country', $model->country) == "Samoa" ? 'selected' : ''}} value="Samoa">Samoa</option> 
<option {{ old('country', $model->country) == "San Marino" ? 'selected' : ''}} value="San Marino">San Marino</option> 
<option {{ old('country', $model->country) == "Sao Tome and Principe" ? 'selected' : ''}} value="Sao Tome and Principe">Sao Tome and Principe</option> 
<option {{ old('country', $model->country) == "Saudi Arabia" ? 'selected' : ''}} value="Saudi Arabia">Saudi Arabia</option> 
<option {{ old('country', $model->country) == "Senegal" ? 'selected' : ''}} value="Senegal">Senegal</option> 
<option {{ old('country', $model->country) == "Serbia and Montenegro" ? 'selected' : ''}} value="Serbia and Montenegro">Serbia and Montenegro</option> 
<option {{ old('country', $model->country) == "Seychelles" ? 'selected' : ''}} value="Seychelles">Seychelles</option> 
<option {{ old('country', $model->country) == "Sierra Leone" ? 'selected' : ''}} value="Sierra Leone">Sierra Leone</option> 
<option {{ old('country', $model->country) == "Singapore" ? 'selected' : ''}} value="Singapore">Singapore</option> 
<option {{ old('country', $model->country) == "Slovakia" ? 'selected' : ''}} value="Slovakia">Slovakia</option> 
<option {{ old('country', $model->country) == "Slovenia" ? 'selected' : ''}} value="Slovenia">Slovenia</option> 
<option {{ old('country', $model->country) == "Solomon Islands" ? 'selected' : ''}} value="Solomon Islands">Solomon Islands</option> 
<option {{ old('country', $model->country) == "Somalia" ? 'selected' : ''}} value="Somalia">Somalia</option> 
<option {{ old('country', $model->country) == "South Africa" ? 'selected' : ''}} value="South Africa">South Africa</option> 
<option {{ old('country', $model->country) == "South Georgia and The South Sandwich Islands" ? 'selected' : ''}} value="South Georgia and The South Sandwich Islands">South Georgia and The South Sandwich Islands</option> 
<option {{ old('country', $model->country) == "Spain" ? 'selected' : ''}} value="Spain">Spain</option> 
<option {{ old('country', $model->country) == "Sri Lanka" ? 'selected' : ''}} value="Sri Lanka">Sri Lanka</option> 
<option {{ old('country', $model->country) == "Sudan" ? 'selected' : ''}} value="Sudan">Sudan</option> 
<option {{ old('country', $model->country) == "Suriname" ? 'selected' : ''}} value="Suriname">Suriname</option> 
<option {{ old('country', $model->country) == "Svalbard and Jan Mayen" ? 'selected' : ''}} value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option> 
<option {{ old('country', $model->country) == "Swaziland" ? 'selected' : ''}} value="Swaziland">Swaziland</option> 
<option {{ old('country', $model->country) == "Sweden" ? 'selected' : ''}} value="Sweden">Sweden</option> 
<option {{ old('country', $model->country) == "Switzerland" ? 'selected' : ''}} value="Switzerland">Switzerland</option> 
<option {{ old('country', $model->country) == "Syrian Arab Republic" ? 'selected' : ''}} value="Syrian Arab Republic">Syrian Arab Republic</option> 
<option {{ old('country', $model->country) == "Taiwan, Province of China" ? 'selected' : ''}} value="Taiwan, Province of China">Taiwan, Province of China</option> 
<option {{ old('country', $model->country) == "Tajikistan" ? 'selected' : ''}} value="Tajikistan">Tajikistan</option> 
<option {{ old('country', $model->country) == "Tanzania, United Republic of" ? 'selected' : ''}} value="Tanzania, United Republic of">Tanzania, United Republic of</option> 
<option {{ old('country', $model->country) == "Thailand" ? 'selected' : ''}} value="Thailand">Thailand</option> 
<option {{ old('country', $model->country) == "Timor-leste" ? 'selected' : ''}} value="Timor-leste">Timor-leste</option> 
<option {{ old('country', $model->country) == "Togo" ? 'selected' : ''}} value="Togo">Togo</option> 
<option {{ old('country', $model->country) == "Tokelau" ? 'selected' : ''}} value="Tokelau">Tokelau</option> 
<option {{ old('country', $model->country) == "Tonga" ? 'selected' : ''}} value="Tonga">Tonga</option> 
<option {{ old('country', $model->country) == "Trinidad and Tobago" ? 'selected' : ''}} value="Trinidad and Tobago">Trinidad and Tobago</option> 
<option {{ old('country', $model->country) == "Tunisia" ? 'selected' : ''}} value="Tunisia">Tunisia</option> 
<option {{ old('country', $model->country) == "Turkey" ? 'selected' : ''}} value="Turkey">Turkey</option> 
<option {{ old('country', $model->country) == "Turkmenistan" ? 'selected' : ''}} value="Turkmenistan">Turkmenistan</option> 
<option {{ old('country', $model->country) == "Turks and Caicos Islands" ? 'selected' : ''}} value="Turks and Caicos Islands">Turks and Caicos Islands</option> 
<option {{ old('country', $model->country) == "Tuvalu" ? 'selected' : ''}} value="Tuvalu">Tuvalu</option> 
<option {{ old('country', $model->country) == "Uganda" ? 'selected' : ''}} value="Uganda">Uganda</option> 
<option {{ old('country', $model->country) == "Ukraine" ? 'selected' : ''}} value="Ukraine">Ukraine</option> 
<option {{ old('country', $model->country) == "United Arab Emirates" ? 'selected' : ''}} value="United Arab Emirates">United Arab Emirates</option> 
<option {{ old('country', $model->country) == "United Kingdom" ? 'selected' : ''}} value="United Kingdom">United Kingdom</option> 
<option {{ old('country', $model->country) == "United States" ? 'selected' : ''}} value="United States">United States</option> 
<option {{ old('country', $model->country) == "United States Minor Outlying Islands" ? 'selected' : ''}} value="United States Minor Outlying Islands">United States Minor Outlying Islands</option> 
<option {{ old('country', $model->country) == "Uruguay" ? 'selected' : ''}} value="Uruguay">Uruguay</option> 
<option {{ old('country', $model->country) == "Uzbekistan" ? 'selected' : ''}} value="Uzbekistan">Uzbekistan</option> 
<option {{ old('country', $model->country) == "Vanuatu" ? 'selected' : ''}} value="Vanuatu">Vanuatu</option> 
<option {{ old('country', $model->country) == "Venezuela" ? 'selected' : ''}} value="Venezuela">Venezuela</option> 
<option {{ old('country', $model->country) == "Viet Nam" ? 'selected' : ''}} value="Viet Nam">Viet Nam</option> 
<option {{ old('country', $model->country) == "Virgin Islands, British" ? 'selected' : ''}} value="Virgin Islands, British">Virgin Islands, British</option> 
<option {{ old('country', $model->country) == "Virgin Islands, U.S." ? 'selected' : ''}} value="Virgin Islands, U.S.">Virgin Islands, U.S.</option> 
<option {{ old('country', $model->country) == "Wallis and Futuna" ? 'selected' : ''}} value="Wallis and Futuna">Wallis and Futuna</option> 
<option {{ old('country', $model->country) == "Western Sahara" ? 'selected' : ''}} value="Western Sahara">Western Sahara</option> 
<option {{ old('country', $model->country) == "Yemen" ? 'selected' : ''}} value="Yemen">Yemen</option> 
<option {{ old('country', $model->country) == "Zambia" ? 'selected' : ''}} value="Zambia">Zambia</option> 
<option {{ old('country', $model->country) == "Zimbabwe" ? 'selected' : ''}} value="Zimbabwe">Zimbabwe</option>