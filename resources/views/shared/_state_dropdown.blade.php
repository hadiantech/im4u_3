<option value="">STATE</option>

@if(isset($model))
<option {{ old('state', $model->state) == "WILAYAH PERSEKUTUAN KUALA LUMPUR" ? 'selected' : ''}} value="WILAYAH PERSEKUTUAN KUALA LUMPUR">WILAYAH PERSEKUTUAN KUALA LUMPUR</option>
<option {{ old('state', $model->state) == "WILAYAH PERSEKUTUAN PUTRAJAYA" ? 'selected' : ''}} value="WILAYAH PERSEKUTUAN PUTRAJAYA">WILAYAH PERSEKUTUAN PUTRAJAYA</option>
<option {{ old('state', $model->state) == "SELANGOR" ? 'selected' : ''}} value="SELANGOR">SELANGOR</option>
<option {{ old('state', $model->state) == "KEDAH" ? 'selected' : ''}} value="KEDAH">KEDAH</option>
<option {{ old('state', $model->state) == "KELANTAN" ? 'selected' : ''}} value="KELANTAN">KELANTAN</option>
<option {{ old('state', $model->state) == "MELAKA" ? 'selected' : ''}} value="MELAKA">MELAKA</option>
<option {{ old('state', $model->state) == "NEGERI SEMBILAN" ? 'selected' : ''}} value="NEGERI SEMBILAN">NEGERI SEMBILAN</option>
<option {{ old('state', $model->state) == "PAHANG" ? 'selected' : ''}} value="PAHANG">PAHANG</option>
<option {{ old('state', $model->state) == "PENANG" ? 'selected' : ''}} value="PENANG">PENANG</option>
<option {{ old('state', $model->state) == "PERAK" ? 'selected' : ''}} value="PERAK">PERAK</option>
<option {{ old('state', $model->state) == "PERLIS" ? 'selected' : ''}} value="PERLIS">PERLIS</option>
<option {{ old('state', $model->state) == "SABAH" ? 'selected' : ''}} value="SABAH">SABAH</option>
<option {{ old('state', $model->state) == "SARAWAK" ? 'selected' : ''}} value="SARAWAK">SARAWAK</option>
<option {{ old('state', $model->state) == "TERENGGANU" ? 'selected' : ''}} value="TERENGGANU">TERENGGANU</option>
<option {{ old('state', $model->state) == "JOHOR" ? 'selected' : ''}} value="JOHOR">JOHOR</option>
<option {{ old('state', $model->state) == "WILAYAH PERSEKUTUAN LABUAN" ? 'selected' : ''}} value="WILAYAH PERSEKUTUAN LABUAN">WILAYAH PERSEKUTUAN LABUAN</option>+

@else

<option {{ old('state') == "WILAYAH PERSEKUTUAN KUALA LUMPUR" ? 'selected' : ''}} value="WILAYAH PERSEKUTUAN KUALA LUMPUR">WILAYAH PERSEKUTUAN KUALA LUMPUR</option>
<option {{ old('state') == "WILAYAH PERSEKUTUAN PUTRAJAYA" ? 'selected' : ''}} value="WILAYAH PERSEKUTUAN PUTRAJAYA">WILAYAH PERSEKUTUAN PUTRAJAYA</option>
<option {{ old('state') == "SELANGOR" ? 'selected' : ''}} value="SELANGOR">SELANGOR</option>
<option {{ old('state') == "KEDAH" ? 'selected' : ''}} value="KEDAH">KEDAH</option>
<option {{ old('state') == "KELANTAN" ? 'selected' : ''}} value="KELANTAN">KELANTAN</option>
<option {{ old('state') == "MELAKA" ? 'selected' : ''}} value="MELAKA">MELAKA</option>
<option {{ old('state') == "NEGERI SEMBILAN" ? 'selected' : ''}} value="NEGERI SEMBILAN">NEGERI SEMBILAN</option>
<option {{ old('state') == "PAHANG" ? 'selected' : ''}} value="PAHANG">PAHANG</option>
<option {{ old('state') == "PENANG" ? 'selected' : ''}} value="PENANG">PENANG</option>
<option {{ old('state') == "PERAK" ? 'selected' : ''}} value="PERAK">PERAK</option>
<option {{ old('state') == "PERLIS" ? 'selected' : ''}} value="PERLIS">PERLIS</option>
<option {{ old('state') == "SABAH" ? 'selected' : ''}} value="SABAH">SABAH</option>
<option {{ old('state') == "SARAWAK" ? 'selected' : ''}} value="SARAWAK">SARAWAK</option>
<option {{ old('state') == "TERENGGANU" ? 'selected' : ''}} value="TERENGGANU">TERENGGANU</option>
<option {{ old('state') == "JOHOR" ? 'selected' : ''}} value="JOHOR">JOHOR</option>
<option {{ old('state') == "WILAYAH PERSEKUTUAN LABUAN" ? 'selected' : ''}} value="WILAYAH PERSEKUTUAN LABUAN">WILAYAH PERSEKUTUAN LABUAN</option>

@endif