{!! Form::open(['url' => route('upload.image'), 'files' => 'true', 'class' => 'col-md-12 mx-0 px-0']) !!}

    <div class="input-group mb-3">
        <div class="custom-file">
            {{ Form::file('image' ,['class' => "custom-file-input ", 'id' => "inputGroupFile01", 'accept'=>"image/*"]) }}
            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
        </div>
    </div>


    {!! Form::hidden('model_name', $model_name) !!}    
    {!! Form::hidden('model_id', $model_id) !!}    
    {!! Form::hidden('collection', $collection) !!}    

    {!! Form::submit('Upload Image', ['class' => 'btn btn-primary btn-block']) !!}    

{!! Form::close() !!}

@section('js')
@parent

<script>
    $('#inputGroupFile01').on('change',function(){
        var fileName = $(this).val();
        $(this).next('.custom-file-label').html(fileName);
    })
</script>

@endsection