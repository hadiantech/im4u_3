<option {{ old('profession', $model->profession) == "Auditing" ? 'selected' : ''}} value="Auditing">Auditing</option>
<option {{ old('profession', $model->profession) == "Management" ? 'selected' : ''}} value="Management">Management</option>
<option {{ old('profession', $model->profession) == "Receivership/Liquidation" ? 'selected' : ''}} value="Receivership/Liquidation">Receivership/Liquidation</option>
<option {{ old('profession', $model->profession) == "Taxation" ? 'selected' : ''}} value="Taxation">Taxation</option>
<option {{ old('profession', $model->profession) == "Analyst" ? 'selected' : ''}} value="Analyst">Analyst</option>
<option {{ old('profession', $model->profession) == "Economist" ? 'selected' : ''}} value="Economist">Economist</option>
<option {{ old('profession', $model->profession) == "Credit Management" ? 'selected' : ''}} value="Credit Management">Credit Management</option>
<option {{ old('profession', $model->profession) == "Corporate Banking" ? 'selected' : ''}} value="Corporate Banking">Corporate Banking</option>
<option {{ old('profession', $model->profession) == "Financial Planning/Wealth Management" ? 'selected' : ''}} value="Financial Planning/Wealth Management">Corporate Banking</option>
<option {{ old('profession', $model->profession) == "Internal Audit" ? 'selected' : ''}} value="Internal Audit">Internal Audit</option>
<option {{ old('profession', $model->profession) == "Loan/Mortgage" ? 'selected' : ''}} value="Loan/Mortgage">Loan/Mortgage</option>
<option {{ old('profession', $model->profession) == "Management" ? 'selected' : ''}} value="Management">Management</option>
<option {{ old('profession', $model->profession) == "Regulatory Compliance" ? 'selected' : ''}} value="Regulatory Compliance">Regulatory Compliance</option>
<option {{ old('profession', $model->profession) == "Retail Banking/Branch Operation" ? 'selected' : ''}} value="Retail Banking/Branch Operation">Retail Banking/Branch Operation</option>
<option {{ old('profession', $model->profession) == "Risk Management" ? 'selected' : ''}} value="Risk Management">Risk Management</option>
<option {{ old('profession', $model->profession) == "Treasury Management" ? 'selected' : ''}} value="Treasury Management">Treasury Management</option>
<option {{ old('profession', $model->profession) == "Underwriter (Insurance)" ? 'selected' : ''}} value="Underwriter (Insurance)">Underwriter (Insurance)</option>
<option {{ old('profession', $model->profession) == "Company Secretary" ? 'selected' : ''}} value="Company Secretary">Company Secretary</option>
<option {{ old('profession', $model->profession) == "Investor Relations" ? 'selected' : ''}} value="Investor Relations">Investor Relations</option>
<option {{ old('profession', $model->profession) == "Clerical/Administrative" ? 'selected' : ''}} value="Clerical/Administrative">Clerical/Administrative</option>
<option {{ old('profession', $model->profession) == "Human Resources" ? 'selected' : ''}} value="Human Resources">Human Resources</option>
<option {{ old('profession', $model->profession) == "Secretarial" ? 'selected' : ''}} value="Secretarial">Secretarial</option>
<option {{ old('profession', $model->profession) == "Top Management" ? 'selected' : ''}} value="Top Management">Top Management</option>
<option {{ old('profession', $model->profession) == "Management" ? 'selected' : ''}} value="Management">Management</option>
<option {{ old('profession', $model->profession) == "Advertising Executive/Account Manager" ? 'selected' : ''}} value="Advertising Executive/Account Manager">Advertising Executive/Account Manager</option>
<option {{ old('profession', $model->profession) == "Consultant" ? 'selected' : ''}} value="Consultant">Consultant</option>
<option {{ old('profession', $model->profession) == "Copywriter" ? 'selected' : ''}} value="Copywriter">Copywriter</option>
<option {{ old('profession', $model->profession) == "Creative" ? 'selected' : ''}} value="Creative">Creative</option>
<option {{ old('profession', $model->profession) == "Media Planning" ? 'selected' : ''}} value="Media Planning">Media Planning</option>
<option {{ old('profession', $model->profession) == "Animator" ? 'selected' : ''}} value="Animator">Animator</option>
<option {{ old('profession', $model->profession) == "Design Drafter" ? 'selected' : ''}} value="Design Drafter">Design Drafter</option>
<option {{ old('profession', $model->profession) == "Fashion Designer" ? 'selected' : ''}} value="Fashion Designer">Fashion Designer</option>
<option {{ old('profession', $model->profession) == "Graphics Designer" ? 'selected' : ''}} value="Graphics Designer">Graphics Designer</option>
<option {{ old('profession', $model->profession) == "Industrial/Product Designer" ? 'selected' : ''}} value="Industrial/Product Designer">Industrial/Product Designer</option>
<option {{ old('profession', $model->profession) == "Photographer" ? 'selected' : ''}} value="Photographer">Photographer</option>
<option {{ old('profession', $model->profession) == "Web Designer" ? 'selected' : ''}} value="Web Designer">Web Designer</option>
<option {{ old('profession', $model->profession) == "Architect/Interior Design" ? 'selected' : ''}} value="Architect/Interior Design">Architect/Interior Design</option>
<option {{ old('profession', $model->profession) == "Civil Engineer" ? 'selected' : ''}} value="Civil Engineer">Civil Engineer</option>
<option {{ old('profession', $model->profession) == "Foreman/Technician" ? 'selected' : ''}} value="Foreman/Technician">Foreman/Technician</option>
<option {{ old('profession', $model->profession) == "Quality Control/Assurance" ? 'selected' : ''}} value="Quality Control/Assurance">Quality Control/Assurance</option>
<option {{ old('profession', $model->profession) == "Site Engineer" ? 'selected' : ''}} value="Site Engineer">Site Engineer</option>
<option {{ old('profession', $model->profession) == "Structural Engineer" ? 'selected' : ''}} value="Structural Engineer">Structural Engineer</option>
<option {{ old('profession', $model->profession) == "Supervisor/Team Lead" ? 'selected' : ''}} value="Supervisor/Team Lead">Supervisor/Team Lead</option>
<option {{ old('profession', $model->profession) == "Clerk of Works/Site Supervisor" ? 'selected' : ''}} value="Clerk of Works/Site Supervisor">Clerk of Works/Site Supervisor</option>
<option {{ old('profession', $model->profession) == "IT - Hardware" ? 'selected' : ''}} value="IT - Hardware">IT - Hardware</option>
<option {{ old('profession', $model->profession) == "Database Administrator" ? 'selected' : ''}} value="Database Administrator">Database Administrator</option>
<option {{ old('profession', $model->profession) == "Infrastructure Security" ? 'selected' : ''}} value="Infrastructure Security">Infrastructure Security</option>
<option {{ old('profession', $model->profession) == "IT Trainer" ? 'selected' : ''}} value="IT Trainer">IT Trainer</option>
<option {{ old('profession', $model->profession) == "Network/System Engineer" ? 'selected' : ''}} value="Network/System Engineer">Network/System Engineer</option>
<option {{ old('profession', $model->profession) == "Project Management" ? 'selected' : ''}} value="Project Management">Project Management</option>
<option {{ old('profession', $model->profession) == "Quality Control/Assurance" ? 'selected' : ''}} value="Quality Control/Assurance">Quality Control/Assurance</option>
<option {{ old('profession', $model->profession) == "System Administrator" ? 'selected' : ''}} value="System Administrator">System Administrator</option>
<option {{ old('profession', $model->profession) == "Functional Consultant/Business Analyst" ? 'selected' : ''}} value="Functional Consultant/Business Analyst">Functional Consultant/Business Analyst</option>
<option {{ old('profession', $model->profession) == "Software Architect" ? 'selected' : ''}} value="Software Architect">Software Architect</option>
<option {{ old('profession', $model->profession) == "Software Engineer/Programmer" ? 'selected' : ''}} value="Software Engineer/Programmer">Software Engineer/Programmer</option>
<option {{ old('profession', $model->profession) == "Software Security" ? 'selected' : ''}} value="Software Security">Software Security</option>
<option {{ old('profession', $model->profession) == "System Analyst" ? 'selected' : ''}} value="System Analyst">System Analyst</option>
<option {{ old('profession', $model->profession) == "Kindergarten Teacher" ? 'selected' : ''}} value="Kindergarten Teacher">Kindergarten Teacher</option>
<option {{ old('profession', $model->profession) == "Lecturer" ? 'selected' : ''}} value="Lecturer">Lecturer</option>
<option {{ old('profession', $model->profession) == "Librarian" ? 'selected' : ''}} value="Librarian">Librarian</option>
<option {{ old('profession', $model->profession) == "Primary/Secondary Teacher" ? 'selected' : ''}} value="Primary/Secondary Teacher">Primary/Secondary Teacher</option>
<option {{ old('profession', $model->profession) == "Professor" ? 'selected' : ''}} value="Professor">Professor</option>
<option {{ old('profession', $model->profession) == "Researcher" ? 'selected' : ''}} value="Researcher">Researcher</option>
<option {{ old('profession', $model->profession) == "School Principal" ? 'selected' : ''}} value="School Principal">School Principal</option>
<option {{ old('profession', $model->profession) == "Chemical Engineering" ? 'selected' : ''}} value="Chemical Engineering">Chemical Engineering</option>
<option {{ old('profession', $model->profession) == "Electrical Engineer" ? 'selected' : ''}} value="Electrical Engineer">Electrical Engineer</option>
<option {{ old('profession', $model->profession) == "Process Engineer" ? 'selected' : ''}} value="Process Engineer">Process Engineer</option>
<option {{ old('profession', $model->profession) == "Project Management" ? 'selected' : ''}} value="Project Management">Project Management</option>
<option {{ old('profession', $model->profession) == "R&D Engineer" ? 'selected' : ''}} value="R&D Engineer">R&D Engineer</option>
<option {{ old('profession', $model->profession) == "Test Engineer" ? 'selected' : ''}} value="Test Engineer">Test Engineer</option>
<option {{ old('profession', $model->profession) == "Electronics Engineer" ? 'selected' : ''}} value="Electronics Engineer">Electronics Engineer</option>
<option {{ old('profession', $model->profession) == "Telecommunication Engineer" ? 'selected' : ''}} value="Telecommunication Engineer">Telecommunication Engineer</option>
<option {{ old('profession', $model->profession) == "Environmental Engineering" ? 'selected' : ''}} value="Environmental Engineering">Environmental Engineering</option>
<option {{ old('profession', $model->profession) == "Industrial Engineering" ? 'selected' : ''}} value="Industrial Engineering">Industrial Engineering</option>
<option {{ old('profession', $model->profession) == "Automotive Engineer" ? 'selected' : ''}} value="Automotive Engineer">Automotive Engineer</option>
<option {{ old('profession', $model->profession) == "CAD-CAM" ? 'selected' : ''}} value="CAD-CAM">CAD-CAM</option>
<option {{ old('profession', $model->profession) == "Drilling/Well Engineer" ? 'selected' : ''}} value="Drilling/Well Engineer">Drilling/Well Engineer</option>
<option {{ old('profession', $model->profession) == "Facilities Engineer" ? 'selected' : ''}} value="Facilities Engineer">Facilities Engineer</option>
<option {{ old('profession', $model->profession) == "Doctor/Diagnosis" ? 'selected' : ''}} value="Doctor/Diagnosis">Doctor/Diagnosis</option>
<option {{ old('profession', $model->profession) == "Pharmacy" ? 'selected' : ''}} value="Pharmacy">Pharmacy</option>
<option {{ old('profession', $model->profession) == "Chiropractor" ? 'selected' : ''}} value="Chiropractor">Chiropractor</option>
<option {{ old('profession', $model->profession) == "Clinical Laboratory Assistant/Technician" ? 'selected' : ''}} value="Clinical Laboratory Assistant/Technician">Clinical Laboratory Assistant/Technician</option>
<option {{ old('profession', $model->profession) == "Medical Examiner" ? 'selected' : ''}} value="Medical Examiner">Medical Examiner</option>
<option {{ old('profession', $model->profession) == "Medical Officer/Paramedic" ? 'selected' : ''}} value="Medical Officer/Paramedic">Medical Officer/Paramedic</option>
<option {{ old('profession', $model->profession) == "Medical Transcriptionist" ? 'selected' : ''}} value="Medical Transcriptionist">Medical Transcriptionist</option>
<option {{ old('profession', $model->profession) == "Medical/Clinical Research" ? 'selected' : ''}} value="Medical/Clinical Research">Medical/Clinical Research</option>
<option {{ old('profession', $model->profession) == "Midwife" ? 'selected' : ''}} value="Midwife">Midwife</option>
<option {{ old('profession', $model->profession) == "Nurse" ? 'selected' : ''}} value="Nurse">Nurse</option>
<option {{ old('profession', $model->profession) == "Optometrist" ? 'selected' : ''}} value="Optometrist">Optometrist</option>
<option {{ old('profession', $model->profession) == "Pathologist" ? 'selected' : ''}} value="Pathologist">Pathologist</option>
<option {{ old('profession', $model->profession) == "Physiotherapist" ? 'selected' : ''}} value="Physiotherapist">Physiotherapist</option>
<option {{ old('profession', $model->profession) == "Radiographer" ? 'selected' : ''}} value="Radiographer">Radiographer</option>
<option {{ old('profession', $model->profession) == "Sonographer" ? 'selected' : ''}} value="Sonographer">Sonographer</option>
<option {{ old('profession', $model->profession) == "Bartender" ? 'selected' : ''}} value="Bartender">Bartender</option>
<option {{ old('profession', $model->profession) == "Chef" ? 'selected' : ''}} value="Chef">Chef</option>
<option {{ old('profession', $model->profession) == "Food Preparation/Kitchen Hand" ? 'selected' : ''}} value="Food Preparation/Kitchen Hand">Food Preparation/Kitchen Hand</option>
<option {{ old('profession', $model->profession) == "Management" ? 'selected' : ''}} value="Management">Management</option>
<option {{ old('profession', $model->profession) == "Restaurant Waiter" ? 'selected' : ''}} value="Restaurant Waiter">Restaurant Waiter</option>
<option {{ old('profession', $model->profession) == "Casino Dealer" ? 'selected' : ''}} value="Casino Dealer">Casino Dealer</option>
<option {{ old('profession', $model->profession) == "Hotel Concierge" ? 'selected' : ''}} value="Hotel Concierge">Hotel Concierge</option>
<option {{ old('profession', $model->profession) == "Hotel Housekeeping" ? 'selected' : ''}} value="Hotel Housekeeping">Hotel Housekeeping</option>
<option {{ old('profession', $model->profession) == "Hotel Manager" ? 'selected' : ''}} value="Hotel Manager">Hotel Manager</option>
<option {{ old('profession', $model->profession) == "Ticketing" ? 'selected' : ''}} value="Ticketing">Ticketing</option>
<option {{ old('profession', $model->profession) == "Tour Guide" ? 'selected' : ''}} value="Tour Guide">Tour Guide</option>
<option {{ old('profession', $model->profession) == "Travel Coordinator/Agent" ? 'selected' : ''}} value="Travel Coordinator/Agent">Travel Coordinator/Agent</option>
<option {{ old('profession', $model->profession) == "Building/Facilities Maintenance" ? 'selected' : ''}} value="Building/Facilities Maintenance">Building/Facilities Maintenance</option>
<option {{ old('profession', $model->profession) == "Machinery Maintenance" ? 'selected' : ''}} value="Machinery Maintenance">Machinery Maintenance</option>
<option {{ old('profession', $model->profession) == "Mold Designer" ? 'selected' : ''}} value="Mold Designer">Mold Designer</option>
<option {{ old('profession', $model->profession) == "Precision Machinist" ? 'selected' : ''}} value="Precision Machinist">Precision Machinist</option>
<option {{ old('profession', $model->profession) == "Inventory Control" ? 'selected' : ''}} value="Inventory Control">Inventory Control</option>
<option {{ old('profession', $model->profession) == "Warehouse" ? 'selected' : ''}} value="Warehouse">Warehouse</option>
<option {{ old('profession', $model->profession) == "Purchasing" ? 'selected' : ''}} value="Purchasing">Purchasing</option>
<option {{ old('profession', $model->profession) == "Sales - Corporate" ? 'selected' : ''}} value="Sales - Corporate">Sales - Corporate</option>
<option {{ old('profession', $model->profession) == "Marketing/Business Dev" ? 'selected' : ''}} value="Marketing/Business Dev">Marketing/Business Dev</option>
<option {{ old('profession', $model->profession) == "Merchandising" ? 'selected' : ''}} value="Merchandising">Merchandising</option>
<option {{ old('profession', $model->profession) == "Retail Sales" ? 'selected' : ''}} value="Retail Sales">Retail Sales</option>
<option {{ old('profession', $model->profession) == "Sales - Eng/Tech/IT" ? 'selected' : ''}} value="Sales - Eng/Tech/IT">Sales - Eng/Tech/IT</option>
<option {{ old('profession', $model->profession) == "Sales - Financial Services" ? 'selected' : ''}} value="Sales - Financial Services">Sales - Financial Services</option>
<option {{ old('profession', $model->profession) == "Telesales/Telemarketing" ? 'selected' : ''}} value="Telesales/Telemarketing">Telesales/Telemarketing</option>
<option {{ old('profession', $model->profession) == "Actuarial/Statistics" ? 'selected' : ''}} value="Actuarial/Statistics">Actuarial/Statistics</option>
<option {{ old('profession', $model->profession) == "Agriculture" ? 'selected' : ''}} value="Agriculture">Agriculture</option>
<option {{ old('profession', $model->profession) == "Aviation" ? 'selected' : ''}} value="Aviation">Aviation</option>
<option {{ old('profession', $model->profession) == "Biotechnology" ? 'selected' : ''}} value="Biotechnology">Biotechnology</option>
<option {{ old('profession', $model->profession) == "Chemistry" ? 'selected' : ''}} value="Chemistry">Chemistry</option>
<option {{ old('profession', $model->profession) == "Food Tech/Nutritionist" ? 'selected' : ''}} value="Food Tech/Nutritionist">Food Tech/Nutritionist</option>
<option {{ old('profession', $model->profession) == "Geology/Geophysics" ? 'selected' : ''}} value="Geology/Geophysics">Geology/Geophysics</option>
<option {{ old('profession', $model->profession) == "Science & Technology" ? 'selected' : ''}} value="Science & Technology">Science & Technology</option>
<option {{ old('profession', $model->profession) == "Physicist" ? 'selected' : ''}} value="Physicist">Physicist</option>
<option {{ old('profession', $model->profession) == "Others" ? 'selected' : ''}} value="Others">Others</option>
<option {{ old('profession', $model->profession) == "Student" ? 'selected' : ''}} value="Student">Student</option>