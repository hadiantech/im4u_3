@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/happening/happenings_header.jpg)">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">WHAT'S HAPPENING!</h1>
            <h5 class="white-text text-shadow">Check out what's happening throughout the years</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  latest articles  -->
<div id="articles" class="container-fluid gray-bg p-5">
    <div class="container">
        <div class="row">
            <div class="row slick-navi align-items-center pt-4 pb-4">
                <div class="col-12 col-md-6 item pr-0">
                    <h1 class="blue-text">LATEST ARTICLES</h1>
                </div>
                <div class="col-12 col-md-4 d-none d-md-block p-0">
                    <a href="javascript:;" class="link blue-text">/ VIEW ALL PROJECTS <i class="fas fa-arrow-alt-circle-right"></i></a>
                </div>
                <div class="col-12 col-md-2 ml-auto item d-none d-md-block">
                    <div class="nav d-none">
                        <span class="blue-bg rounded ml-auto">
                            <a href="" role="button" data-slide="prev"><i class="fas fa-chevron-left white-text " style="" ></i></a>
                        </span>
                        <span class="blue-bg rounded">
                            <a href="" role="button" data-slide="next"><i class="fas fa-chevron-right white-text " style=""></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="slick-up-project pb-4">
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>

            </div>
        </div>
    </div>
</div>
<!--  end  -->


<!--  success stories  -->
<div id="stories" class="container-fluid p-0 pt-5 pb-5" style="background:#1488c8;">
    <div class="container pt-5 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-white">OUR SUCCESS STORIES</h1>
                <p class="text-white">We are proud to share some of the success stories that we’ve had throughout the years. Since it was first launch in 2012, Volunteer Malaysia has continued to inspire new generation of youths to be actively involved in volunteer activities through our
                    successful and meaningful programs.</p>
            </div>
        </div>

        <div class="row">
            <div class="slick-item-success">
                <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                    <div class="success-box" style="background-image: url(img/happening/ambassador.jpg); background-size: cover;"></div>
                    <div class="success-text p-4" style="background-color: #2A2A2A;">
                        <a href="" data-toggle="modal" data-target="#successmodal">
                            <h3 class="white-text">ReachOut Run</h3>
                        </a>
                        <p class="dotdotdot-text-s white-text">Ahh. Children. (they high tale it out of the barn.) Listen, woah. (he trips but gets back up.) Hello, uh excuse me. Sorry about your barn. (Marty opens the barn door.) It's already mutated intro human form, shoot it.Uh, no, no,
                            no, no. (Notices that Marty is watching them.) What are you looking at, butt-head? Hey Biff, check out this guy's life preserver, dork thinks he's gonna drown. Yeah, well, how about my homework, McFly? Uh, well, okay Biff,
                            uh, I'll finish that on up tonight and I'll bring it over first thing tomorrow morning. Hey not too early I sleep in </p>

                        <div class="row mb-2">
                            <a href="" data-toggle="modal" data-target="#successmodal" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                    <div class="success-box border border-secondary" style="background-image: url(img/happening/ambassador.jpg); background-size: cover;"></div>
                    <div class="success-text p-3" style="background-color: #2A2A2A;">
                        <h3 class="white-text">Negaraku PRIHATIN</h3>
                        <p class="dotdotdot-text-m white-text">Yeah, that's right. That's right, I am a slacker. Don't you remember? You gave me detention last week. Last week? The school burnt down six years ago. Now, you've got exactly three seconds to get off my porch with your nuts in
                            tact. One - I just wanna know what the hell is going on here. Two... Argh!!!</p>
                        <div class="row">
                            <a href="" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                    <div class="success-box border border-secondary" style="background-image: url(img/happening/ambassador.jpg); background-size: cover;"></div>
                    <div class="success-text p-3" style="background-color: #2A2A2A;">
                        <h3 class="white-text">iM4U Fabrik Kasihs</h3>
                        <p class="dotdotdot-text-m white-text">Lights on? (The lights come on.) Yes, now look. Just take it easy and you'll be fine. And be careful in the future. Future? Have a nice day Mrs McFly.
                        </p>
                        <div class="row">
                            <a href="" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!--  collaborate  -->
<div id="collaborate" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="red-text">COLLABORATE WITH US</h1>
                <h3 class="blue-text">Corporate Clients & NGOs</h3>
            </div>
        </div>
        <div class="row align-items-center mb-5">
            <div class="col-sm-12 col-md-8">
                <p>If your organisation is passionate about programs related to the environment, community well being, sports, arts & culture and even skills development, why not collaborate with us? Let’s work together to create more awareness and achieve
                    maximum impact!</p>
                <p>Let’s co-create unique, mutually beneficial partnerships. We can showcase your company as a leading supporter of volunteerism, engage your staff and clients and increase the opportunities which we can offer to the Malaysian youths.</p>
            </div>
            <div class="col-sm-4 col-md-3 d-sm-none d-md-block">
                <img src="img/happening/collaborate_icon.png" class="img-fluid" />
            </div>
        </div>

        <div class="row pt-5 m-3 no-gutters" style="border-top:2px solid #1f518c;">
            <div class="col-sm-12">
                <h3 class="blue-text">Collaboration Stories Highlights</h3>
            </div>
        </div>
        <div class="row">
            <div class="slick-collab">
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
            </div>
        </div>
        <div class="row pt-5 no-gutters" style="border-bottom:2px solid #1f518c;"></div>

        <div class="row pt-5 pb-3 pl-2">
            <h3 class="blue-text">Our Partners</h3>
        </div>
        <div class="row p-0 mb-4 no-gutters">
            <div class="col-sm-2"><img src="img/happening/logos/1.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/2.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/3.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/4.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/5.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/6.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/7.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/8.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/9.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/10.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/11.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/12.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/13.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/14.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/15.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/16.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/17.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/18.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/19.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/20.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/21.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/22.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/23.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/24.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/25.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/26.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/27.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/28.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/29.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/30.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/31.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/32.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/33.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/34.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/35.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/36.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/37.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/38.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/39.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/40.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/41.png" class="img-fluid hover-zoom" /></div>
            <div class="col-sm-2"><img src="img/happening/logos/42.png" class="img-fluid hover-zoom" /></div>
        </div>
    </div>
</div>
<!--  end  -->

<!--  be our ambassador  -->
<div id="ambassador" class="container-fluid red-bg">
    <div class="container">
        <div class="row pt-5">
            <h1 class="white-text pb-3">BE OUR AMBASSADOR</h1>
            <p class="white-text">Do you have a personal cause that you stand for? Want to make a positive impact towards your community? Become our ambassador and you’ll get the opportunity to create your own volunteer programs and unique social action campaigns that address
                current issues which will benefit the communities.</p>
        </div>
        <div class="row pb-5">
            <a href="mailto:pr@vm.my?subject=Be Volunteer Malaysia Partner" class="btn btn-white">Contact Us Now</a>
        </div>
    </div>
</div>
<div class="container-fluid" style="background-image: url(img/happening/ambassador.jpg); background-size: cover; background-position: center; min-height: 430px;"></div>

<div class="container-fluid red-bg">
    <div class="container pt-5 pb-5">
        <div class="row pb-4">
            <h3 class="white-text">Ambassadors Stories Highlights</h3>
        </div>

        <div class="row pb-4">
            <div class="slick-amba">
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <div class="col-sm-12 white-bg text-center">
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                        <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <div class="col-sm-12 white-bg text-center">
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                        <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <div class="col-sm-12 white-bg text-center">
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                        <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <div class="col-sm-12 white-bg text-center">
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                        <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <div class="col-sm-12 white-bg text-center">
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Yatim</h5>
                        <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<!--  end  -->

<!--  people journal  -->
<div id="tpj" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container">
        <div class="row pt-4 pb-4">
            <h1 class="blue-text">THE PEOPLE'S JOURNAL</h1>
        </div>
        <div class="row p-3">
            <div class="slick-people-journal">
                <div class="pj-box col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title-md p-3 text-center" style="height: 140px">CSR Rumah Anak Yatim</h5>
                    <div class="row"><a href="javascript:;" class="btn btn-red mb-3">READ STORY</a></div>
                </div>
                <div class="pj-box col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title-md p-3 text-center" style="height: 140px">CSR Rumah Anak Yatim</h5>
                    <div class="row"><a href="javascript:;" class="btn btn-red mb-3">READ STORY</a></div>
                </div>
                <div class="pj-box col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title-md p-3 text-center" style="height: 140px">CSR Rumah Anak Yatim</h5>
                    <div class="row"><a href="javascript:;" class="btn btn-red mb-3">READ STORY</a></div>
                </div>
                <div class="pj-box col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title-md p-3 text-center" style="height: 140px">CSR Rumah Anak Yatim</h5>
                    <div class="row"><a href="javascript:;" class="btn btn-red mb-3">READ STORY</a></div>
                </div>
                <div class="pj-box col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title-md p-3 text-center" style="height: 140px">CSR Rumah Anak Yatim</h5>
                    <div class="row"><a href="javascript:;" class="btn btn-red mb-3">READ STORY</a></div>
                </div>

            </div>
        </div>
        <div class="row rounded p-4 mx-2 mt-4 white-bg" style="border:1px solid #1f518c">
            <div class="col-sm-12 ">
                <h4 class="blue-text">HAVE GOOD STORIES TO SHARE? SUBMIT YOUR ARTICLES</h4>
            </div>
            <div class="col-sm-12">
                <hr style="border: 1px solid #1f518c" />
            </div>
            <div class="col-sm-12">
                <p>We are looking for unique, well-told stories/essays/write-ups that you would like to share! The theme for this month is “Red” and we are open to receiving stories in various formats from graphic essays, photo essays, personal opinion pieces
                    to even comedic articles. There is no strict word count, but the 800 to 1200 word range would be preferable. Both Malay & English articles are accepted so submit your stories today!</p>
                <br/>
                <h5>Follow This Steps:</h5>
                <p class="mb-1">1. Download and fill the article form here: <a href="" class="white-text bold badge badge-primary">DOWNLOAD</a></p>
                <p>2. Submit the completed article form here: <a href="" class="white-text bold badge badge-primary">SUBMIT</a></p>
            </div>
        </div>
    </div>
</div>
<!--  end  -->



<!-- modal  -->
<div class="modal fade" id="successmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="position: relative;">
            <div class="modal-header leader-modal-header blue-bg p-0" style="background-image: url(img/happening/ambassador.jpg); background-size: cover; background-repeat: no-repeat; background-position: center; min-height: 350px;">
                <div class="row" style="position: absolute; right: 20px">
                    <button type="button" class="close white-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="row no-gutters justify-content-center align-items-center">


                </div>
            </div>
            <div class="modal-body m-0 p-0">
                <div class="row p-4">
                    <h3 class="blue-text">REACHING OUT TO NEW HEIGHTS</h3>

                    <p>iM4U Reach Out Convention & Celebration celebrated its’ 5th year milestone back in February when they were in Batu Pahat, Johorfor the first round of Reach Out 2017. The programme made its way through several other major cities around
                        Malaysia until the 18th of March including Pahang, Kedah, Negeri Sembilan & Melaka.</p>

                    <p>The convention, which aims to promote volunteerism to Malaysian youth through the sharing of experiences by a selected group of successful individuals featured big and inspiring names including Datuk Jake Abdullah, Tengku Dato' Sri
                        Zafrul, Azran Osman – Rani, Joe Flizzow, Raviraj & Aishah from Project TRY, Kavin Jay, Anita Yusof and AJ ‘Pyro’ Lias Mansor.</p>

                    <p>The programme, that went to 5 different institution - Universiti Tun Hussein Onn, Universiti Malaysia Pahang, Universiti Utara Malaysia, Politeknik Nilai, Politeknik Merlimau was hosted by iM4U fm’s very own radio announcers; Jiggy,
                        TV, Amrita, Fiza, Tyler and Ira.</p>
                </div>
                <div class="row px-4">
                    <h4 class="blue-text">LINKS</h4>
                </div>
                <div class="row px-4 mb-3">
                    <a href="" target="_blank" class="p-2 m-1 italic badge badge-pill badge-warning">Video Highlights 1</a>
                    <a href="" target="_blank" class="p-2 m-1 italic badge badge-pill badge-warning">Video Highlights 2</a>
                    <a href="" target="_blank" class="p-2 m-1 italic badge badge-pill badge-warning">Video Highlights 3</a>
                </div>

                <div class="row gray-bg p-4">
                    <div class="container">
                        <h4 class="blue-text">PHOTO GALLERY</h4>
                        <div class="row slick-photo-gallery">
                            <div class="col-sm-6 border p-0 m-2 rounded box-shadow">
                                <img src="img/happening/reachoutconvention.jpg" class="img-fluid" />
                            </div>
                            <div class="col-sm-6 border p-0 m-2 rounded box-shadow">
                                <img src="img/happening/reachoutconvention.jpg" class="img-fluid" />
                            </div>
                            <div class="col-sm-6 border p-0 m-2 rounded box-shadow">
                                <img src="img/happening/reachoutconvention.jpg" class="img-fluid" />
                            </div>
                            <div class="col-sm-6 border p-0 m-2 rounded box-shadow">
                                <img src="img/happening/reachoutconvention.jpg" class="img-fluid" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end   -->

@endsection @section('js') @parent @endsection