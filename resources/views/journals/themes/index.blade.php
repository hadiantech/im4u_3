
@extends('layouts.admin.master')

@section('title', 'Journals')
@section('subtitle', '| Edit Journal Theme')

@section('content')


<div class="col-sm-12 border bg-white p-4">
    <div class='col-lg-12'>

        <h1><i class='fa fa-key'></i> Update Theme : {{$theme->title or 'null'}}</h1>
        <br>

        @if($theme)
        {{ Form::model($theme, array('route' => array('themes.update', $theme->id), 'method' => 'PUT')) }}

        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
        </div>
        <br>
        <a class="btn btn-link" href="{{ route('journals.index') }}">Back</a> 
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
        @else
        {{ Form::open(array('url' => 'themes')) }}

        <div class="form-group">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
        </div>
        <br>

        <a class="btn btn-link" href="{{ route('journals.index') }}">Back</a> 
        {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
        @endif
    </div>
</div>

@endsection
