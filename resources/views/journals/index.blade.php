@extends('layouts.admin.master')

@section('title', 'Journals')

@section('content')


<div class="card">
    <div class="card-body">
        @can('add_journals')
            <a href="{{ route('journals.create') }}" class="btn btn-success">
                <i class="fas fa-plus"></i> New
            </a>
        @endcan

        @can('edit_journal_theme')
            <a href="{{ route('themes.index') }}" class="btn btn-success">
                    <i class="fas fa-edit"></i> Update Theme</a>
        @endcan
    </div>
</div>

<br />
<div>
    <a href="{{ URL::to('journals') }}">All</a> | <a href="{{ URL::to('journals?view=trash') }}">Trash</a>
</div>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Name (unique)</th>
            <th>Title</th>
            <th>Author</th>
            <th>Feature</th>
            <th>Hidden</th>
            <th>Operation</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($journals as $journal)
        <tr>

            <td>{{ $journal->name }}</td>
            <td>{{ $journal->title }}</td>
            <td>{{ $journal->author_name }}</td>
            <td>@include('shared._check', ['check' => $journal->featured])</td>
            <td>@include('shared._check', ['check' => $journal->hidden])</td>

            <td>
                @include('shared._table_action_2', ['model' => $journal, 'url' => 'journals'])
            </td>
        </tr>
        @endforeach
    </tbody>

</table>
        




@endsection
