@extends('layouts.front.master') @section('content')
<!--  header -->
<div class="container-fluid gray-bg p-0 ">
    <div class="container white-bg py-5">
        <div class="row">
            <div class="col-md-12 d-flex align-items-center">
                <p class="mb-0 mr-3 futura-book" style="color:#767676; font-size:1.5em">Share</p>
                <a href="http://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" target="_blank">
                    <i class="fab fa-facebook-square mr-1" style="color:#3b5998; font-size:2em"></i>
                </a>
                <a href="http://twitter.com/share?url={{ url()->current() }}" target="_blank">
                    <i class="fab fa-twitter-square mr-1" style="color:#00aced; font-size:2em"></i>
                </a>
                <a href="https://plus.google.com/share?url={{ url()->current() }}" target="_blank">
                    <i class="fab fa-google-plus-square mr-1" style="color:#d34836; font-size:2em"></i>
                </a>
            </div>
        </div>

        {{-- title --}}
        <div class="row">
            <div class="col-md-12 p-3 mt-1 mb-3">
                <h1 class="blue-text">{{ $journal->title }}</h1>
            </div>
        </div>

        {{-- header image --}}
        <div class="row">
            <div class="col-md-12">
                <div style="background-image:url('{{ $journal->getMedia('journals')->first()->getUrl() }}'); background-repeat:no-repeat; background-size:cover; background-position:center; min-height:500px; max-height:700px"></div>
            </div>
        </div>

        {{-- content --}}
        <div class="row mt-5 mb-5">
            <div class="col-md-12">
                {!! $journal->content !!}    
            </div>
        </div>

        {{-- author --}}
        <div class="row m-2 rounded border p-4">
            
            <div class="col-md-2 col-sm-12 col-xs-12">
                <img src="{{ $journal->getMedia('authors')->first()->getUrl() }}" class="img-fluid rounded-circle" style="width:130px; height:130px" />
            </div>
            <div class="col-md-10 align-items-center ">
                <div class="row">
                    <div class="col-md-12">
                        <span class="blue-text font-weight-bold mb-0">AUTHOR</span>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12">
                        <p class="text-weight-bold" style="font-weight:700 !important">{{ $journal->author_name }}</p>                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <p>{!! $journal->author_detail !!}</p>
                    </div>
                </div>
            </div>
           
        </div>
    </div>
</div>
<!--  end  -->
@endsection 
@section('js') 
@parent
@endsection