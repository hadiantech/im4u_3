@extends('layouts.admin.master')

@section('title', 'Journals')
@section('subtitle', 'Edit')

@section('content')

<div class="row">

    <div class="col-sm-2 border bg-white p-1">
        <section class="dashboard-counts no-padding-bottom no-padding-top">
        <div class="row bg-white has-shadow">
            <div class="col-lg-12">
                <div class="thumbnail">
                    <h3>Main Image</h3>
                    <hr/>

                    @if(empty($journal->getMedia('journals')->first()))
                    <img width="100%" src="{{url('img/no_image.png')}}" />
                    @else
                    <img width="100%" src="{{ $journal->getMedia('journals')->first()->getUrl() }}" />
                    @endif

                    <div class="caption">
                        <br />
                        <p>
                            @if(!empty($journal->getMedia('journals')->first()))
                            <a href="{{ $journal->getMedia('journals')->first()->getUrl() }}" target="_blank" class="btn btn-primary btn-block" role="button">View Image</a>
                            @else
                            <a href="{{url('img/no_image.png')}}" target="_blank" class="btn btn-primary btn-block" role="button">View Image</a>
                            @endif

                            <a data-toggle="modal" data-target="#mainimage" class="btn btn-primary btn-block" role="button">Change Image</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        </section>

        <section class="dashboard-counts no-padding-bottom">
            <div class="row bg-white has-shadow">
                <div class="col-lg-12">
                    <div class="thumbnail">
                        <h3>Author Image</h3>
                        <hr/>

                        @if(empty($journal->getMedia('authors')->first()))
                        <img width="100%" src="{{url('img/no_image.png')}}" />
                        @else
                        <img width="100%" src="{{ $journal->getMedia('authors')->first()->getUrl() }}" />
                        @endif

                        <div class="caption">
                            <br />
                            <p>
                                @if(!empty($journal->getMedia('authors')->first()))
                                <a href="{{ $journal->getMedia('authors')->first()->getUrl() }}" target="_blank" class="btn btn-primary btn-block" role="button">View Image</a>
                                @else
                                <a href="{{url('img/no_image.png')}}" target="_blank" class="btn btn-primary btn-block" role="button">View Image</a>
                                @endif

                                <a data-toggle="modal" data-target="#authorimage" class="btn btn-primary btn-block" role="button">Change Image</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
    <div class="col-sm-10 border bg-white p-4">
        <div class="row bg-white has-shadow">
            <div class='col-lg-12'>

                {{ Form::model($journal, array('route' => array('journals.update', $journal->id), 'method' => 'PUT', 'data-parsley-validate')) }}

                <div class="form-group">
                    {{ Form::label('name', 'Name (unique)') }}
                    {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('title', 'Title') }}
                    {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('author_name', 'Author Name') }}
                    {{ Form::text('author_name', null, array('class' => 'form-control', 'required')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('author_detail', 'Author Detail') }}
                    {{ Form::textarea('author_detail', null, array('class' => 'form-control my-editor', 'required', 'rows' => 3)) }}
                </div>

                <div class="form-group">
                    {{ Form::label('content', 'Content') }}
                    {{ Form::textarea('content', null, array('class' => 'form-control my-editor', 'required', 'rows' => 20)) }}
                </div>

                <div class="form-group">
                    {{ Form::label('featured', 'Featured ') }}

                    {{ Form::hidden('featured', 0) }}
                    {{ Form::checkbox('featured', 1, null) }}
                </div>

                <div class="form-group">
                    {{ Form::label('hidden', 'Hidden ') }}

                    {{ Form::hidden('hidden', 0) }}
                    {{ Form::checkbox('hidden', 1, null) }}
                </div>

                <div class="form-group">
                    {{ Form::label('released_at', 'Released At ') }}
                    {{ Form::date('released_at', null, array('class' => 'form-control my-editor', 'required')) }}
                </div>

                <a class="btn btn-link" href="{{ route('journals.index') }}">Back</a> 
                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                {{ Form::close() }}
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="authorimage" tabindex="-1" role="dialog" aria-labelledby="authorimage" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{ Form::model($journal, array('route' => array('journals.author.image.update', $journal->id),'files' => true, 'method' => 'PUT', 'data-parsley-validate')) }}
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload New Picture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('author_image', 'Author Image') }}<br />
                    {{ Form::file('author_image', null, array('class' => 'form-control', 'required')) }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

<div class="modal fade" id="mainimage" tabindex="-1" role="dialog" aria-labelledby="mainimage" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{ Form::model($journal, array('route' => array('journals.main.image.update', $journal->id),'files' => true, 'method' => 'PUT', 'data-parsley-validate')) }}
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Upload New Picture</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('image', 'Main Image') }}<br />
                    {{ Form::file('image', null, array('class' => 'form-control', 'required')) }}
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
</div>

@endsection


@section('js')
@parent
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection
