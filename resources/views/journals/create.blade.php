@extends('layouts.admin.master')

@section('title', 'Journals')

@section('subtitle', 'Create')

@section('content')

<div class="col-sm-12 border bg-white p-4">
    <div class='col-lg-12'>

    {!! Form::open(['url' => route('journals.store'), 'files' => 'true']) !!}

    <div class="form-group">
        {{ Form::label('image', 'Main Image') }}<br />
        {{ Form::file('image', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Name (unique)') }}
        {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('author_image', 'Author Image') }}<br />
        {{ Form::file('author_image', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('author_name', 'Author Name') }}
        {{ Form::text('author_name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('author_detail', 'Author Detail') }}
        {{ Form::textarea('author_detail', null, array('class' => 'form-control my-editor', 'rows' => 3)) }}
    </div>

    <div class="form-group">
        {{ Form::label('content', 'Content') }}
        {{ Form::textarea('content', null, array('class' => 'form-control my-editor', 'rows' => 20)) }}
    </div>

    <div class="form-group">
        {{ Form::label('featured', 'Featured ') }}

        {{ Form::hidden('featured', 0) }}
        {{ Form::checkbox('featured', 1, false) }}
    </div>

    <div class="form-group">
        {{ Form::label('hidden', 'Hidden ') }}

        {{ Form::hidden('hidden', 0) }}
        {{ Form::checkbox('hidden', 1, true) }}
    </div>

    <div class="form-group">
        {{ Form::label('released_at', 'Released At ') }}
        {{ Form::date('released_at', Carbon\Carbon::now(), array('class' => 'form-control my-editor', 'required')) }}
    </div>
    
    <a class="btn btn-link" href="{{ route('journals.index') }}">Back</a> 
    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection

@section('js')
@parent
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endsection