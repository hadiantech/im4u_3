@extends('layouts.front.master') @section('content')
<!--  header -->
<div class="container-fluid gray-bg p-0" style="min-height:90vh">
    <div class="container white-bg py-5" style="min-height:90vh">
        <div class="row">
            <div class="col-md-12 p-4">
                <h1 class="blue-text">THE PEOPLE'S JOURNAL ARCHIVE</h1>
            </div>
        </div>
        <div class="row mt-2">
            {!! Form::open(['url' => route('journal.archive') , 'class' => 'form-inline']) !!}
            <div class="col-md-3">
                {{ Form::select('year', $year_data, $year, ['class' => 'form-control p-2 ml-3', 'style' => 'min-width:120px', 'placeholder' => 'Select Year']) }}
            </div>

            <div class="col-md-3">
                {{ Form::select('month', $month, ['class' => 'form-control p-2 ml-3', 'style' => 'min-width:130px', 'placeholder' => 'Select Month']) }}
            </div>
            
            <div class="col-md-3 ml-4">
                <button class="btn btn-success btn-sm white-text"><i class="fa fa-search"></i> Search</button>
            </div>
            {!! Form::close() !!}
        </div>

        {{-- Archive listing --}}
        <div class="row mt-5 px-3">
            @foreach($journals as $journal)
            <a href="{{ route('journals.show', $journal->name) }}" style="width:100%">
                <div class="col-md-12 border-bottom p-3 tpj-list">
                    <p class="mb-0">{{ Carbon\Carbon::parse($journal->released_at)->format('M d, Y') }}</p>
                    <p class="bold mb-0">{{ $journal->title }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>
    
</div>
<!--  end  -->
@endsection 
@section('js') 
@parent
@endsection