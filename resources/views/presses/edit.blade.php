@extends('layouts.admin.master')

@section('title', 'Press Release')
@section('subtitle', '| Edit Press')

@section('content')

<div class="col-sm-12 border bg-white p-4">
    <div class='col-lg-12'>

    {{ Form::model($press, array('route' => array('presses.update', $press->id), 'method' => 'PUT', 'data-parsley-validate')) }}

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
    </div>


    <div class="form-group">
        {{ Form::label('link', 'Link') }}
        {{ Form::url('link', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('released_at', 'Released at') }}
        {{ Form::date('released_at', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('highlight', 'Highlight ') }}

        {{ Form::hidden('highlight', 0) }}
        {{ Form::checkbox('highlight', 1, null) }}
    </div>

    <div class="form-group">
        {{ Form::label('hidden', 'Hidden ') }}

        {{ Form::hidden('hidden', 0) }}
        {{ Form::checkbox('hidden', 1, null) }}
    </div>

    <a class="btn btn-link" href="{{ route('presses.index') }}">Back</a> 
    {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    </div>
</div>

<br />
<br />
<div class="col-sm-12 border bg-white p-4">
    <div class='col-lg-12'>
        Image (optional)
        <hr />
        {{ Form::model($press, array('route' => array('presses.image.update', $press->id),'files' => true, 'method' => 'PUT', 'data-parsley-validate')) }}

        @if(empty($press->getMedia('press')->first()))
        <img style="max-height: 100px;"  src="{{ url('img/no_image.png') }}">
        @else
        <img style="max-height: 100px;" src="{{ $press->getMedia('press')->first()->getUrl() }}" />
        @endif

        <div class="form-group">
            {{ Form::label('image', 'Image') }}<br />
            {{ Form::file('image', null, array('class' => 'form-control', 'required')) }}
        </div>

        <br />
        {{ Form::submit('Save Picture', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}

        <br />
        {!! Form::open(['method' => 'DELETE', 'route' => ['presses.image.delete', $press->id] ]) !!}
        {!! Form::submit('Remove Picture', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>
</div>

@endsection
