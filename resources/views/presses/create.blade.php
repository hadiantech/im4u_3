@extends('layouts.admin.master')

@section('title', 'Press Release')

@section('subtitle', 'Add Press')

@section('content')

<div class="col-sm-12 border bg-white p-4">
    <div class='col-lg-12'>

    {{ Form::open(array('url' => 'presses', 'files' => true, 'data-parsley-validate')) }}

    <div class="form-group">
        {{ Form::label('image', 'Image (optional)') }}<br />
        {{ Form::file('image', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('title', 'Title') }}
        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
    </div>


    <div class="form-group">
        {{ Form::label('link', 'Link') }}
        {{ Form::url('link', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('released_at', 'Released at') }}
        {{ Form::date('released_at', \Carbon\Carbon::now(), array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('highlight', 'Highlight ') }}

        {{ Form::hidden('highlight', 0) }}
        {{ Form::checkbox('highlight', 1, false) }}
    </div>

    <div class="form-group">
        {{ Form::label('hidden', 'Hidden ') }}

        {{ Form::hidden('hidden', 0) }}
        {{ Form::checkbox('hidden', 1, false) }}
    </div>


    <a class="btn btn-link" href="{{ route('presses.index') }}">Back</a> 
    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection
