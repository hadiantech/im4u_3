@extends('layouts.admin.master')

@section('title', 'Press Release')

@section('content')

<div class="card">
    <div class="card-body">
        @can('add_presses')
        <a href="{{ route('presses.create') }}" class="btn btn-success">
            <i class="fas fa-plus"></i> New
        </a>
        @endcan
    </div>
</div>


<br />
<table id="datatable-id" class="table table-bordered">
    <thead>
        <tr>
            <th>Title</th>
            <th>Link</th>
            <th>Released At</th>
            <th>Highlight</th>
            <th>Hidden</th>
            <th>Action</th>
        </tr>
    </thead>
</table>

@endsection

@section('js')
@parent
<script>
$(document).ready(function(){
    var oTable = $('#datatable-id').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('datatables.presses') !!}',
        columns: [
            { data: 'title', name: 'title' },
            { data: 'link', name: 'link' },
            { data: 'released_at', name: 'released_at' },
            { data: 'highlight_check', name: 'highlight' },
            { data: 'hidden_check', name: 'hidden' },
            { data: 'action', orderable: false, searchable: false}
        ]
    });

});
</script>


@endsection
