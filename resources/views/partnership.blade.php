@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/happening/happenings_header.jpg)">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">PARTNERSHIPS AND COLLABORATIONS</h1>
            <h5 class="white-text text-shadow">We move your business forward.</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  collaborate  -->
<div id="collaborate" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="blue-text">COLLABORATE WITH US</h1>
                <h3 class="red-text">Corporate Clients & NGOs</h3>
            </div>
        </div>
        <div class="row align-items-center mb-0">
            <div class="col-sm-12 col-md-8">
                <p>If your organisation is passionate about programs related to the environment, community well being, sports, arts & culture and even skills development, why not collaborate with us? Let’s work together to create more awareness and achieve
                    maximum impact!</p>
                <p>Let’s co-create unique, mutually beneficial partnerships. We can showcase your company as a leading supporter of volunteerism, engage your staff and clients and increase the opportunities which we can offer to the Malaysian communities.</p>
            </div>
            <div class="col-sm-4 col-md-3 d-sm-none d-md-block">
                <img src="img/happening/collaborate_icon.png" class="img-fluid" />
            </div>
        </div>
        <div class="row pb-3">
            <div class="col-sm-12">
                <a href="javascript:;" class="btn btn-red">Collaborate Now</a>
            </div>
        </div>
    </div>
</div>

<!--  partner  -->
<div id="partners" class="container-fluid p-0 pt-5 pb-5" style="background:#1488c8;">
    <div class="container pt-4 pb-4">
    <div class="row">
        <div class="col-sm-12 pb-4">
            <h1 class="white-text">OUR PARTNERS</h1>
        </div>
    </div>
    <div class="row p-0 mb-4 no-gutters">
        <div class="col-sm-2"><img src="img/happening/logos/1.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/2.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/3.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/4.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/5.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/6.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/7.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/8.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/9.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/10.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/11.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/12.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/13.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/14.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/15.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/16.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/17.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/18.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/19.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/20.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/21.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/22.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/23.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/24.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/25.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/26.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/27.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/28.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/29.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/30.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/31.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/32.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/33.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/34.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/35.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/36.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/37.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/38.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/39.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/40.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/41.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/42.png" class="img-fluid hover-zoom" /></div>
    </div>
    </div>
</div>

<!--  collaboration stories  -->
<div id="collaboratestories" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12 pb-4">
                <h1 class="blue-text">COLLABORATIONS STORIES HIGHLIGHTS</h1>
            </div>
        </div>
        <div class="row">
            <div class="slick-collab">
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 pb-3 dotdotdot-title p-3 text-center">Spritzer, MyBurgerlab</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">APE Malaysia</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">AEON CO. (M) BHD</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">The Bodyshop Malaysia</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/front/thumb.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 dotdotdot-title p-3 text-center">Canon Malaysia & Northern Corridor Implementation Authority (NCIA)</h5>
                    <a href="javascript:;" class="btn btn-red mb-3">READ STORY</a>
                </div>
            </div>
        </div>
    </div>
</div>


<!--  end  -->


@endsection @section('js') @parent @endsection