@extends('layouts.front.master') @section('content')

<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <h2>Create New Project</h2>
            <p>*All information will not be saved until you have completed the last step</p>
        </div>
    </div>
    <div class="row no-gutters mt-2">
        <div class="card mb-3 col-sm-12">
            <!-- select project type-->
            <div class="card-header bold">
                Select Project Type
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <label class="form-label">What is the type of your project?</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="radio-ind" value="ind" name="org-selector" class="custom-control-input">
                        <label class="custom-control-label" for="radio-ind">Individual</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="radio-org" value="org" name="org-selector" class="custom-control-input">
                        <label class="custom-control-label" for="radio-org">Organisation</label>
                    </div>
                </div>
            </div>
        </div>
        <!--organisation-->
        <div class="card col-sm-12 my-3 mt-2 p-0 hide" id="org-form">
            <div class="card-header bold">
                Organisation Detail
            </div>
            <div class="card-body">
                <!--organisation selector-->
                <div class="col-md-12">
                    <div class="form-group mb-2">
                        <label>Choose an organisation. If your organisation is not listed , you may add a new one.</label>
                    </div>
                    <div class="form-group">
                        <a href="javascript:;" id="addnew-org" class="btn btn-primary mr-2">Add New Organisation</a>
                        <a id="cancel-add-new" href="javascript:;" class="btn btn-warning mr-2 hide">Cancel</a>
                    </div>
                </div>
                
                <!--select old organisation-->
                <div class="col-md-12" id="old-org">
                    <div class="form-group">
                        <label>Select your organisation</label>
                        {{ Form::select('size',['1' => 'Organisation Name 1', '2' => 'Organisation Name 2'],null,['placeholder' => 'Select',
                        'class' => 'custom-select'])}}
                    </div>
                </div>
                <!--add new org form-->

                <div class="col-md-12 my-3 hide" id="new-org">
                    <div class="row">
                        <div class="col-md-12 border rounded p-4">
                            <div class="form-group">
                                <label class="form-label">Organisation Details</label>
                                <br/> 
                                {{ Form::label('org_name', 'Organisation Name') }} 
                                {{ Form::text('org_name', null, array('class'=> 'form-control', 'required')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('org_desc', 'Description (optional) ') }} 
                                {{ Form::textarea('org_desc', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('website', 'Organisation Website (optional) ') }} 
                                {{ Form::text('website', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="row">
                                <div class="col-md-6 px-0">
                                    <div class="form-group">
                                        <label>
                                            <h5>
                                                <b>Contact Person Details</b>
                                            </h5>
                                        </label>
                                        <br/> {{ Form::label('org_contact_name', 'Contact Person Name') }} {{ Form::text('org_contact_name',
                                        null, array('class' => 'form-control', 'required')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('org_contact_num', 'Contact Person Number') }} {{ Form::text('org_contact_num', null, array('class' => 'form-control',
                                        'required')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('org_email', 'Email') }} {{ Form::text('org_email', null, array('class' => 'form-control', 'required')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-proj-detail">NEXT</a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 my-3 hide" id="proj-detail-form">
            <div class="card-header bold">
                Project Detail
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('proj_title', 'Insert Your Project Title',array('class'=>'form-label')) }}
                        <p class="sub2">
                            <i>Be creative! Awesome Title = More Attention!</i>
                        </p>
                        {{ Form::text('proj_title', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('proj_tagline', 'Insert your Project Tagline!',array('class'=>'form-label')) }}
                        <p class="sub2">
                            <i>In 160 characters, tell your audience what your project is about</i>
                        </p>
                        {{ Form::textarea('proj_title', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('proj_additional_info', 'Additional Information',array('class'=>'form-label')) }}
                        <p class="sub2">
                            <i>Please fill any additional information that your volunteers need to know.</i>
                        </p>
                        {{ Form::textarea('proj_additional_info', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="form-group">
                        <label class="form-label">When is your project?</label>
                        <div class="row">
                            <div class="col-md-2 px-0 mr-2">
                                {{ Form::label('proj_date_start', 'Start Date') }} {{ Form::date('proj_date_start', \Carbon\Carbon::now() ,array('class'
                                => 'form-control', 'required')) }}
                            </div>
                            <div class="col-md-2 px-0 mr-2">
                                {{ Form::label('proj_date_end', 'End Date') }} {{ Form::date('proj_date_end', \Carbon\Carbon::now() ,array('class' => 'form-control',
                                'required')) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 px-0">
                                {{ Form::label('proj_time_start', 'Start Time') }}
                            </div>
                            <div class="col-md-1 px-0 mr-2">

                                {{ Form::select('proj_time_start', [ '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7',
                                '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12' ], null, ['class'=>'custom-select']
                                )}}

                            </div>
                            <div class="col-md-1 px-0 mr-1">
                                {{ Form::select('proj_time_ampm_start', ['am' => 'AM', 'pm' => 'PM'],null,['class'=> 'custom-select']) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 px-0">
                                {{ Form::label('proj_time_end', 'End Time') }}
                            </div>
                            <div class="col-md-1 px-0 mr-2">

                                {{ Form::select('proj_time_end', [ '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8'
                                => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12' ], null, ['class'=>'custom-select']
                                )}}

                            </div>
                            <div class="col-md-1 px-0 mr-1">
                                {{ Form::select('proj_time_ampm_end', ['am' => 'AM', 'pm' => 'PM'],null,['class'=> 'custom-select']) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('proj_hours', 'How many hours do you need your volunteers for?',array('class'=>'form-label')) }} {!! Form::number('proj_hours',
                        1.00, array('min' => '1','class' => 'form-control', 'required')) !!}
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-proj-location">NEXT</a>
                </div>
            </div>
        </div>

        <div class="card col-sm-12 my-3 px-0 hide" id="proj-location">
            <div class="card-header bold">
                Project Location
            </div>
            <div class="card-body">
                <div class="row col-md-6 px-0">
                    <div class="col-md-12">
                        <label class="form-label">Enter your project location address</label>
                    </div>
                    <div class="col-md-12">
                        {{ Form::label('add_1', 'Address 1') }} {{ Form::text('add_1', null, array('class' => 'form-control mb-2', 'required')) }}
                        {{ Form::label('add_2', 'Address 2') }} {{ Form::text('add_2', null, array('class' => 'form-control
                        mb-2', 'required')) }}
                    </div>
                    <div class="col-md-6 mb-2">
                        {{ Form::label('city', 'City') }} {{ Form::text('city', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="col-md-6 mb-2">
                        {{ Form::label('state', 'State') }} {{ Form::text('state', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="col-md-6 mb-2">
                        {{ Form::label('country', 'Country') }} {{ Form::text('country', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="col-md-6 mb-2">
                        {{ Form::label('postcode', 'Postcode') }} {{ Form::text('postcode', null, array('class' => 'form-control', 'required')) }}
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-vol-details">NEXT</a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 my-3 px-0 hide" id="vol-details">
            <div class="card-header bold">
                Volunteer Details
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::label('vol_num', 'Number of volunteer needed', array('class'=>'form-label')) }} {!! Form::number('vol_num', 1.00,
                        array('min' => '1','class' => 'form-control', 'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label ml-3">Pick the category that reflect your project the most</label>
                    <div class="col-md-12 d-flex">
                        <div class="custom-control custom-checkbox mr-3">
                            <input type="checkbox" class="custom-control-input" id="cat_1">
                            <label class="custom-control-label" for="cat_1">Youth Development</label>
                        </div>
                        <div class="custom-control custom-checkbox mr-3">
                            <input type="checkbox" class="custom-control-input" id="cat_2">
                            <label class="custom-control-label" for="cat_2">Community Service</label>
                        </div>
                        <div class="custom-control custom-checkbox mr-3">
                            <input type="checkbox" class="custom-control-input" id="cat_3">
                            <label class="custom-control-label" for="cat_3">Fitness & Health</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">What are your volunteers going to do?</label>
                            <p class="sub2">
                                <i>Let your volunteers know what they will be assigned to. Keep it short and simple</i>
                            </p>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">1</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">2</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">3</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">What will you provide for your volunteers?</label>
                            <p class="sub2">
                                <i>List up 3 items that you will be giving your volunteer in preparation for the event</i>
                            </p>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">1</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">2</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">3</div>
                                </div>
                                <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-proj-photo">NEXT</a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 my-3 px-0 hide" id="proj-photo">
            <div class="card-header bold">
                Project Photo
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8 mb-2 d-flex flex-column justify-content-center align-items-center">
                            <label class="form-label">Project Photo</label>
                            <label class="bg-image-label btn btn-green" style="width:150px" for="i1">
                                <i class="bb bb-picture bb-3x"></i>
                                <div class="text">Browse</div>
                            </label>
                        </div>
                        <div class="col-md-4 d-flex flex-column justify-content-center align-items-center">
                            <label class="form-label">Project thumbnail</label>
                            <label class="bg-image-label btn btn-green" style="width:150px" for="i2">
                                <i class="bb bb-picture bb-3x"></i>
                                <div class="text">Browse</div>
                            </label>
                        </div>
                        
                        <div class="col-md-8">
                            <input type="file" name="" id="i1" onchange="readURL(event)" style="display:none" />
                            <div class="bg-image project-banner-preview" id="bg-image-i1">
                                <div class="white-bg black-text p-2 border rounded alpha-1"><b>Banner Preview</b></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <input type="file" name="" id="i2" onchange="readURL(event)" style="display:none" />
                            <div class="bg-image project-thumb-preview" id="bg-image-i2">
                                <div class="white-bg black-text p-2 border rounded alpha-1"><b>Thumbnail Preview</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row">
                    <a href="javascript:;" class="btn btn-blue btn-block" id="">SUBMIT</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection @section('js') @parent
<script src="{{ asset('js/org-selector.js') }}"></script> @endsection
