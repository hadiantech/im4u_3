@extends('layouts.front.master') @section('content')
<!--  header -->
<div class="container-fluid gray-bg p-0 " style="min-height: 90vh">
    <div class="container white-bg py-5"  style="min-height: 90vh">
        <div class="row">
            <div class="col-md-12 pl-4 pr-4 pb-3 pt-3">
                <h1 class="blue-text">PRESS RELEASE ARCHIVE</h1>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-3 d-flex align-items-center">
                <input id="search" type="text" class="form-control mr-1" placeholder="Enter text to search..." /><i class="fas fa-search"></i>
            </div>
            <div class="col-md-3">
                @php
                $now = Carbon\Carbon::now()->year;
                $last = 2014;
                @endphp
                <select id="year_select" class="form-control" title="Filter by year">
                    @for ($i = $now; $i >= $last; $i--)
                    <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>
        </div>

        {{-- Archive listing --}}
        
        <div id="data_listing" class="row mt-5 px-3">
            
        </div>

    </div>
    
</div>
<!--  end  -->

@endsection 
@section('js') 
@parent

<script>

    $( document ).ready(function() {
        call_ajax({{ Carbon\Carbon::now()->year }});
    });

    $('#year_select').on('change', function() {
        call_ajax(this.value);
    });  

    $("#search").keyup(function() {
        var value = this.value.toLowerCase();

        $("#data_listing").find("a").each(function(index) {
            
            //if (!index) return;
            var item = $(this).find(".bold").first().text();
            $(this).toggle(item.toLowerCase().indexOf(value) !== -1);
        });
    });

    function call_ajax(year){
        
        $.ajax({
            type:'GET',
            url:'/presses/year/'+year,
            success:function(data){
                append_archive(data);
            }
        });
    }  

    function append_archive(data){
        $('#data_listing').empty();
        $('#search').val('');

        data.forEach(data => {
            $("#data_listing").append(`
            <a href="`+data['link']+`" target="_blank" style="width:100%">
                <div class="col-md-12 border-bottom p-3 tpj-list">
                    <p class="mb-0">`+data['released_at']+`</p>
                    <p class="bold mb-0">`+data['title']+`</p>
                </div>
            </a>
            `);
        });
    }
</script>

@endsection