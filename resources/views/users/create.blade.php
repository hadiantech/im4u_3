@extends('layouts.admin.master')

@section('title', 'User Management')
@section('subtitle', 'Create')

@section('content')

<div class="row">
    <div class='col-lg-12'>

    {{ Form::open(array('url' => 'users')) }}

    <div class="form-group">
        {{ Form::label('email', 'E-Mail Address') }}
        {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password_confirmation', 'Confirm Password') }}
        {{ Form::password('password_confirmation', array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('roles_label', 'Roles') }}
        @foreach($roles as $role)
        <br />
        {{ Form::checkbox('roles[]', $role->id) }}
        {{ Form::label('roles', $role->name) }} 
        @endforeach
    </div>

    {{ Form::submit('Add', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection