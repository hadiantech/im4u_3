@extends('layouts.admin.master')

@section('title', 'User Management')
@section('subtitle', 'Edit')

@section('content')

<div class="row">
    <div class='col-lg-12'>

    {{ Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'E-Mail Address') }}
        {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('roles_label', 'Roles') }}
        @foreach($roles as $role)
        <br />
        {{ Form::checkbox('roles[]', $role->id) }}
        {{ Form::label('roles', $role->name) }} 
        @endforeach
    </div>

    <a class="btn btn-link" href="{{ url()->previous() }}">Back</a>
    {{ Form::submit('Save', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection