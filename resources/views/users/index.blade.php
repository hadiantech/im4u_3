@extends('layouts.admin.master')

@section('title', 'User Management')

@section('content')

<div class="card">
    <div class="card-body">
        @can('add_users')
            <a href="{{route('users.create')}}" class="btn btn-success">
                <i class="fas fa-plus"></i> New
            </a>
        @endcan
    </div>
</div>

<br />
<table id="users-table" class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Roles</th>
        <th>Created At</th>
        <th>Action</th>
    </tr>
    </thead>
</table>

@endsection

@section('js')
@parent

<script>
    $(function () {
        $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/datatables/getUser',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'name'},
                {data: 'email'},
                {data: 'roles', name: 'roles.name', orderable: false},
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>

@endsection