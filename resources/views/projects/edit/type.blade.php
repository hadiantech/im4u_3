<div class="tab-pane fade" id="type" role="tabpanel" aria-labelledby="pills-contact-tab">
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-certificate"></i>  Project Type 
                </div>
                <div class="card-body" id="proj-detail-form">

                    @isset($project->organisation)
                        Project Type : Organisation<br />
                        Organisation Name : {{ $project->organisation->name }}<br />
                        Organisation description : {{ $project->organisation->description }}<br />
                        Organisation website : {{ $project->organisation->website }}<br />
                        Contact Person Name : {{ $project->organisation->incharge_name }}<br />
                        Contact Person Number : {{ $project->organisation->incharge_phone }}<br />
                        Contact Person Email : {{ $project->organisation->incharge_email }}

                    @else
                        Project Type : Individual
                    @endisset

                    <br />
                    <br />
                    <a class="btn btn-primary" data-toggle="collapse" href="#edittype" role="button" aria-expanded="false" aria-controls="collapseExample">
                        Edit Project Type
                    </a>
                    <br />
                    <br />


                    {!! Form::model($project, ['route' => ['projects.update.type', $project->id], 'method' => 'PUT', 'class'=> 'collapse', 'id'=> 'edittype']) !!}

                    <!-- select project type-->
                    <div class="form-group">
                        <label>What is the type of your project?</label>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio-ind" value="ind" name="org_selector" class="custom-control-input">
                            <label class="custom-control-label" for="radio-ind">Individual</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio-org" value="org" name="org_selector" class="custom-control-input">
                            <label class="custom-control-label" for="radio-org">Organisation</label>
                        </div>
                    </div>

                    <!--organisation-->
                    <div id="org-form" class="hide">
                        <div class="form-group">
                            <div class="form-group mb-2">
                                <label>Choose an organisation. If your organisation is not listed , you may add a new one.</label>
                            </div>
                            <div class="form-group">
                                <a href="javascript:;" id="addnew-org" class="btn btn-primary mr-2">Add New Organisation</a>
                                <a id="cancel-add-new" href="javascript:;" class="btn btn-warning mr-2 hide">Cancel</a>
                            </div>
                        </div>

                        <!--select old organisation-->
                        <div id="old-org" class="form-group">
                            <label>Select your organisation</label>
                            {{ Form::select('organisation_id', $organisations, null, ['placeholder' => 'Select','class' => 'custom-select', 'id' => 'organisation_id'])}}
                        </div>

                        <!--add new org form-->
                        <div class="form-group hide" id="new-org">
                            <div class="row">
                                <div class="col-md-12 border rounded p-4">
                                    <div class="form-group">
                                        <label class="form-label">Organisation Details</label>
                                        <br/> 
                                        {{ Form::label('name', 'Organisation Name') }} 
                                        {{ Form::text('name', null, array('class'=> 'form-control')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('description', 'Description') }} 
                                        {{ Form::textarea('description', null, array('class' => 'form-control')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('website', 'Organisation Website (optional) ') }} 
                                        {{ Form::url('website', null, array('class' => 'form-control')) }}
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 px-0">
                                            <div class="form-group">
                                                <label>
                                                    <h5>
                                                        <b>Contact Person Details</b>
                                                    </h5>
                                                </label>
                                                <br/> {{ Form::label('incharge_name', 'Contact Person Name') }} {{ Form::text('incharge_name',
                                                null, array('class' => 'form-control')) }}
                                            </div>
                                            <div class="form-group">
                                                {{ Form::label('incharge_phone', 'Contact Person Number') }} {{ Form::text('incharge_phone', null, array('class' => 'form-control')) }}
                                            </div>
                                            <div class="form-group">
                                                {{ Form::label('incharge_email', 'Email') }} {{ Form::email('incharge_email', null, array('class' => 'form-control')) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@section('js') 
@parent

<script src="{{ asset('js/org-selector.js') }}"></script> 

@endsection