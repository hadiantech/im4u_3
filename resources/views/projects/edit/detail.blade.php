<div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="pills-home-tab">
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-user-circle"></i> Project Detail
                </div>
                <div class="card-body">
                    {!! Form::model($project, ['route' => ['projects.update.detail', $project->id], 'method' => 'PUT']) !!}
                        <div class="form-group">
                            {{ Form::label('title', 'Title') }} 
                            {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('tagline', 'Tagline') }} 
                            {{ Form::textarea('tagline', null, array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('information', 'Additional Information') }} 
                            {{ Form::textarea('information', null, array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('started_at', 'Start Date') }} 
                            {{ Form::date('started_at', null ,array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('ended_at', 'End Date') }} 
                            {{ Form::date('ended_at', null ,array('class' => 'form-control','required')) }}  
                        </div>

                        <div class="form-group">
                            {{ Form::label('time_started_at', 'Start Time') }}
                            {{ Form::text('time_started_at', null, array('class' => 'form-control clockpicker', 'required')) }}
                        </div>
                        
                        <div class="form-group">
                            {{ Form::label('time_ended_at', 'End Time') }}
                            {{ Form::text('time_ended_at', null, array('class' => 'form-control clockpicker', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('hours', 'Hours') }} 
                            {!! Form::number('hours', null, array('min' => '1','class' => 'form-control', 'required')) !!}
                        </div>

                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@section('js')
@parent

<script>
    $('.clockpicker').clockpicker({
        donetext: 'Done', 
    });
</script>

@endsection