<div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="pills-contact-tab">
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-certificate"></i>  Project Location 
                </div>
                <div class="card-body">
                    {!! Form::model($project, ['route' => ['projects.update.location', $project->id], 'method' => 'PUT']) !!}
                    <div class="form-group">
                        {{ Form::label('address_1', 'Address 1') }} 
                        {{ Form::text('address_1', null, array('class' => 'form-control mb-2', 'required')) }}
                    </div>

                    <div class="form-group">
                            {{ Form::label('address_2', 'Address 2 (optional)') }} 
                            {{ Form::text('address_2', null, array('class' => 'form-control mb-2')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('city', 'City') }} 
                        {{ Form::text('city', null, array('class' => 'form-control', 'required')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('postcode', 'Postcode') }} 
                        {{ Form::text('postcode', null, array('class' => 'form-control', 'required')) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('state', 'State') }} 
                        <select id="state" name="state" class="form-control select-fix-margin {{ $errors->has('state') ? ' is-invalid' : '' }}" required>
                            @include('shared._state_dropdown', ['model'=>$project])
                        </select>
                    </div>
                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>
    </div>
</div>