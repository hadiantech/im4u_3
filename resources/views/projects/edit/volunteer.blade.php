<div class="tab-pane fade" id="vdetail" role="tabpanel" aria-labelledby="pills-contact-tab">
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-certificate"></i> Volunteer Detail
                </div>
                <div class="card-body">
                    {!! Form::model($project, ['route' => ['projects.update.volunteer', $project->id], 'method' => 'PUT']) !!}
                    
                    {{-- <div class="form-group">
                        {{ Form::label('number_volunteer', 'Number of volunteer needed') }}
                        {!! Form::number('number_volunteer', null, array('min' => '1','class' => 'form-control', 'required')) !!}
                    </div> --}}

                    <div class="form-group">
                        @php
                        $left_assignment = 0;
                        $left_rewards = 0;
                        @endphp
                        {{ Form::label('assignments', 'What are your volunteers going to do?') }}
                        @foreach($project->assignments as $assignment)
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ $loop->iteration }}</div>
                            </div>
                            <input name="assignments[]" value="{{ $assignment->detail }}" type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                        </div>
                        @php
                        $left_assignment = $loop->iteration;
                        @endphp
                        @endforeach

                        @for ($i = $left_assignment+1; $i < 4; $i++)
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ $i }}</div>
                            </div>
                            <input name="assignments[]" type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                        </div>
                        @endfor
                    </div>

                    <div class="form-group">
                        {{ Form::label('rewards', 'What will you provide for your volunteers?') }}
                        @foreach($project->rewards as $reward)
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ $loop->iteration }}</div>
                            </div>
                            <input name="rewards[]" value="{{ $reward->detail }}" type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                        </div>
                        @php
                        $left_rewards = $loop->iteration;
                        @endphp
                        @endforeach

                        @for ($i = $left_rewards+1; $i < 4; $i++)
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">{{ $i }}</div>
                            </div>
                            <input name="rewards[]" type="text" class="form-control" id="inlineFormInputGroup" placeholder="">
                        </div>
                        @endfor
                    </div>

                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>