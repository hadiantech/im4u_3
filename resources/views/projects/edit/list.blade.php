<div class="tab-pane fade" id="vlist" role="tabpanel" aria-labelledby="pills-contact-tab">
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-certificate"></i> Volunteer List : {{ $project->volunteers->count() }}
                </div>
                <div class="card-body">
                    <table id="users-table" class="table table-bordered" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Phone Number</th>
                            <th>Email Address</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@section('js')
@parent

<script>
    $(function () {
        var oTable = $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: {
                url: '{!! route('datatables.joined', $project->id) !!}'
            },
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'name', name: 'name'},
                {data: 'phone_number', name: 'phone_number'},
                {data: 'email', name: 'email'}
            ]
        });
    });
</script>
@endsection