<div class="tab-pane fade" id="category" role="tabpanel" aria-labelledby="pills-category-tab">
    <div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-certificate"></i>  Categories
                </div>
                <div class="card-body">
                    {!! Form::model($project, ['route' => ['projects.update.categories', $project->id], 'method' => 'PUT']) !!}

                    @foreach($categories as $category)
                    <div class="custom-control custom-checkbox mr-3">
                        <input type="checkbox" class="custom-control-input" name="category[]" {{ $project->categories->contains($category->id)  ? 'checked' : ''}}  value="{{ $category->id }}" id="cat_{{ $loop->iteration }}">
                        <label class="custom-control-label" for="cat_{{ $loop->iteration }}">{{ $category->name }}</label>
                    </div>
                    @endforeach

                    <br />
                    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>