@extends('layouts.admin.master') 
@section('content')

<div class="container mt-5">
    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-3 p-0 border rounded white-bg m-2">
            <div class="row" style="position:absolute; top:10px; right:10px">
                @if($project->status == "pending")
                <span class="badge badge-warning"  style="font-size:15px">Pending Approval</span>
                @elseif($project->status == "rejected")
                <span class="badge badge-danger"  style="font-size:15px">Rejected</span>
                @elseif($project->status == "suspended")
                <span class="badge badge-info"  style="font-size:15px">Suspended</span>
                @else
                <span class="badge badge-success" style="font-size:15px">Published</span>
                @endif
            </div>
            <div class="row">
                @if(!$project->getMedia('thumbnail')->first())
                    <img class="card-img-top rounded img-fluid mb-3" src="http://via.placeholder.com/300x300"> @else
                    <img class="card-img-top rounded img-fluid mb-3" src="{{ $project->getMedia('thumbnail')->first()->getUrl()}} "> @endif
                <div class="row p-3">
                    @include('shared._upload_image', ['collection' => 'thumbnail', 'model_name' => 'Project', 'model_id' => $project->id])
                </div>
                
            </div>
            <div class="row p-3">
                <div class="col-md-12">
                    <h3 class="mb-3">{{ $project->title }}</h3>
                </div>

                @can('edit_projects')
                <div class="col-md-12 border rounded p-3">
                    @if($project->status == "pending" || $project->status == "suspended")
                    <a href="{{ route('projects.update.status', [$project->id, 'published']) }}" class="btn btn-success btn-med btn-block">Approve this project</a>
                    <a href="{{ route('projects.update.status', [$project->id, 'rejected']) }}" class="btn btn-warning btn-med btn-block">Reject this project</a>
                    @elseif($project->status == "rejected")
                    <a href="{{ route('projects.update.status', [$project->id, 'published']) }}" class="btn btn-success btn-med btn-block">Approve this project</a>
                    @else
                    <a href="{{ route('projects.update.status', [$project->id, 'suspended']) }}" class="btn btn-warning btn-med">Unpublished this project</a>
                    @endif
                </div>
                @endcan  

                <div class="col-md-12 border rounded p-3 mt-2">
                    <a href="{{ route('projects.show', $project->id) }}" class="btn btn-info btn-med btn-block">View this project</a>
                </div>

            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-8 p-2 border rounded white-bg m-2">
            <div class="row">
                <div class="col-md-12 project-edit-banner" style="background-image:url('{{ $project->getMedia('banner')->first()->getUrl()}}');"></div>
                @include('shared._upload_image', ['collection' => 'banner', 'model_name' => 'Project', 'model_id' => $project->id])
            </div>
            <div class="row border-bottom">
                <div class="col-md-12 p-3">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#detail" role="tab" aria-controls="detail"
                                aria-selected="true">Project Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#type" role="tab" aria-controls="type"
                                aria-selected="false">Project Type</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#location" role="tab" aria-controls="location"
                                aria-selected="false">Project Location</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#vdetail" role="tab" aria-controls="location"
                                aria-selected="false">Volunteer Detail</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#category" role="tab" aria-controls="category"
                                aria-selected="false">Categories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#vlist" role="tab" aria-controls="location"
                                aria-selected="false">Volunteer List</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 p-3">
                    <div class="tab-content" id="pills-tabContent">
                        @include('projects.edit.detail')
                        @include('projects.edit.type')
                        @include('projects.edit.location')
                        @include('projects.edit.volunteer')
                        @include('projects.edit.categories')
                        @include('projects.edit.list')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js') 
@parent

@endsection
