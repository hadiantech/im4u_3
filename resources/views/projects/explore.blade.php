@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(img/explore/explore_bg.jpg)">
        <div class="col-md-12 text-center">
        <h1 class="white-text text-shadow">PROJECTS AND EVENTS</h1>
            <h5 class="white-text text-shadow">Take part. Get involved. Make a difference</h5>
        </div>
    </div>
</div>
<!--  end  -->

@if($featured)
<div class="container-fluid" id="recentproject">
    <div class="container pt-5 pb-5">
        <div class="row mb-4">
            <div class="col-sm-12 p-0">
                <h1 class="blue-text">FEATURED PROJECTS</h1>
            </div>
        </div>
        <div class="row">
            <div class="slick-featured-project rounded box-shadow">
                <div class="row box p-0 d-flex">
                    <div class="col-sm-12 col-md-12 col-lg-8" style="background-image: url(img/happening/reachoutconvention.jpg); background-repeat: no-repeat; background-position: center; min-height: 400px;"></div>
                    <div class="col-sm-12 col-lg-4 white-bg p-4">
                        <div class="row">
                            <div class="col-sm-12 p-0 mb-3">
                                <span class="badge badge-primary">PARTICIPATION</span>
                            </div>
                        </div>
                        <div class="row">
                            <h3>Reach Out: Convention & Celebration 2018</h3>
                            <p>So, what do we do? I have a plan. (Biff’s Car) (He drives along, not noticing the flying DeLorean almost right behind him. Biff has the radio on.) (v.o) Repeating tonight's earlier weather bulletin, a severe thunderstorm is
                                heading for Hill Valley.</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 p-0">
                                <p style="font-size:14px">
                                    <i class="fas fa-map-marker mr-2"></i> Taman Perindustrian Puchong, Selangor
                                </p>
                            </div>
                            <div class="col-sm-12 p-0">
                                <p style="font-size:14px">
                                    <i class="far fa-calendar-alt mr-2"></i> Feb 26, 2017 - Feb 27, 2017
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <a href="" class="btn btn-blue btn-block"><i class="far fa-arrow-alt-circle-right"></i> PARTICIPATE TODAY</a>
                        </div>
                    </div>
                </div>
                <div class="row box p-0 d-flex">
                    <div class="col-sm-12 col-md-12 col-lg-8" style="background-image: url(img/happening/reachoutconvention.jpg); background-repeat: no-repeat; background-position: center; min-height: 400px;"></div>
                    <div class="col-sm-12 col-lg-4 white-bg p-4">
                        <div class="row">
                            <div class="col-sm-12 p-0 mb-3">
                                <span class="badge badge-primary">PARTICIPATION</span>
                            </div>
                        </div>
                        <div class="row">
                            <h3>Reach Out: Convention & Celebration 2018</h3>
                            <p>So, what do we do? I have a plan. (Biff’s Car) (He drives along, not noticing the flying DeLorean almost right behind him. Biff has the radio on.) (v.o) Repeating tonight's earlier weather bulletin, a severe thunderstorm is
                                heading for Hill Valley.</p>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 p-0">
                                <p style="font-size:14px">
                                    <i class="fas fa-map-marker mr-2"></i> Taman Perindustrian Puchong, Selangor
                                </p>
                            </div>
                            <div class="col-sm-12 p-0">
                                <p style="font-size:14px">
                                    <i class="far fa-calendar-alt mr-2"></i> Feb 26, 2017 - Feb 27, 2017
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <a href="" class="btn btn-blue btn-block"><i class="far fa-arrow-alt-circle-right"></i> PARTICIPATE TODAY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="container-fluid mt-0 white-bg box-shadow p-3 mb-3">
    <div class="container p-0">
        <div class="row align-items-center" style="height:90px">
            <div class="col-sm-12 col-lg-8">
                <h1 class="blue-text m-0">CURRENT PROJECTS</h1>
            </div>
            <div class="col-sm-12 col-lg-4 justify-content-center align-items-center">
                {!! Form::open(['url' => route('explore'), 'id' => 'explore-form' ]) !!}
                <div class="form-group form-inline m-0">
                    {{ Form::select('category_id', $categories, null, ['placeholder' => 'All Categories','class' => 'form-control col-sm-5 m-1', 'id' => 'category_id'])}}
                    <a class="mx-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('explore-form').submit();">
                        <i class="fas fa-search blue-text"></i>
                    </a>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container mt-4 p-0">
        <div class="row no-gutters">

            @forelse($projects as $project)
            <div class="col-sm-12 col-md-6 col-lg-4 p-3 mb-3">
                <div class="col-sm-12 white-bg p-0 rounded" style="height:620px">
                    <div class="row">
                        <div class="col-sm-12" style="background-image: url({{ $project->getMedia('thumbnail')->first()->getUrl() }}); background-repeat: no-repeat; background-position: center; height: 250px; position: relative;">
                            {{-- <div class="" style="position: absolute; top:15px;"><span class="badge badge-primary p-2 box-shadow">PARTICIPATION</span></div> --}}
                            <div class="" style="position: absolute; top:15px;"><span class="badge badge-success p-2 box-shadow">VOLUNTEER</span></div>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-md-12">
                            <h5 class="dotdotdot-title-s my-3">{{ $project->title }}</h5>
                            <p class="dotdotdot-text" style="margin-bottom:0px; height:90px">{{ $project->tagline }}</p>
                        </div>
                    </div>
                    <div class="row p-3">
                        <div class="col-sm-12">
                            <p style="font-size:14px; height:40px" class="descfix">
                                <i class="fas fa-map-marker mr-2"></i> {{ $project->address_1 }} {{ $project->address_2 }} {{ $project->city }}
                            </p>
                        </div>
                        <div class="col-sm-12">
                            <p style="font-size:14px">
                                <i class="far fa-calendar-alt mr-2"></i> {{ Carbon\Carbon::parse($project->started_at)->format('F j, Y') }} - {{ Carbon\Carbon::parse($project->ended_at)->format('F
                                j, Y') }}
                            </p>
                        </div>
                    </div>
                    <div class="row p-3">
                        {{-- <a href="" class="btn btn-blue btn-block"><i class="far fa-arrow-alt-circle-right"></i> PARTICIPATE NOW</a> --}}
                        <a href="{{ route('projects.show', $project->id) }}" class="btn btn-green btn-block"><i class="far fa-arrow-alt-circle-right"></i> REGISTER NOW</a>
                    </div>
                </div>
            </div>
            @empty
                <p>No projects</p>
            @endforelse

        </div>
    </div>
</div>

<!--  success stories  -->
<div id="stories" class="container-fluid p-0 pt-4 pb-4" style="background:#1488c8;">
        <div class="container pt-5 pb-4">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="text-white">OUR SUCCESS STORIES</h1>
                    <p class="text-white">We are proud to share some of the success stories that we’ve had throughout the years. Since it was first launched in 2012, Volunteer Malaysia has continued to inspire new generation of youths to be actively involved in volunteer activities through our
                        successful and meaningful programs.</p>
                </div>
            </div>
    
            <div class="row">
                <div class="slick-item-success">
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/reachout2017/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#reachout2017">
                                <h3 class="white-text">Reach Out Convention & Celebration 2017</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Volunteer Malaysia Reach Out Convention & Celebration celebrated its 5th year milestone back in February when they were in Batu Pahat, Johor for the first round of Reach Out 2017. The program made its way through several other states in Malaysia until the 18th of March including Pahang, Kedah, Negeri Sembilan & Melaka.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#reachout2017" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/reachoutrun2016/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#reachoutrun2016">
                                <h3 class="white-text">Reach Out Run 2016</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Reach Out Run: Run For A Cause, Volunteer Malaysia’s charity run, returned for its second year in 2016. Close to 4,000 people took part in this year’s run that aimed to raise a total of RM80,000 for four charity organizations; Juara Turtle Project, Sabah Wetlands Conservation Society, Project WHEE! and Malaysian Rare Disorders Society. Each organization is expected to receive RM20,000.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#reachoutrun2016" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/vma2016/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#vma2016">
                                <h3 class="white-text">Volunteer Malaysia Awards 2016</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Volunteer Malaysia celebrates volunteerism with its first ever Volunteer Malaysia Awards, a Prime Minister’s Award ceremony held at Sunway Resort Hotel and Spa. This prestigious award ceremony sees various people from different backgrounds coming together to celebrate the outstanding achievements of those who have volunteered and contributed positively towards the betterment of our community.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#vma2016" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/vm2015/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#vm2015">
                                <h3 class="white-text">Volunteer Malaysia 2015</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Volunteer Malaysia (VM) is an event that signifies the extraordinary effort made by Volunteer Malaysia to engage great participation in volunteerism. Held in the month of September every year, VM offers a platform and opportunity for youth to participate in a variety of the platform’s signature volunteer activities like Green Team, Public Art Movement, beach and underwater clean up and War on Dengue.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#vm2015" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/asean4u/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#asean4u">
                                <h3 class="white-text">ASEAN4U</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">ASEAN4U was Volunteer Malaysia’s (VM) first volunteer network program that saw VM hosting 70 delegates from Malaysia, Indonesia, Thailand, Cambodia, Laos, Myanmar, Brunei, the Philippines, Singapore and Vietnam from 29 October to 3 November 2015.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#asean4u" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/ihyaramadan/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#ihyaramadan">
                                <h3 class="white-text">IHYA Ramadan 2015</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">IHYA Ramadan is an interfaith program, initiated by Volunteer Malaysia in conjunction with the month of Ramadan. The program hopes to share the blessings of Ramadan with individuals from different backgrounds, through volunteer activities.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#ihyaramadan" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  modal  -->
    <div class="modal fade" id="reachout2017" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">REACHING OUT TO NEW HEIGHTS</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>Volunteer Malaysia Reach Out Convention &amp; Celebration celebrated its 5th year milestone back in February when they were in&nbsp;<strong><em>Batu Pahat, Johor</em></strong>for the first round of Reach Out 2017. The program made its way&nbsp;through several other states in Malaysia until the 18th&nbsp;of March including&nbsp;<strong><em>Pahang, Kedah, Negeri Sembilan &amp; Melaka</em></strong>.</p>
                            <p>The convention, which aims to promote volunteerism to Malaysian youth through the sharing of experiences by a selected group of successful individuals featured big and inspiring names including&nbsp;<strong><em>Datuk Jake Abdullah, Tengku Dato' Sri Zafrul Aziz, Azran Osman Rani, Joe Flizzow, Raviraj Sawlani & Siti Aishah from Project TRY, Kavin Jay, Anita Yusof and AJ &lsquo;Pyro&rsquo; Lias Mansor</em></strong><em>.</em></p>
                            <p>The programme that took place in 5 different institutions -&nbsp;<strong><em>Universiti Tun Hussein Onn, Universiti Malaysia Pahang,&nbsp;Universiti Utara Malaysia,&nbsp;Politeknik Nilai,&nbsp;Politeknik Merlimau</em></strong>&nbsp;was hosted by iM4U fm&rsquo;s very own radio announcers;&nbsp;<strong><em>Jiggy, TV, Amrita, Fiza, Tyler and Ira</em>.</strong></p>
                            <p>Performances by&nbsp;<strong><em>Masdo, Pitahati, Ernie Zakri, Syamel, Oh Chentaku, Harris Baba, Awi Rafael&nbsp;</em></strong>were the highlight of the tour as the crowd got to sit back and chill after a day of knowledge and stimulation<strong><em>.</em></strong>&nbsp;</p>
                            <p>All in all, we're glad that this year's event was a success and that everyone who came had a good time and an unforgettable experience. We also hope that this event will inspire even more youth to go out there and be inspired by those who have made it before them.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=radkdwckaZ8" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=nwwoABX26hk" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=4GvQWp_zWl4" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=ePqdfYhXQAA" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=4x4MVsS9pwo" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/4.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="reachoutrun2016" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">REACH OUT RUN 2016</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                                <p>Reach Out Run: Run For A Cause, Volunteer Malaysia&rsquo;s charity run, returned for its second year in 2016. Close to 4,000 people took part in this year&rsquo;s run that aimed to raise a total of RM80,000 for four charity organizations; Juara Turtle Project, Sabah Wetlands Conservation Society, Project WHEE! and Malaysian Rare Disorders Society. Each organization is expected to receive RM20,000.</p>
                                <p>The run, which was first introduced in 2015, aims to inculcate a more balanced and healthier life among Malaysians while helping the four organizations that champion the different causes. Besides that, Volunteer Malaysia also hopes to promote unity through the run.</p>
                                <p>This year&rsquo;s run was divided into four different categories: 15km (men's open &amp; women's open), 10km (men's open &amp; women's open), 10km (college students &amp; university students) and 5km Fun Run.</p>
                                <p>The top three winners for the 15km category men and women open received cash prizes of RM2,000, RM1,500 and RM1,000 while those of the 10km categories took home cash prizes of RM1,500, RM1,000 and RM500 respectively. The winners from the 15km and 10km categories also pledged 10 per cent of their winnings to one of the four selected organizations. The winners for the college and university category walked away with RM3,000, RM2,000 and RM1,000 from DRe1m Fund to implement their initiatives for the community. There was no cash prize for the 5km Fun Run category.</p>
                                <p>2016 also saw the participation of local celebrities; Siti Saleha, Pushpa Narayan, Faizdickie and Sean Lee as Reach Out Run ambassadors. They were joined by iM4U fm announcers; Jiggy, Tyler, Fiza and TV at the event that was held at Dataran Putrajaya. Besides taking part in the run, the ambassadors also participated in &lsquo;Reach Out Run Volunteer Activities&rsquo;, a volunteer program that was held in conjunction with Reach Out Run 2016.</p>
                                <p>The ambassadors took part in volunteer activities for the four participating organizations. Fiza and Pushpa Narayan took part in the Juara Turtle Project that was held in Pulau Tioman where volunteers had the opportunity to participate in night surveys (beach & boat patrol), monitor nesting sea turtles, and help with local environmental education, PAM & beach clean up. Sabah Wetlands Conservation Society on the other hand saw volunteers working together with Siti Saleha and Tyler to carry out mangrove plant seedings, tree plantings and clean up in an effort to preserve and rehabilitate Malaysia&rsquo;s natural wetlands in Sabah. Project WHEE! brought Faizdickie and Jiggy as well as the volunteers to Bario, Sarawak to help convert an old hostel into a resource centre for the local students.  The team got to work with the local Kelabit community there while experiencing the wonderful culture of the Bario Highlands. Sean Lee and TV had the opportunity to explore the Family City Farm in Seri Kembangan with the children from the Malaysian Rare Disorder Society (MRDS).</p>
                                <p>To sum it all up, Reach Out Run 2016 was indeed a spectacular event. Thank you all who participated and we&rsquo;ll see you again soon!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=S8WHRPvS0zc" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/4.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vma2016" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vma2016/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">VOLUNTEER MALAYSIA AWARDS 2016</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                                <p>Volunteer Malaysia celebrates volunteerism with its first ever Volunteer Malaysia Awards, a Prime Minister&rsquo;s Award ceremony held at Sunway Resort Hotel and Spa. This prestigious award ceremony sees various people from different backgrounds coming together to celebrate the outstanding achievements of those who have volunteered and contributed positively towards the betterment of our community.</p>
                                <p>There were 13 awards presented on the night, which include for Best Volunteer Initiative (Private Sector, Public Sector, Media Organisation, NGO/NPO, School, DRe1m Fund, Personality and People&rsquo;s Choice), Lifetime Achievement Award, Superelawan of the Year, Best Social Media Campaign for a Volunteer Initiative, Special Jury Award and iM4U fm Award for Best Humanitarian Song.</p>
                                <p>Submission of entries was open from 1<sup>st</sup>&nbsp;September 2016 until 30<sup>th</sup>&nbsp;October 2016, where individuals and organizations from around the country obtained the opportunity to submit their best volunteer initiatives. The nominations and winners were selected by a panel of notable jury that consists of prominent individuals from various relevant industries.</p>
                                <p>Besides those awards, Volunteer Malaysia volunteers from all walks of life who have volunteered a certain number of hours in a year were also recognized with Volunteer Malaysia Awards for Bronze, Silver and Gold Achievements.</p>
                                <p>Acknowledged and officiated by the Prime Minister of Malaysia, the Volunteer Malaysia Awards has been conceptualized to celebrate, honour and pay tribute to individuals and groups of volunteers who have inspired and promoted volunteerism via their remarkable contributions. This award ceremony highlights the best practices executed at minimum costs with optimum positive impact that is sustainable and long term. They must be able to reach out to a wide spread of people and benefit the intended community. This is a way of recognizing and rewarding the volunteers for their undivided time and effort, and to encourage people of all ages to volunteer and make a difference in their community.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vma2016/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vma2016/3.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vm2015" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">VOLUNTEER MALAYSIA 2015</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>Volunteer Malaysia (VM) is an event that signifies the extraordinary effort made by Volunteer Malaysia to engage great participation in volunteerism. Held in the month of September every year, VM offers a platform and opportunity for youth to participate in a variety of platform’s signature volunteer activities like Green Team, Public Art Movement, beach and underwater clean up and War on Dengue.<br /><br />VM is one of Volunteer Malaysia&rsquo;s signature programs that have successfully brought together volunteers from a cross section of community of different backgrounds and interests since its inception in 2014. These are individuals who have volunteered their time and effort for a noble cause.</p>
                            <p>The significant increase in the number of volunteers and the number of hours spent on the activities show that VM has successfully garnered the support and interest of fellow Malaysians.<br /><br />Another highlight from VM 2015 was the introduction of underwater clean up, where passionate divers came together to help clear up marine waste.<br /><br />VM 2015 also witnessed another milestone with the participation of volunteers in Korea, France and Australia, through Volunteer Malaysia international Outreach Centres. The activities were carried out simultaneously in those countries on 12 September 2015.<br /><br />One of the winning factors for VM is the support and participation from various government ministries that helped to facilitate the arrangement and access in certain areas. These included the Ministry of Tourism and Culture, Ministry of Higher Education, Ministry of Health, Ministry of Urban Wellbeing, Housing and Local Authority and NBOS.<br /><br />Support and participation also came from corporations like Astro that had signed up to volunteer at VM as part of their CSR initiative. VM is definitely the program that helps to bring change to the community. Be part of VM to inspire others!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=BBr9xKvfoMQ" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/4.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="asean4u" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">ASEAN4U</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>ASEAN4U was Volunteer Malaysia’s (VM) first volunteer network program that saw VM hosting 70 delegates from Malaysia, Indonesia, Thailand, Cambodia, Laos, Myanmar, Brunei, the Philippines, Singapore and Vietnam in 2015.<br /><br />This program was initiated by Volunteer Malaysia to create a platform that fostered and strengthened the spirit of volunteerism among the ASEAN communities. The participants had the opportunity to share their experience and impart their knowledge to their peers. Besides that, they also gained hands-on experience of carrying out volunteer activities in Malaysia.<br /><br />It started off with a leadership training conducted by Michael Teoh from Thriving Talents and a talk by Jonathan Yabut, the winner of &lsquo;The Apprentice Asia&rsquo; on 29th and 30th October. On the third day, they took part in &lsquo;Eco Basket&rsquo;, an activity where they weaved baskets to raise funds for single parents. In the evening, they cooked and distributed food for 500 homeless in 10 locations around Kuala Lumpur.<br /><br />The following day, the delegates worked hand in hand with Volunteer Malaysia volunteers in &lsquo;Ecocentric Transitions Workshop&rsquo;, where they helped to restore nature by planting trees in Taman Tun Dr. Ismail. They visited an orphanage on 2nd November where they repainted and beautified the premise.</p>
                            <p>The program ended on a high note when the delegates had the chance to meet a Minister in the Prime Minister’s Department, YB Senator Datuk Seri Abdul Wahid Omar in Putrajaya on 3rd November. They also received their certificate of participation from the Senator who is also Volunteer Malaysia&rsquo;s mentor.<br /><br />ASEAN4U is truly an inspiring program that has torn down all barriers in the aim to inculcate volunteerism.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=Ag3DMYMIoRo" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/4.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/5.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ihyaramadan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">IHYA Ramadan 2015</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>iHYA Ramadan is an interfaith program, initiated by Volunteer Malaysia in conjunction with the month of Ramadan. The program hopes to share the blessings of Ramadan with individuals from different backgrounds, through volunteer activities.<br /><br />Six meaningful activities were carried out under iHYA Ramadan 2015 :<br /><br />1. Perkampungan Orang Asli &ndash; A get-together for converts from the &lsquo;Orang Asli&rsquo; community to welcome the Ramadan.<br /><br />2. Beauty of Ramadan with Ustaz Nouman Ali Khan &ndash; A religious discourse conducted by Nouman Ali Khan, a well-known Islamic preacher and the founder of The Bayyinah Institute for Arabic and Qur&rsquo;anic Studies from USA.<br /><br />3. iHYA Ramadan &ndash; Fabrik Kasih &ndash; A collaboration with GIATMARA Wilayah Persekutuan Kuala Lumpur to prepare baju Raya for the underprivileged children.<br /><br />4. iQRA&rsquo; &ndash; A nationwide initiative where 100 volunteers from Volunteer Malaysia Outreach Centres in each state across the country, gathered and recited the Al-Quran together.<br /><br />5. Ramadan Rangers &ndash; Volunteers helped to distribute iftar packs to those were on-duty in the hours of berbuka at 70 locations.<br /><br />6. We look forward to seeing you at this year&rsquo;s iHYA Ramadan!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=CjYwxOxXT0s" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/4.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/5.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- end modal -->


    <!--  collaborate  -->
<div id="collaborate" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="blue-text">COLLABORATE WITH US</h1>
                <h3 class="red-text">Corporate Clients & NGOs</h3>
            </div>
        </div>
        <div class="row align-items-center mb-0">
            <div class="col-sm-12 col-md-8">
                <p>If your organisation is passionate about programs related to the environment, community well being, sports, arts & culture and even skills development, why not collaborate with us? Let’s work together to create more awareness and achieve
                    maximum impact!</p>
                <p>Let’s co-create unique, mutually beneficial partnerships. We can showcase your company as a leading supporter of volunteerism, engage your staff and clients and increase the opportunities which we can offer to the Malaysian communities.</p>
            </div>
            <div class="col-sm-4 col-md-3 d-sm-none d-md-block">
                <img src="img/happening/collaborate_icon.png" class="img-fluid" />
            </div>
        </div>
        <div class="row pb-3">
            <div class="col-sm-12">
                <a href="mailto:BMG@volunteermalaysia.my?subject=Collaborate With Us" class="btn btn-red">Collaborate Now</a>
            </div>
        </div>
    </div>
</div>

<!--  partner  -->
<div id="partners" class="container-fluid p-0 pt-5 pb-5" style="background:#1488c8;">
    <div class="container pt-4 pb-4">
    <div class="row">
        <div class="col-sm-12 pb-4">
            <h1 class="white-text">OUR PARTNERS</h1>
        </div>
    </div>
    <div class="row p-0 mb-4 no-gutters">
        <div class="col-sm-2"><img src="img/happening/logos/1.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/2.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/3.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/4.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/5.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/6.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/7.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/8.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/9.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/10.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/11.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/12.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/13.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/14.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/15.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/16.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/17.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/18.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/19.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/20.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/21.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/22.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/23.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/24.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/25.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/26.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/27.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/28.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/29.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/30.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/31.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/32.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/33.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/34.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/35.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/36.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/37.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/38.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/39.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/40.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/41.png" class="img-fluid hover-zoom" /></div>
        <div class="col-sm-2"><img src="img/happening/logos/42.png" class="img-fluid hover-zoom" /></div>
    </div>
    </div>
</div>

    <!--  Project Highlight  -->
    <div id="projecthighlight" class="container-fluid bluesea-bg pt-5 pb-5">
        <div class="container pt-4 pb-4">
            <div class="row">
                <div class="col-sm-12 pb-4">
                    <h1 class="blue-text">PROJECTS HIGHLIGHTS</h1>
                </div>
            </div>
            <div class="row">
                <div class="slick-collab">
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/aeon/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 pb-3 dotdotdot-title p-3 text-center">AEON CO. (M) BHD</h5>
                        <br/>
                        <br/>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#aeon" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/unicef/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">UNICEF</h5>
                        <br/>
                        <br/>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#unicef" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/pg/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">P&G</h5>
                        <br/>
                        <br/>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#pg" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/sazzy/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">Sazzy Falak (Actress)</h5>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#sazzy" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/altimet/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">ALTIMET (Singer)</h5>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#altimet" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/rohingya/1.jpeg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">Mission for the Rohingyas</h5>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#rohingya" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        <div class="col-sm-12 img" style="background-image: url(img/explore/reality/1.png); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                        <h5 class="pt-3 dotdotdot-title p-3 text-center">Reality Chat</h5>
                        <br/>
                        <br/>
                        <div class="col-md-12 d-flex flex-wrap justify-content-center">
                            <a href="" data-toggle="modal" data-target="#reality" class="btn btn-red mb-3">READ STORY</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--  modal  -->
    <div class="modal fade" id="aeon" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">AEON CO. (M) BHD</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <h4 class="blue-text mb-3">PROGRAM NAME</h4>
                            <p>Stuff The Bus, Hometown Forest Program (tree-planting)</p>
                            <p><strong><u>Stuff The Bus</u></strong></p>
                            <p>As part of Volunteer Malaysia&rsquo;s post-flood relief efforts, Volunteer Malaysia and AEON organized a program called &lsquo;Stuff The Bus&rsquo;. The program featured a bus that was parked in front of selected AEON shopping malls and Volunteer Malaysia volunteers &lsquo;stuffed&rsquo; it with items such as detergents, mops, brooms and multi-purpose cleaners that were donated by the public. The items were then brought to the Volunteer Malaysia collection center where they were packed and distributed to the flood victims.</p>
                            <p><strong><u>Hometown Forest Program (tree-planting)</u></strong></p>
                            <p>To strengthen the partnership between Volunteer Malaysia and AEON, volunteers from the Volunteer Malaysia International Outreach Center Japan were invited to participate in AEON&rsquo;s annual tree-planting program in Atsuma, Hokaiddo together with the employees of the company. A total of 200 participants were involved in the tree-planting program. Trees were planted nearby Atsuma River, which has significantly lost its groundwater recharging capabilities throughout the years. The newly planted trees would help restore the functions the woodlands originally possessed and secure water resources crucial for farming and rice cultivation.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/4.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/5.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/6.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/7.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/aeon/8.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="unicef" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="position: relative;">
                    <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                        <div class="row" style="position: absolute; right: 20px">
                            <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row no-gutters justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/unicef/1.jpg" class="img-fluid" /></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4 mt-3">
                                <h2 class="blue-text">UNICEF</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-3">PROGRAM NAME</h4>
                                <p>U-Report</p>
                                <p><strong><u>U-report</u></strong></p>
                                <p>Volunteer Malaysia partnerned with UNICEF to create awareness about the U-Report, a platform for young people to voice their ideas and opinions on issues they care about and to help create positive change in their communities. Various U-Report workshops have been held at The Rubix with more than 200 volunteers participating in the program. Throughout the workshop, participants will have to identify social issues affecting young people in Malaysia. Some of the topics identified include child marriage, quality education, poverty, migrants/refugees, the economy, mental health and other equally interesting themes.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3 mt-3">
                                <h4 class="blue-text">PHOTO GALLERY</h4>
                            </div>
                            <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/unicef/2.jpg" class="img-fluid" /></div>
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/unicef/3.jpg" class="img-fluid" /></div>
                            </div>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pg" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="position: relative;">
                    <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                        <div class="row" style="position: absolute; right: 20px">
                            <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row no-gutters justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/pg/1.jpg" class="img-fluid" /></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4 mt-3">
                                <h2 class="blue-text">P&G</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-3">PROGRAM NAME</h4>
                                <p>Water 4 Life, Volunteer Malaysia Flood Relief (Hygiene Kits)</p>
                                <p><strong><u>Water 4 Life</u></strong></p>
                                <p>&lsquo;Water 4 Life&rsquo; was a collaborative effort between Volunteer Malaysia (VM) and P&amp;G to provide safe drinking water to communities that do not have access to clean water.</p>
                                <p>The program took place at Kampung Air Jernih, Selama in Perak as well as Kampung Suasa, Beaufort in Sabah. Representatives from VM and P&amp;G ran special workshops for the villagers and school children on how to use the &lsquo;P&amp;G Purifier of Water&rsquo; packets in order to get clean drinking water. Additionally, VM volunteers worked on a water pipe project that would filter dirty water as it flows through to the village.</p>
                                <p><strong><u>Volunteer Malaysia Flood Relief (Hygiene Kits)</u></strong></p>
                                <p>As part of Volunteer Malaysia&rsquo;s Flood Relief program, P&amp;G provided basic toiletries for the hygiene kits that were distributed to the flood victims in the East Coast. The donated items included toothbrushes, toothpastes, shavers and sanitary pads.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3 mt-3">
                                <h4 class="blue-text">PHOTO GALLERY</h4>
                            </div>
                            <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/pg/2.jpg" class="img-fluid" /></div>
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/pg/3.jpg" class="img-fluid" /></div>
                            </div>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="sazzy" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="position: relative;">
                    <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                        <div class="row" style="position: absolute; right: 20px">
                            <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row no-gutters justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/sazzy/1.jpg" class="img-fluid" /></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4 mt-3">
                                <h2 class="blue-text">SAZZY FALAK (ACTRESS, TV HOST, FASHION DESIGNER)</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-3">PROGRAM NAME</h4>
                                <p>Sazzy&rsquo;s Iftar with Single Mothers, Fabrik Kasih</p>
                                <p>Sazzy Falak has been actively involved in volunteer programs throughout her years as an Volunteer Malaysia Ambassador. Her &lsquo;Iftar with Single Mothers&rsquo; program takes place every year during the holy month of Ramadan, where Sazzy and her celebrity friends break fast with underprivileged women and spend some quality time with them and their families.</p>
                                <p>Sazzy has also participated in &lsquo;Fabrik Kasih&rsquo;, an initiative that showcases Raya clothing designs prepared by students from four selected vocational colleges. The students spent a total of six weeks to sew 275 Raya outfits for four homes, under Sazzy&rsquo;s supervision. The beneficiaries were residents of Pusat Jagaan Titian OKU Nur, Rumah Jagaan Orang Tua Al Ikhlas, Persatuan Ibu Tunggal Warga Sabah &amp; Semenanjung and Rumah Anak Yatim Limpahan Kasih.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-4">
                                <h4 class="blue-text mb-3">LINKS</h4>
                                <p><a href="https://www.gempak.com/artikel/1122/sazzy-falak-bantu-golongan-kurang-berkemampuan-menerusi-fabrik-kasih" target="_blank">News</a></p>
                                <p><a href="https://www.sunwayproperty.com/enewz/enewz-detail/fabrik-kasih-im4u-fm-bersama-kumpulan-sunway-gives-back-to-community/issue/July/id/209fd9f0-572b-688c-a7be-ff000068ef51" target="_blank">News</a></p>
                                <p><a href="https://www.youtube.com/watch?v=YszOnBEn-BQ" target="_blank">Video</a></p>
                            </div>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="altimet" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="position: relative;">
                    <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                        <div class="row" style="position: absolute; right: 20px">
                            <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row no-gutters justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/altimet/1.jpg" class="img-fluid" /></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4 mt-3">
                                <h2 class="blue-text">ALTIMET (HIP HOP ARTIST, ENTREPRENEUR)</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <h4 class="blue-text mb-3">PROGRAM NAME</h4>
                                <p>Rapper With A Cause</p>
                                <p>&lsquo;Rapper With A Cause&rsquo; is Altimet&rsquo;s very own program with Volunteer Malaysia. The program looked to create an opportunity for the hearing and hearing impaired community to bridge the gap through fun and educational activities. Collaborating with the Special Education Division under the Ministry of Education, Volunteer Malaysia invited 100 hearing impaired students between the ages of 10 and 18 for an afternoon workshop facilitated by SilentSHOUT (a social enterprise that works with the deaf community), culminating with the performance by Altimet himself. The workshop comprised of activities such as a film screening, a spelling bee contest and charades.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-4">
                                <h4 class="blue-text mb-3">LINKS</h4>
                                <p><a href="https://www.rage.com.my/rapping-for-a-cause/" target="_blank">News</a></p>
                                <p><a href="https://www.youtube.com/watch?v=PMir0Fpzad0" target="_blank">Video</a></p>
                            </div>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="rohingya" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="position: relative;">
                    <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                        <div class="row" style="position: absolute; right: 20px">
                            <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row no-gutters justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/rohingya/1.jpeg" class="img-fluid" /></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4 mt-3">
                                <h2 class="blue-text">Negaraku Prihatin: Mission for the Rohingyas</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <p>Volunteer Malaysia together with Malaysia Airports, Malaysia Airlines, MABKargo and Malindo Air is working on the Mission For The Rohingyas with a donation drive at 6 different airports across Malaysia. 9th&nbsp;September 2017 marked a historic moment as we sent off the first batch of humanitarian aid consisting 12 tonnes of relief items to Bangladesh via military flight at TUDM Subang Airbase.</p>
                                <p>On 7th&nbsp;October 2017, marked another milestone as we witnessed the first commercial cargo flight took off to Shah Amanat International Airport, Chittagong with 30 tonnes of humanitarian aid consisting of food, wash items and survival kits to be distributed to the Rohingya refugees in Cox&rsquo;s Bazaar, Bangladesh.</p>     
                                <p>Peter Bellew, Chief Executive Officer of Malaysia Airlines; Abdul Luqman Azmi, Chief Executive Officer for MABKargo; Tan Sri Md Nor Md Yusof, Chairman of Malaysia Airlines; Mohd Arif Jaafar, General Manager of Malaysia Airports; Shahrunnizam Abdul Jamil, Manager of Landside Office and Tan Shyue Wern, Volunteer Malaysia Director of Brand Management Group were present to see flight MH6446 take off at 7.15 am in the morning and was estimated to reach Shah Amanat International Airport at 9 am local time.</p>                       
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3 mt-3">
                                <h4 class="blue-text">PHOTO GALLERY</h4>
                            </div>
                            <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/rohingya/2.jpeg" class="img-fluid" /></div>
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/rohingya/3.jpg" class="img-fluid" /></div>
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/rohingya/4.jpg" class="img-fluid" /></div>
                            </div>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="reality" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="position: relative;">
                    <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                        <div class="row" style="position: absolute; right: 20px">
                            <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row no-gutters justify-content-center align-items-center">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/reality/1.png" class="img-fluid" /></div>
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12 mb-4 mt-3">
                                <h2 class="blue-text">#ChitChatting with three young talents at “Reality Chat”</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <p>The inaugural Reality Chat which took place on Saturday, 29 July 2017 at Lanai Santai, RUBIX, featured various young talented speakers with the aim to inspire youth to dream big, to be innovative and creative in running businesses. With the topic of &ldquo;Start-Ups &amp; Social Enterprise&rdquo;, the chat saw speakers such as Ehon Chan, Executive Director of MaGIC; Dzameer Dzulkifli, Co-Founder &amp; Managing Director of Teach for Malaysia; and Raeesa Sya, CEO of Orkid Cosmetics. The discussion jumped straight into topics such as risk taking in businesses and stepping out of their comfort zone to succeed. The speakers also gave tips and shared their insights on how they made their business relevant while leaving an impact towards the community.</p>
                                <p>Dzameer Dzulkifli spoke about his concerns for the issues surrounding education in Malaysia and highlighted the efforts placed by Teach For Malaysia to empower the nation to help the underprivileged school children. He expressed that it is because of this deep concern for education, which led him to establish Teach for Malaysia. Dzameer believes that leadership is beyond the classroom and encouraging youth volunteerism within the education industry will drive the nation to build better future generations.</p>
                                <p>Representing the Malaysian Global Innovation &amp; Creativity Centre or better known as MaGiC, Ehon shared some tips to the young aspiring entrepreneurs on how to start and sustain a business in the long run. He encouraged the young entrepreneurs to be confident, believe in their ideas and truly comprehend what their business have to offer as it is an important factor required for their business to succeed.</p>
                                <p>The founder of Orkid Cosmetics, Raeesa also shared her journey as a young entrepreneur. Also reinforcing on the tips shared by Ehon, she added that understanding current trends and consumer behaviours are as important as being relevant in the market. However, in her opinion, the purpose of a business should not merely be based on making big money or following current trends blindly. Businesses should be able to sustain itselves for the long term and take into consideration of the impacts the businesses have towards the end users and those affected by it. She also believed that customers&rsquo; loyalty towards her products is of utmost importance.</p>
                                <p>Throughout the session, the speakers echoed a similar tune on how a business can grow despite the adversities and the importance of considering the impact the business may bring towards the communities. However, the most important points are: believe in what you do, know your business thoroughly and execute with trust.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3 mt-3">
                                <h4 class="blue-text">PHOTO GALLERY</h4>
                            </div>
                            <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/reality/2.png" class="img-fluid" /></div>
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/reality/3.png" class="img-fluid" /></div>
                                <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/explore/reality/4.png" class="img-fluid" /></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    <!-- end modal -->

{{-- Invoke from finish registration --}}
@if(Request::input('popup') == 'create')
<div id="createModal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="green-bg d-flex justify-content-center p-2" style="position:relative;">
                <i class="far fa-thumbs-up white-text m-4" style="font-size:5em"></i>
                <a href="" data-dismiss="modal" class="close"  aria-label="Close" style="position:absolute; top:10px; right:15px">
                    <span aria-hidden="true">&times;</span>
                </a>
            </div>
            <div class="modal-body text-center">
                <h3 class="green-text pt-3">Thank You!</h3>
                <p>You just completed your registration. Now you can start completing your profile first.</p>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <a href="/profile" class="btn btn-green" style="width:200px;">GO TO PROFILE</a>
            </div>
        </div>
    </div>
</div>
@endif

@endsection 
@section('js') 
@parent 
<script type="text/javascript">
    $(window).on('load',function(){
        $('#createModal').modal('show');
    });
</script>

<script>
$(document).ready(function () {
    $(".descfix").dotdotdot({

        ellipsis: "\u2026 ",
        height: 60,
        truncate: "word"

    });
});

</script>
@endsection