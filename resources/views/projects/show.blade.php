@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container p-0 ">
    @if($project->getMedia('banner')->first())
    <div class="row align-items-center justify-content-center" style="background-image: url({{ $project->getMedia('banner')->first()->getUrl()}}); background-size: cover; height: 320px; background-repeat: no-repeat; background-position: center;">
    </div>
    @else
    <div class="row align-items-center justify-content-center" style="background-image: url(http://via.placeholder.com/900x300); background-size: cover; height: 320px; background-repeat: no-repeat; background-position: center;">
    @endif
</div>
<!--  end  -->

<!-- ni utk thumbnail -->
{{-- {{ $project->getMedia('thumbnail')->first()->getUrl() }} --}}

<div class="container-fluid">
    <div class="container white-bg p-3">
        <div class="row justify-content-md-between">
            <div class="col-sm-6 col-lg-6 d-flex align-items-center">
                <p class="m-2">Share now</p>
                <a href="http://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}" target="_blank">
                    <i class="fab fa-facebook-f m-2"></i>
                </a>
                <a href="http://twitter.com/share?url={{ url()->current() }}" target="_blank">
                    <i class="fab fa-twitter m-2"></i>
                </a>
                <a href="https://plus.google.com/share?url={{ url()->current() }}" target="_blank">
                    <i class="fab fa-google-plus-g m-2"></i>
                </a>
            </div>
            <div class="col-sm-6 col-md-4 d-flex align-items-center justify-content-md-end">
                <i class="far fa-flag m-2"></i>
                <a href="">
                    <p class="m-2">Report Abuse</p>
                </a>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <h2 class="mb-4">{{ $project->title }}</h2>
                <p>{{ $project->tagline }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-3 p-0 m-0">
                <div class="border m-2 p-4 d-flex flex-column justify-content-center align-items-center" style="min-height: 200px">
                    <div class="row">
                        <i class="fas fa-calendar-alt mb-3 fa-2x"></i>
                    </div>
                    <div class="row text-center">
                        <p style="font-size: 0.8em">{{ Carbon\Carbon::parse($project->started_at)->format('F j, Y') }} - {{ Carbon\Carbon::parse($project->ended_at)->format('F
                            j, Y') }}</p>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-md-3 p-0 m-0">
                <div class="border m-2 p-4 d-flex flex-column justify-content-center align-items-center" style="min-height: 200px">
                    <div class="row">
                        <i class="fas fa-clock mb-3 fa-2x"></i>
                    </div>
                    <div class="row text-center ">
                        <p style="font-size: 0.8em">{{ date("g:i a", strtotime($project->time_started_at)) }} - {{ date("g:i a", strtotime($project->time_ended_at)) }}</p>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-md-3 p-0 m-0">
                <div class="border m-2 p-4 d-flex flex-column justify-content-center align-items-center" style="min-height: 200px">
                    <div class="row">
                        <i class="fas fa-map-marker-alt mb-3 fa-2x"></i>
                    </div>
                    <div class="row text-center">
                        <p style="font-size: 0.8em">
                            {{ $project->address_1 }}, {{ $project->address_2 }}<br/>
                            {{ $project->city }}, {{ $project->state }}<br/>
                        </p>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 col-md-3 p-0 m-0">
                <div class="border m-2 p-4 d-flex flex-column justify-content-center align-items-center" style="min-height: 200px">
                    <div class="row">
                        <i class="fas fa-users mb-3 fa-2x"></i>
                    </div>
                    <div class="row text-center">
                        <p style="font-size: 0.8em">
                            <span class="green-text bold">{{ $project->volunteers->count() }} </span> /{{ $project->number_volunteer }}
                            <br/> Volunteer Registered</p>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 mt-3 mb-3">
                @guest
                <a href="{{ route('login') }}" class="btn btn-green btn-block">
                        <i class="far fa-arrow-alt-circle-right"></i> SIGN IN
                    </a>
                @else
                    @if($project->status == "published")
                        @if(empty($joined))
                            @if($project->volunteers->count() == $project->number_volunteer)

                                @if(empty($subscribed))
                                    <a data-toggle="modal" data-target="#fullModal" class="btn btn-red btn-block">
                                        <i class="far fa-arrow-alt-circle-right"></i> FULL CAPACITY
                                    </a> 
                                @else
                                    <a href="{{ route('projects.unsubscribe', $project->id) }}" class="btn btn-red btn-block">
                                        <i class="far fa-arrow-alt-circle-right"></i> CANCEL SUBSCRIBED
                                    </a>
                                @endif

                            @else
                                <a href="" data-toggle="modal" data-target="#thanksModal" data-backdrop="static" data-keyboard="false" class="btn btn-green btn-block">
                                    <i class="far fa-arrow-alt-circle-right"></i> REGISTER NOW
                                </a>
                            @endif
                        @else
                        <a href="" data-toggle="modal" data-target="#withdrawModal" class="btn btn-red btn-block">
                            <i class="far fa-arrow-alt-circle-right"></i> LEAVE
                        </a>
                        @endif
                    @endif
                @endguest

                @can('edit_projects')
                    <a href="{{ route('projects.edit', $project->id) }}" class="btn btn-yellow btn-block">
                        <i class="far fa-arrow-alt-circle-right"></i> EDIT PROJECT
                    </a>
                @else
                    @auth
                        @if(Auth::user()->id == $project->user_id)
                            <a href="{{ route('projects.edit', $project->id) }}" class="btn btn-yellow btn-block">
                                <i class="far fa-arrow-alt-circle-right"></i> EDIT PROJECT
                            </a>
                        @endif
                    @endauth
                @endcan

            </div>

            {{-- Thank u modal after register/join project --}}
            <div id="thanksModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="green-bg d-flex justify-content-center p-2" style="position:relative;">
                            <i class="fas fa-check-circle white-text m-4" style="font-size:5em"></i>
                            <a href="{{ route('projects.join', $project->id) }}" class="close"  aria-label="Close" style="position:absolute; top:10px; right:15px">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body text-center">
                            <h3 class="green-text pt-3">Thanks! You're A Superhero!</h3>
                            <p>We will constantly keep you updated about the project details. Stay tuned.</p>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <a href="{{ route('projects.join', $project->id) }}" class="btn btn-green" style="width:200px;">OK</a>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Full capacity modal --}}
            <div id="fullModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="blue-bg d-flex justify-content-center p-2" style="position:relative;">
                            <i class="far fa-meh white-text m-4" style="font-size:5em"></i>
                            <a href="" class="close" data-dismiss="modal"  aria-label="Close" style="position:absolute; top:10px; right:15px">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body text-center">
                            <h3 class="blue-text pt-3">Full Capacity!</h3>
                            <p>Sorry! It seems like the capacity of registered volunteer reach its limit. If you are still interested with this project, please subscribe to get notifications via email. We will notify you when empty seat available so you can hop in.</p>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <a href="" class="btn btn-blue-o" style="width:200px;" data-dismiss="modal">CANCEL</a>
                            <a href="{{ route('projects.subscribe', $project->id) }}" class="btn btn-blue" style="width:200px;"><i class="far fa-envelope"></i> SUBSCRIBE</a>
                        </div>
                    </div>
                </div>
            </div>

             {{-- Withdraw from project modal --}}
            <div id="withdrawModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="red-bg d-flex justify-content-center p-2" style="position:relative;">
                            <i class="far fa-question-circle white-text m-4" style="font-size:5em"></i>
                            <a href="" class="close" data-dismiss="modal"  aria-label="Close" style="position:absolute; top:10px; right:15px">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <div class="modal-body text-center">
                            <h3 class="red-text pt-3">Withdraw From Project?</h3>
                            <p>Are you sure you want to withdraw from this project?</p>
                        </div>
                        <div class="modal-footer d-flex justify-content-center">
                            <a href="" class="btn btn-red-o" style="width:200px;" data-dismiss="modal">CANCEL</a>
                            <a href="{{ route('projects.leave', $project->id) }}" class="btn btn-red" style="width:200px;">YES</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt-4">
    <div class="container p-0">
        <div class="row">
            <div class="col-sm-12 col-md-6 pl-0 pr-sm-0 pb-sm-4 pr-md-0 pr-lg-4" style="min-height: 150px">
                <div class="col-sm-12 white-bg p-5 border border-gray" style="min-height: 250px">
                    <h3>What can you do to help?</h3>
                    <ul class="mt-4">
                        @foreach($project->assignments as $assignment)
                        <li class="mb-2">{{ $assignment->detail }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 pl-0 pr-0">
                <div class="col-sm-12 white-bg p-5 border border-gray" style="min-height: 250px">
                    <h3>What will you get?</h3>
                    <ul class="mt-4">
                        @foreach($project->rewards as $reward)
                        <li class="mb-2">{{ $reward->detail }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 pl-0 pr-0">
                <div class="col-sm-12 white-bg p-5 border border-gray" style="min-height: 150px">
                    <h3>Additional Information</h3>
                    {{ $project->information }}
                </div>
            </div>
            <div class="col-sm-12 pl-0 pr-0 mt-4">
                <div class="row white-bg border border-gray p-4 ">

                    @if($project->organisation)
                    <div class="col-sm-12 col-md-4">
                        <div class="row m-3">
                            <div class="col-sm-12 col-md-2">
                                <i class="fas fa-building fa-2x"></i>
                            </div>
                            <div class="col-sm-10">
                                <h5 class="m-0 mb-3 mt-md-0">Organised By</h5>
                                <p class="m-0">{{ $project->organisation->name }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="row m-3">
                            <div class="col-sm-12 col-md-2">
                                <i class="fas fa-id-card fa-2x"></i>
                            </div>
                            <div class="col-sm-10">
                                <h5 class="m-0 mb-3 mt-md-0">Contact Info</h5>
                                <p class="m-0">Phone: {{ $project->organisation->incharge_phone }}</p>
                                <p class="m-0">Email: {{ $project->organisation->incharge_email }}</p>
                            </div>
                        </div>
                    </div>
                    @endif

                    <div class="col-sm-12 col-md-4">
                        <div class="row m-3">
                            <div class="col-sm-12 col-md-2">
                                <i class="fas fa-id-badge fa-2x"></i>
                            </div>
                            <div class="col-sm-10">
                                <h5 class="m-0 mb-3 mt-md-0">Contact Person</h5>
                                <p class="m-0">Name: {{ $project->user->name }}</p>
                                <p class="m-0">Phone: {{ $project->user->phone_number }}</p>
                                <p class="m-0">Email: {{ $project->user->email }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection 

