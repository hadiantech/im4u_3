
<a class="btn btn-xs btn-success" href="{{route($url.'.show', $model->id)}}">Show</a>

@if($joined == false)
<a class="btn btn-xs btn-warning" href="{{route($url.'.edit', $model->id)}}">Edit</a>
<a class="btn btn-xs btn-danger" data-method="delete" data-confirm="confirm" href="{{ route($url.'.destroy', $model->id) }}">Delete</a>
@endif