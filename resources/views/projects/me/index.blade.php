@extends('layouts.admin.master')

@section('title', 'My Project')

@section('content')

<br />
<table id="users-table" class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Title</th>
        <th>Status</th>
        <th>Created At</th>
        <th>Action</th>
    </tr>
    </thead>
</table>

@endsection

@section('js')
@parent

<script>
    $(function () {
        var oTable = $('#users-table').DataTable({
            serverSide: true,
            processing: true,
            dom: 'l<"#add">frtip',
            ajax: {
                url: '{!! route('datatables.projects.me') !!}',
                data: function(d){
                    d.status = $("#status option:selected").val()
                }
            },
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'title'},
                {data: 'status'},
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });

        $("div.filterddl").html('');

        $('<label/>').text('Filter By ').appendTo('#add');

        $select = $('<select/>').appendTo('#add');
        $select.attr('id', 'status');
        $select.attr('name', 'status');
        $select.attr('class', 'datatables-select');
        $('<option/>').val('').text('All').appendTo($select);
        jQuery.each(statusesjs, function(index, item) {
            $('<option/>').val(item.value).text(item.name).appendTo($select);
        });
        $('#status').change( function() { oTable.draw(); } );

        if(typeof defaultStatus !== 'undefined'){
            $('option[value=' + defaultStatus + ']')
            .attr('selected',true);
            oTable.draw();
        }

    });
</script>

@endsection