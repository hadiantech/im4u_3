@extends('layouts.front.master') 
@section('content')



{!! Form::open(['url' => route('projects.store'), 'files' => 'true']) !!}

<div class="container mt-5" style="min-height:80vh !important;">
        @include('shared._error')
    <div class="row">
        <div class="col-md-12">
            <h2 class="blue-text">Create New Project</h2>
            <p class="red-text">*All information will not be saved until you have completed the last step</p>
        </div>
    </div>
    <div class="row no-gutters mt-2">
        <div class="card mb-3 col-sm-12">
            <!-- select project type-->
            <div class="card-header bold">
                Select Project Type
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <label class="form-label">What is the type of your project?</label>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="radio-ind" value="ind" name="org_selector" class="custom-control-input">
                        <label class="custom-control-label" for="radio-ind">Individual</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="radio-org" value="org" name="org_selector" class="custom-control-input">
                        <label class="custom-control-label" for="radio-org">Organisation</label>
                    </div>
                </div>
            </div>
        </div>
        <!--organisation-->
        <div class="card col-sm-12 my-3 mt-2 p-0 hide" id="org-form">
            <div class="card-header bold">
                Organisation Detail
            </div>
            <div class="card-body">
                <!--organisation selector-->
                <div class="col-md-12">
                    <div class="form-group mb-2">
                        <label>Choose an organisation. If your organisation is not listed , you may add a new one.</label>
                    </div>
                    <div class="form-group">
                        <a href="javascript:;" id="addnew-org" class="btn btn-primary mr-2">Add New Organisation</a>
                        <a id="cancel-add-new" href="javascript:;" class="btn btn-warning mr-2 hide">Cancel</a>
                    </div>
                </div>
                
                <!--select old organisation-->
                <div class="col-md-12" id="old-org">
                    <div class="form-group">
                        <label>Select your organisation</label>
                        {{ Form::select('organisation_id', $organisations, null, ['placeholder' => 'Select','class' => 'custom-select', 'id' => 'organisation_id'])}}
                    </div>
                </div>
                <!--add new org form-->

                <div class="col-md-12 my-3 hide" id="new-org">
                    <div class="row">
                        <div class="col-md-12 border rounded p-4">
                            <div class="form-group">
                                <label class="form-label">Organisation Details</label>
                                <br/> 
                                {{ Form::label('name', 'Organisation Name') }} 
                                {{ Form::text('name', null, array('class'=> 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('description', 'Description') }} 
                                {{ Form::textarea('description', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('website', 'Organisation Website (optional) ') }} 
                                {{ Form::url('website', null, array('class' => 'form-control')) }}
                            </div>
                            <div class="row">
                                <div class="col-md-6 px-0">
                                    <div class="form-group">
                                        <label>
                                            <h5>
                                                <b>Contact Person Details</b>
                                            </h5>
                                        </label>
                                        <br/> {{ Form::label('incharge_name', 'Contact Person Name') }} {{ Form::text('incharge_name',
                                        null, array('class' => 'form-control')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('incharge_phone', 'Contact Person Number') }} {{ Form::text('incharge_phone', null, array('class' => 'form-control')) }}
                                    </div>
                                    <div class="form-group">
                                        {{ Form::label('incharge_email', 'Email') }} {{ Form::email('incharge_email', null, array('class' => 'form-control')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-proj-detail">NEXT</a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 my-3 hide" id="proj-detail-form">
            <div class="card-header bold">
                Project Detail
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="form-group">
                        {{ Form::label('title', 'Insert Your Project Title',array('class'=>'form-label')) }}
                        <p class="sub2">
                            <i>Be creative! Awesome Title = More Attention!</i>
                        </p>
                        {{ Form::text('title', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('tagline', 'Insert your Project Tagline!',array('class'=>'form-label')) }}
                        <p class="sub2">
                            <i>In 160 characters, tell your audience what your project is about</i>
                        </p>
                        {{ Form::textarea('tagline', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('information', 'Additional Information',array('class'=>'form-label')) }}
                        <p class="sub2">
                            <i>Please fill any additional information that your volunteers need to know.</i>
                        </p>
                        {{ Form::textarea('information', null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        <label class="form-label">When is your project?</label>
                        <div class="row">
                            <div class="col-md-2 px-0 mr-2">
                                {{ Form::label('started_at', 'Start Date') }} {{ Form::date('started_at', \Carbon\Carbon::now() ,array('class'
                                => 'form-control', 'required')) }}
                            </div>
                            <div class="col-md-2 px-0 mr-2">
                                {{ Form::label('ended_at', 'End Date') }} {{ Form::date('ended_at', \Carbon\Carbon::now() ,array('class' => 'form-control',
                                'required')) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 px-0">
                                {{ Form::label('time_started_at', 'Start Time') }}
                            </div>
                            <div class="col-md-1 px-0 mr-2">
                                {{ Form::text('time_started_at', null, array('class' => 'form-control clockpicker', 'required')) }}
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12 px-0">
                                {{ Form::label('time_ended_at', 'End Time') }}
                            </div>
                            <div class="col-md-1 px-0 mr-2">
                                {{ Form::text('time_ended_at', null, array('class' => 'form-control clockpicker', 'required')) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        {{ Form::label('hours', 'How many hours do you need your volunteers for?',array('class'=>'form-label')) }} {!! Form::number('hours',
                        1.00, array('min' => '1','class' => 'form-control', 'required')) !!}
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-proj-location">NEXT</a>
                </div>
            </div>
        </div>

        <div class="card col-sm-12 my-3 hide px-0" id="proj-location">
            <div class="card-header bold">
                Project Location
            </div>
            <div class="card-body">
                <div class="row col-md-6 px-0">
                    <div class="col-md-12">
                        <label class="form-label">Enter your project location address</label>
                    </div>
                    <div class="col-md-12">
                        {{ Form::label('address_1', 'Address 1') }} 
                        {{ Form::text('address_1', null, array('class' => 'form-control mb-4', 'required')) }}
                        {{ Form::label('address_2', 'Address 2 (optional)') }} 
                        {{ Form::text('address_2', null, array('class' => 'form-control mb-4')) }}
                    </div>
                    <div class="col-md-6 mb-2">
                        {{ Form::label('city', 'City') }} 
                        {{ Form::text('city', null, array('class' => 'form-control', 'required')) }}
                    </div>
                    <div class="col-md-6 mb-2">
                            {{ Form::label('postcode', 'Postcode') }} 
                            {{ Form::text('postcode', null, array('class' => 'form-control mb-4', 'required')) }}
                        </div>
                    <div class="col-md-12 mb-2">
                        {{ Form::label('state', 'State') }} 
                        <select id="state" name="state" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                            @include('shared._state_dropdown')
                        </select>
                    </div>
                    {{-- <div class="col-md-6 mb-2">
                        {{ Form::label('country', 'Country') }} 
                        {{ Form::text('country', null, array('class' => 'form-control', 'required')) }}
                    </div> --}}
                    
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-vol-details">NEXT</a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 my-3 px-0 hide" id="vol-details">
            <div class="card-header bold">
                Volunteer Details
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="col-md-12">
                        {{ Form::label('number_volunteer', 'Number of volunteer needed', array('class'=>'form-label')) }}
                         {!! Form::number('number_volunteer', 1.00,
                        array('min' => '1','class' => 'form-control', 'required')) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="form-label ml-3">Pick the category that reflect your project the most</label>
                    <div class="col-md-12 d-flex">
                        @foreach($categories as $category)
                        <div class="custom-control custom-checkbox mr-3">
                            <input type="checkbox" class="custom-control-input" name="category[]" value="{{ $category->id }}" id="cat_{{ $loop->iteration }}">
                            <label class="custom-control-label" for="cat_{{ $loop->iteration }}">{{ $category->name }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label">What are your volunteers going to do?</label>
                            <p class="sub2">
                                <i>Let your volunteers know what they will be assigned to. Keep it short and simple</i>
                            </p>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">1</div>
                                </div>
                                {{ Form::text('assignments[]', null, array('class' => 'form-control', 'id' => 'inlineFormInputGroup')) }}
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">2</div>
                                </div>
                                {{ Form::text('assignments[]', null, array('class' => 'form-control', 'id' => 'inlineFormInputGroup')) }}
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">3</div>
                                </div>
                                {{ Form::text('assignments[]', null, array('class' => 'form-control', 'id' => 'inlineFormInputGroup')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="form-label">What will you provide for your volunteers?</label>
                            <p class="sub2">
                                <i>List up 3 items that you will be giving your volunteer in preparation for the event</i>
                            </p>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">1</div>
                                </div>
                                {{ Form::text('rewards[]', null, array('class' => 'form-control', 'id' => 'inlineFormInputGroup')) }}
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">2</div>
                                </div>
                                {{ Form::text('rewards[]', null, array('class' => 'form-control', 'id' => 'inlineFormInputGroup')) }}
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">3</div>
                                </div>
                                {{ Form::text('rewards[]', null, array('class' => 'form-control', 'id' => 'inlineFormInputGroup')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row justify-content-end">
                    <a href="javascript:;" class="btn btn-green pull-right" id="next-proj-photo">NEXT</a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 my-3 px-0 hide" id="proj-photo">
            <div class="card-header bold">
                Project Photo
            </div>
            <div class="card-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8 mb-2 d-flex flex-column justify-content-center align-items-center">
                            <label class="form-label">Project Photo</label>
                            <label class="bg-image-label btn btn-green" style="width:150px" for="i1">
                                <i class="bb bb-picture bb-3x"></i>
                                <div class="text">Browse</div>
                            </label>
                        </div>
                        <div class="col-md-4 d-flex flex-column justify-content-center align-items-center">
                            <label class="form-label">Project thumbnail</label>
                            <label class="bg-image-label btn btn-green" style="width:150px" for="i2">
                                <i class="bb bb-picture bb-3x"></i>
                                <div class="text">Browse</div>
                            </label>
                        </div>
                        
                        <div class="col-md-8">
                            <input type="file" name="banner" id="i1" onchange="readURL(event)" style="display:none" accept="image/*" />
                            <div class="bg-image project-banner-preview" id="bg-image-i1">
                                <div class="white-bg black-text p-2 border rounded alpha-1"><b>Banner Preview</b></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <input type="file" name="thumbnail" id="i2" onchange="readURL(event)" style="display:none" accept="image/*" />
                            <div class="bg-image project-thumb-preview" id="bg-image-i2">
                                <div class="white-bg black-text p-2 border rounded alpha-1"><b>Thumbnail Preview</b></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted px-4">
                <div class="row">
                    <button type="submit" class="btn btn-blue btn-block">SUBMIT</button>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection

@section('js') 
@parent
<script src="{{ asset('js/org-selector.js') }}"></script> 

<script>
    $('.clockpicker').clockpicker({
        donetext: 'Done', 
    });
</script>
@endsection
