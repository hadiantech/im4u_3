<!-- sidebar -->
<div class="d-flex">
    <nav class="sidebar" style="background: #3d464d">
        <ul class="list-unstyled pl-1 pt-2">
            <li><a href="{{route('projects.me')}}"><i class="fa fa-fw fa-link"></i> My Project</a></li>
            <li><a href="{{ route('projects.joined') }}"><i class="fa fa-fw fa-link"></i> My Joined Project</a></li>
            
            <!--
            @can('view_dashboard')
            <li><a href="{{route('dashboard')}}"><i class="fa fa-fw fa-link"></i> Dashboard</a></li>
            @endcan
            -->

            @can('view_users')
            <li style="position: relative;">
                <a href="#submenu1" data-toggle="collapse"><i class="fa fa-fw fa-users"></i> User Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu1" class="list-unstyled collapse">
                    <li><a href="{{route('users.index')}}">All User</a></li>
                    @can('add_users')
                    <li><a href="{{route('users.create')}}">Create New User</a></li>
                    @endcan
                </ul>
            </li>
            @endcan

            @can('view_projects')
            <li style="position: relative;">
                <a href="#submenu2" data-toggle="collapse"><i class="fa fa-fw fa-clipboard-check"></i> Project Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu2" class="list-unstyled collapse">
                    <li><a href="{{ route('projects.index') }}">All</a></li>
                    <li><a href="{{ route('projects.index', ['status'=>'published']) }}">Published</a></li>
                    <li><a href="{{ route('projects.index', ['status'=>'pending']) }}">Pending <span class="badge badge-warning">{{ App\Project::pending()->count() }}</span></a></li>
                    <li><a href="{{ route('projects.index', ['status'=>'suspended']) }}">Suspended</a></li>
                    <li><a href="{{ route('projects.index', ['status'=>'ended']) }}">Ended</a></li>
                </ul>
            </li>
            @endcan

            @can('view_categories')
            <li style="position: relative;">
                <a href="#submenu3" data-toggle="collapse"><i class="fa fa-fw fa-tags"></i> Category Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu3" class="list-unstyled collapse">
                    <li><a href="{{route('categories.index')}}">All Category</a></li>
                    @can('add_categories')
                    <li><a href="{{route('categories.create')}}">Create New Category</a></li>
                    @endcan
                </ul>
            </li>
            @endcan

            @can('view_roles')
            <li style="position: relative;">
                <a href="#submenu4" data-toggle="collapse"><i class="fa fa-fw fa-thumbtack"></i> Role Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu4" class="list-unstyled collapse">
                    <li><a href="{{route('roles.index')}}">All Role</a></li>
                </ul>
            </li>
            @endcan

            @can('view_banners')
            <li style="position: relative;">
                <a href="#submenu5" data-toggle="collapse"><i class="fas fa-images"></i> Banner Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu5" class="list-unstyled collapse">
                    <li><a href="{{route('banners.index')}}">All Banner</a></li>
                    @can('add_banners')
                    <li><a href="{{route('banners.create')}}">Create New Banner</a></li>
                    @endcan
                </ul>
            </li>
            @endcan
            
            @can('view_journals')
            <li style="position: relative;">
                <a href="#submenu6" data-toggle="collapse"><i class="fas fa-book"></i> Journal Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu6" class="list-unstyled collapse">
                    <li><a href="{{route('journals.index')}}">All Journal</a></li>
                    @can('add_journals')
                    <li><a href="{{route('journals.create')}}">Create New Journal</a></li>
                    @endcan
                </ul>
            </li>
            @endcan

            @can('view_presses')
            <li style="position: relative;">
                <a href="#submenu7" data-toggle="collapse"><i class="fas fa-book"></i> Press Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu7" class="list-unstyled collapse">
                    <li><a href="{{route('presses.index')}}">All Press</a></li>
                    @can('add_presses')
                    <li><a href="{{route('presses.create')}}">Create New Press</a></li>
                    @endcan
                </ul>
            </li>
            @endcan

            {{-- <li><a href="#"><i class="fas fa-wrench"></i> Settings</a></li> --}}

        </ul>
    </nav>

    <!-- REMINDER: PUT CONTENT INSIDE THIS DIV "content"  -->
    <div class="content p-4" style="background: #f7f7f7">
        <!-- Volunteer Stats -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12 p-0 mb-3">  
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 p-0 mb-3">
                                <h1 class="blue-text mb-3">@yield('title')</h1>
                                <h4 style="font-family:poppins">@yield('subtitle')</h4>
                            </div>
                        </div>
                        @include('flash::message')
                        @include('shared/_error')
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- end -->
    </div>

</div>
<!-- end   -->