<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    
    <!-- Main   -->
    <link href="{!! asset('css/main.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/bsadmin.css') !!}" media="all" rel="stylesheet" type="text/css" />

    <!-- Plugins     -->
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,300' rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    
    <link rel="stylesheet" href="{{ asset('css/jquery-clockpicker.min.css') }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

    @yield('css')

</head>

<body>

    @include('cookieConsent::index')
    <!--  menu  -->
    @include('layouts.admin.navbar')
    <!-- content -->
    @include('layouts.admin.content')
    @include('shared._popup')

    @include('shared._delete_confirm')

    <script src="{{ mix('/js/app.js') }}"></script>

    <!-- Optional JavaScript -->
    
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

    <script src="{!! asset('js/jquery.dotdotdot.js') !!}"></script>
    <script src="{!! asset('js/bsadmin.js') !!}"></script>
    {{-- <script src="{!! asset('js/chartdata.js') !!}"></script> --}}
    <script src="{!! asset('js/main.js') !!}"></script>
    <script src="{!! asset('js/jquery-clockpicker.min.js') !!}"></script>
    

    @include('footer')
    @yield('js')

</body>

</html>