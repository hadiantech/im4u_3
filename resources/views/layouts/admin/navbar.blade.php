<!--  menu  -->
<nav class="navbar navbar-expand navbar-dark bg-primary" style="background-color: #3383d7 !important">
    <a class="sidebar-toggle text-light mr-3"><i class="fa fa-bars"></i></a>

    <a class="navbar-brand" href="/"><img src="{{asset('img/logo/whitevmlogo.png')}}" width="auto" height="45" alt=""></a>

    <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <div class="item d-lg-flex">
                <a class="nav-item nav-link white-text mx-lg-3" href="{{route('home')}}">Back to website</a>
                @include('layouts.basic.user_menu')
            </div>
            
        </div>    
    </div>
</nav>
<!--  end  -->