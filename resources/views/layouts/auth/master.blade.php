<!DOCTYPE html>
<html lang="en">

<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    
    <!-- Main   -->
    <link href="{!! asset('css/main.css') !!}" media="all" rel="stylesheet" type="text/css" />
    <link href="{!! asset('css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />

    <!-- Plugins     -->
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:400,700,800,300' rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

    @yield('css')

</head>

<body class="gray-bg">

    @include('cookieConsent::index')
    <!--  menu  -->
    @include('layouts.basic.navbar')
    <!-- content -->
    @include('layouts.basic.content')
    @include('shared._popup')

    @include('shared._delete_confirm')

    <script src="{{ mix('/js/app.js') }}"></script>

    <!-- Optional JavaScript -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src="vendors/jQuery.dotdotdot/dist/jquery.dotdotdot.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
    <script src="{!! asset('js/main.js') !!}"></script>

    @yield('js')

</body>

</html>