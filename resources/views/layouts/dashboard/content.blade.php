<div class="container-fixed container-fluid">
    <div class="row">

        <nav class="col-md-2 d-none d-md-block sidebar">
            <div class="sidebar-sticky">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('dashboard')}}">
                            Dashboard
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('users.index')}}">
                            Users Management
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('roles.index')}}">
                            Roles & Permission
                        </a>
                    </li>
                </ul>

            </div>
        </nav>

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
            <!-- Page Heading -->
            <h1 class="my-4">@yield('title')
                <small class="text-secondary">@yield('subtitle')</small>
            </h1>


            @include('flash::message')
            @include('shared/_error')
            @yield('content')
        </main>
    </div>

</div>