<div class="navbar-collapse collapse">
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown">
                @if(!Auth::user()->getMedia('profile')->first())
                <div class="user-thumbnail" style="background-image:url('{{ asset('img/default_user.png') }}')" /></div>
                @else
                <div class="user-thumbnail" style="background-image:url('{{ Auth::user()->getFirstMediaUrl('profile') }}')" /></div>
                @endif
            </a>
            <div class="dropdown-menu white-bg  dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item pb-2 pt-2" href="{{ route('profile') }}">My Profile</a>

                <a class="dropdown-item pb-2 pt-2" href="{{ route('projects.me') }}">My Project</a>

                <a class="dropdown-item pb-2 pt-2" href="{{ route('projects.joined') }}">My Joined Project</a>

                <!--
                @can('view_dashboard')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('dashboard') }}">Dashboard</a>
                @endcan
                -->

                @can('view_projects')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('projects.index') }}">Project Management</a>
                @endcan

                @can('view_users')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('users.index') }}">User Management</a>
                @endcan

                @can('view_categories')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('categories.index') }}">Category Management</a>
                @endcan

                @can('view_roles')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('roles.index') }}">Role Management</a>
                @endcan

                @can('view_banners')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('banners.index') }}">Banner Management</a>
                @endcan

                @can('view_journals')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('journals.index') }}">Journal Management</a>
                @endcan

                @can('view_presses')
                <a class="dropdown-item pb-2 pt-2" href="{{ route('presses.index') }}">Press Management</a>
                @endcan

                <a class="dropdown-item pb-2 pt-2" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
            </div>
        </li>
    </ul>
</div>

<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>