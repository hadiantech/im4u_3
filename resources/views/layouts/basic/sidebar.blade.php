<!-- sidebar -->
<div class="d-flex">
    <nav class="sidebar bg-dark">
        <ul class="list-unstyled">
            <li><a href="#"><i class="fa fa-fw fa-link"></i> Dashboard</a></li>
            <li style="position: relative;">
                <a href="#submenu1" data-toggle="collapse"><i class="fa fa-fw fa-users"></i> User Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu1" class="list-unstyled collapse">
                    <li><a href="vms_user_list.html">All User</a></li>
                    <li><a href="#">Create New User</a></li>
                </ul>
            </li>
            <li style="position: relative;">
                <a href="#submenu2" data-toggle="collapse"><i class="fa fa-fw fa-clipboard-check"></i> Project Management</a>
                <i class="fas fa-angle-down white-text" style="position: absolute; top:16px; right: 15px "></i>
                <ul id="submenu2" class="list-unstyled collapse">
                    <li><a href="vms_project_list.html">Published</a></li>
                    <li><a href="vms_project_pending.html">Pending <span class="badge badge-warning">10</span></a></li>
                    <li><a href="#">Suspended</a></li>
                    <li><a href="#">Ended</a></li>
                    <li><a href="#">Organisation</a></li>
                </ul>
            </li>

            <li><a href="#"><i class="fas fa-wrench"></i> Settings</a></li>

        </ul>
    </nav>

    <!-- REMINDER: PUT CONTENT INSIDE THIS DIV "content"  -->
</div>
<!-- end   -->