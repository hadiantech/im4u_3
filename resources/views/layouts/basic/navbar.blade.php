<nav class="navbar navbar-expand-lg navbar-light white-bg">
    <a class="navbar-brand p-0 m-0" href="/"><img src="{{asset('img/logo/vmlogotext.png')}}" width="auto" height="35" alt=""></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon" style="color: #000;"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <div class="item d-lg-flex">
                <a class="" href="{{route('about')}}">
                    <div class="nav-item nav-link blue4-text mx-lg-2" href="" style="position: relative; display: inline-block;">ABOUT US
                        <i class="fas fa-sort-down" style="position: absolute; right:-4px; top:14px; display: inline-block;"></i>
                        <div class="sub-menu white-bg rounded px-1 pt-3 pb-3">
                            <a class="nav-link blue4-text" href="{{route('about')}}#vision_mission">Vision & Mission</a>
                            <a class="nav-link blue4-text" href="{{route('about')}}#pillars">Our Pillars</a>
                   <!--         <a class="nav-link blue4-text" href="{{route('about')}}#team">Management Team</a> -->
                        </div>
                    </div>
                </a>

                <a class="" href="{{route('getinvolved')}}">
                    <div class="nav-item nav-link blue4-text mx-lg-3" href="" style="position: relative; display: inline-block;">GET INVOLVED
                        <i class="fas fa-sort-down" style="position: absolute; right:-4px; top:14px; display: inline-block;"></i>
                        <div class="sub-menu white-bg rounded px-1 pt-3 pb-3">
                            <a class="nav-link blue4-text" href="{{route('getinvolved')}}#whyvolunteer">Volunteer With Us</a>
                            <a class="nav-link blue4-text" href="{{route('getinvolved')}}#howtobe">Be A Volunteer</a>
                            <a class="nav-link blue4-text" href="{{route('getinvolved')}}#benefit">Benefits Of Volunteering</a>
                        </div>
                    </div>
                </a> 
                
                <a class="" href="{{route('explore')}}">
                    <div class="nav-item nav-link blue4-text mx-lg-2" href="" style="position: relative; display: inline-block;">PROJECTS & EVENTS
                        <i class="fas fa-sort-down" style="position: absolute; right:-4px; top:14px; display: inline-block;"></i>
                        <div class="sub-menu white-bg rounded px-1 pt-3 pb-3">
                            <a class="nav-link blue4-text" href="{{route('explore')}}#recentproject">Current Projects</a>
                            <a class="nav-link blue4-text" href="{{route('explore')}}#stories">Success Stories</a>
                            <a class="nav-link blue4-text" href="{{route('explore')}}#collaborate">Collaborate With Us</a>
                            <a class="nav-link blue4-text" href="{{route('explore')}}#partners">Our Partners</a>
                            <a class="nav-link blue4-text" href="{{route('explore')}}#projecthighlight">Projects Highlights</a>
                        </div>
                    </div>
                </a>

                <a class="" href="{{route('dreamfund')}}">
                    <div class="nav-item nav-link blue4-text mx-lg-3" href="" style="position: relative; display: inline-block;">DREAM FUND
                        <i class="fas fa-sort-down" style="position: absolute; right:-4px; top:14px; display: inline-block;"></i>
                        <div class="sub-menu white-bg rounded px-1 pt-3 pb-3">
                            <a class="nav-link blue4-text" href="{{route('dreamfund')}}#whatis">What is DREAM Fund</a>
                            <a class="nav-link blue4-text" href="{{route('dreamfund')}}#apply">Apply For DREAM Fund</a>
                            <a class="nav-link blue4-text" href="{{route('dreamfund')}}#projects">Project Examples</a>
                        </div>
                    </div>
                </a>

                <a class="" href="{{route('journal')}}">
                    <div class="nav-item nav-link nav-item-fix blue4-text mx-lg-3" href="" style="position: relative; display: inline-block; margin-bottom:5px;">THE PEOPLE'S JOURNAL
                    <i class="fas fa-sort-down" style="position: absolute; right:-4px; top:14px; display: inline-block;"></i>
                        <div class="sub-menu white-bg rounded px-1 pt-3 pb-3">
                            <a class="nav-link blue4-text" href="{{route('journal')}}#featured">Article of The Month</a>
                            <a class="nav-link blue4-text" href="{{route('journal')}}#share">Share Your Stories</a>
                            <a class="nav-link blue4-text" href="{{route('journal')}}#archive">Archives</a>
                    </div>
                    </div>
                </a> 

                <a class="" href="{{route('media')}}">
                    <div class="nav-item nav-link blue4-text mx-lg-3" href="" style="position: relative; display: inline-block;">MEDIA CENTRE
                        <i class="fas fa-sort-down" style="position: absolute; right:-4px; top:14px; display: inline-block;"></i>
                        <div class="sub-menu white-bg rounded px-1 pt-3 pb-3">
                            <a class="nav-link blue4-text" href="{{route('media')}}#press">Press Release</a>
                            <a class="nav-link blue4-text" href="{{route('media')}}#factsheet">Factsheet</a>
                            <a class="nav-link blue4-text" href="{{route('media')}}#faq">FAQ</a>
                            <a class="nav-link blue4-text" href="{{route('media.press')}}">Archives</a>
                            <a class="nav-link blue4-text" href="{{route('media')}}">Photo Gallery</a>
                        </div>
                    </div>
                </a> 

                <a class="" href="{{route('contactus')}}">
                    <div class="nav-item nav-link blue4-text mx-lg-3 nav-item-fix" href="" style="position: relative; display: inline-block;">CONTACT US
                    </div>
                </a> 

                @guest
                @else
                <a class="" href="{{ route('projects.create') }}">
                    <div class="nav-item nav-link blue4-text mx-lg-3 nav-item-fix" href="" style="position: relative; display: inline-block;">CREATE PROJECT</div>
                </a>
                @endguest

                @guest
                <a class="nav-item nav-link blue4-text mx-lg-2 nav-item-fix" href="{{route('login')}}" style="position: relative; display: inline-block;">SIGN IN</a> 
                @else

                @include('layouts.basic.user_menu')

                @endguest

            </div>
            <div class="item">

            </div>
        </div>
    </div>
</nav>