@extends('layouts.auth.master')

@section('content')

    


    <div class="row">
        <div class="col-md-12">
            <!-- Page Heading -->
            <h1 class="my-4">404
                <small>not found</small>
            </h1>
            <iframe src="https://open.spotify.com/embed?uri=spotify:user:tohawk89:playlist:3vB2W9YQ1gH5gDNzFpTuAY" width="300" height="380" frameborder="0" allowtransparency="true" allow="encrypted-media"></iframe>
            <br />
            <br />
            <a class="btn btn-link" href="{{ url()->previous() }}">Back</a>
            <a class="btn btn-link" href="{{ route('home') }}">Home</a>
        </div>
    </div>

@endsection
