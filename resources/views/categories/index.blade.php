@extends('layouts.admin.master')

@section('title', 'Category Management') 
@section('subtitle', 'List') 

@section('content')

<div class="card">
    <div class="card-body">
        @can('add_categories')
            <a href="{{route('categories.create')}}" class="btn btn-success">
                <i class="fas fa-plus"></i> New
            </a>
        @endcan
    </div>
</div>

<br />
<table id="categories-table" class="table table-bordered">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Created At</th>
        <th>Action</th>
    </tr>
    </thead>
</table>

@endsection

@section('js')
@parent
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>

<script>
    $(function () {
        $('#categories-table').DataTable({
            serverSide: true,
            processing: true,
            ajax: '/datatables/getCategories',
            columns: [
                {data: 'DT_Row_Index', orderable: false, searchable: false},
                {data: 'name'},
                {data: 'created_at'},
                {data: 'action', orderable: false, searchable: false}
            ]
        });
    });
</script>

@endsection