@extends('layouts.admin.master') 
@section('title', 'Category Management') 
@section('subtitle', 'Edit') 
@section('content') 

<div class="col-sm-12 border bg-white p-4">
    {{ Form::model($category,['route' => ['categories.update', $category->id], 'method' => 'PUT']) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }} 
        {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <br />
    <a class="btn btn-link" href="{{ route('categories.index') }}">Back</a> 
    {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} 
    
    {{ Form::close() }}
</div>

@endsection 
