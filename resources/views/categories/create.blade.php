@extends('layouts.admin.master') 
@section('title', 'Category Management') 
@section('subtitle', 'Create') 
@section('content') 

<div class="col-sm-12 border bg-white p-4">
    {{ Form::open(array('url' => 'categories')) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <br />
    <a class="btn btn-link" href="{{ route('categories.index') }}">Back</a> 
    {{ Form::submit('Add', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} 
    
    {{ Form::close() }}
</div>

@endsection 
