@extends('layouts.admin.master')

@section('title', 'Banners')

@section('content')

<div class="card">
    <div class="card-body">
        @can('add_banners')
            <a href="{{ route('banners.create') }}" class="btn btn-success">
                <i class="fas fa-plus"></i> New
            </a>
        @endcan
    </div>
</div>

<br />
<div>
    <a href="{{ URL::to('banners') }}">All</a> | <a href="{{ URL::to('banners?view=trash') }}">Trash</a>
</div>

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Order</th>
            <th>Image</th>
            <th>Hidden</th>
            <th>Operation</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($banners as $banner)
        <tr>

            <td>{{ $banner->order }}</td>
            <td>
                <img style="max-height: 50px;" src="{{ $banner->getMedia('banners')->first()->getUrl() }}" />
            </td>
            <td>@include('shared._check', ['check' => $banner->hidden])</td>

            <td>
            @include('shared._table_action_2', ['model' => $banner, 'url' => 'banners'])
            </td>
        </tr>
        @endforeach
    </tbody>

</table>


@endsection
