@extends('layouts.admin.master')

@section('title', 'Banners')
@section('subtitle', 'Edit')

@section('content')

<div class="col-sm-12 border bg-white p-4">

    <div>
        <img class="img-fluid" src="{{ $banner->getMedia('banners')->first()->getUrl() }}" />
    </div> <br />

    {{ Form::model($banner, array('route' => array('banners.update', $banner->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('link', 'Link') }}
        {{ Form::url('link', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('order', 'Order') }}
        {{ Form::number('order', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('hidden', 'Hidden ') }}

        {{ Form::hidden('hidden', 0) }}
        {{ Form::checkbox('hidden', 1, null) }}
    </div>

    <a class="btn btn-link" href="{{ route('banners.index') }}">Back</a> 
    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

</div>

@endsection
