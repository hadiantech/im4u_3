@extends('layouts.admin.master')

@section('title', 'Banners')

@section('subtitle', 'Create')

@section('content')

<div class="col-sm-12 border bg-white p-4">
    <div class='col-lg-12'>

    {!! Form::open(['url' => route('banners.store'), 'files' => 'true']) !!}

    <div class="form-group">
        {{ Form::label('banner', 'Banner') }}<br />
        {{ Form::file('banner', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
            {{ Form::label('link', 'Link') }}
            {{ Form::url('link', null, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('order', 'Order') }}
        {{ Form::number('order', false, array('class' => 'form-control', 'min' => '1', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('hidden', 'Hidden ') }}

        {{ Form::hidden('hidden', 0) }}
        {{ Form::checkbox('hidden', 1, false) }}
    </div>

    <a class="btn btn-link" href="{{ route('banners.index') }}">Back</a> 
    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection
