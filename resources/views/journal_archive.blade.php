@extends('layouts.front.master') @section('content')
<!--  header -->
<div class="container-fluid gray-bg p-0" style="min-height:90vh">
    <div class="container white-bg py-5" style="min-height:90vh">
        <div class="row">
            <div class="col-md-12 p-4">
                <h1 class="blue-text">THE PEOPLE'S JOURNAL ARCHIVE</h1>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-3 d-flex align-items-center">
                <input id="search" type="text" class="form-control mr-1" placeholder="Enter text to search..." /><i class="fas fa-search"></i>
            </div>
            <div>
                <select id="month_select" class="form-control" title="Filter by month">
                    <option value="1">Jan - Mac</option>
                    <option value="4">Apr - Jun</option>
                    <option value="7">Jul - Sep</option>
                    <option value="10">Oct - Dec</option>
                </select>
            </div>
            <div class="col-md-3">
                @php
                $now = Carbon\Carbon::now()->year;
                $last = 2014;
                @endphp
                <select id="year_select" class="form-control" title="Filter by year">
                    @for ($i = $now; $i >= $last; $i--)
                    <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                </select>
            </div>
        </div>

        {{-- Archive listing --}}
        
        <div id="data_listing" class="row mt-5 px-3">
            
        </div>
    </div>
</div>
<!--  end  -->]

@endsection 
@section('js') 
@parent
@javascript('year_selected', $year)
@javascript('month_selected', $month)

<script>

$( document ).ready(function() {
    $('#year_select').val(year_selected);
    $('#month_select').val(month_selected);
    call_ajax(year_selected, month_selected);
});

$('#year_select').on('change', function() {
    call_ajax(this.value, $('#month_select').val());
}); 

$('#month_select').on('change', function() {
    call_ajax( $('#year_select').val(), this.value);
}); 

function call_ajax(year, month){
    $.ajax({
        type:'GET',
        url:'/journal/year/'+year+'/'+month,
        success:function(data){
            append_archive(data);
        }
    });
}

$("#search").keyup(function() {
    var value = this.value.toLowerCase();

    $("#data_listing").find("a").each(function(index) {
        
        //if (!index) return;
        var item = $(this).find(".bold").first().text();
        $(this).toggle(item.toLowerCase().indexOf(value) !== -1);
    });
});

function append_archive(data){
    $('#data_listing').empty();
    $('#search').val('');

    if(data && data !=""){
        data.forEach(data => {
            $("#data_listing").append(`
            <a href="/journals/`+data['name']+`" target="_blank" style="width:100%">
                <div class="col-md-12 border-bottom p-3 tpj-list">
                    <p class="mb-0">`+data['released_at']+`</p>
                    <p class="bold mb-0">`+data['title']+`</p>
                </div>
            </a>
            `);
        });
    }else{
        $("#data_listing").append(`
        <div class="col-md-12 p-3 tpj-list">
            Sorry, there is no journal for your selected months.
        </div>
        `);
    }
    
}
</script>

@endsection