@extends('layouts.master')

@section('title', 'Versions')

@section('content')

<div class="col-lg-12 col-lg-offset-1">

    <div class="row bg-white has-shadow">
        <!-- Item -->
        <div class="col-lg-12">
        @can('Add Version')
        <a href="{{ URL::to('versions/create') }}" class="btn btn-success">Add Version</a>
        @endcan
        </div>
    </div>

    <section>
        <div class="row bg-white has-shadow">
            <div>
            <a href="{{ URL::to('versions') }}">All</a> | <a href="{{ URL::to('versions?view=trash') }}">Trash</a>
            </div>

            <table class="table table-responsive table-bordered table-striped">
                <thead>
                    <tr>

                        <th>Title</th>
                        <th>Released At</th>
                        <th>Detail</th>
                        <th>Highlight</th>
                        <th>Operation</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($versions as $version)
                    <tr>

                        <td>{{ $version->name }}</td>
                        <td>{{ $version->released_at }}</td>
                        <td>{{ $version->detail }}</td>
                        <td>@include('layouts.plugin.check', ['check' => $version->highlight])</td>
                        <td>

                        {!! Form::open(['method' => 'DELETE', 'route' => ['versions.destroy', $version->id] ]) !!}

                        @if(request('view') == 'trash')
                            <a href="{{ route('versions.restore', $version->id) }}" class="btn btn-info" role="button">Restore</a>
                        @else
                            @can('Edit Version')
                            <a href="{{ route('versions.edit', $version->id) }}" class="btn btn-info" role="button">Edit</a>
                            @endcan
                        @endif

                        @can('Delete Version')
                        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                        @endcan

                        {!! Form::close() !!}

                        </td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </section>

</div>

@endsection
