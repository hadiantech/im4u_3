@extends('layouts.master')
@include('layouts.plugin.editor')

@section('title', 'Versions')

@section('subtitle', '| Add Version')

@section('content')

<div class="row bg-white has-shadow">
    <div class='col-lg-12'>

    {{ Form::open(array('url' => 'versions')) }}

    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('highlight', 'Highlight ') }}

        {{ Form::hidden('highlight', 0) }}
        {{ Form::checkbox('highlight', 1, false) }}
    </div>

    <div class="form-group">
        {{ Form::label('detail', 'Detail') }}
        {{ Form::textarea('detail', null, array('class' => 'form-control my-editor', 'required')) }}
    </div>

    <div class="form-group">
        {{ Form::label('released_at', 'Released at') }}
        {{ Form::date('released_at', \Carbon\Carbon::now(), array('class' => 'form-control', 'required')) }}
    </div>


    {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}

    {{ Form::close() }}

    </div>
</div>

@endsection
