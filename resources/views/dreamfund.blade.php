@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/2.jpg)">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">DREAM FUND</h1>
            <h5 class="white-text text-shadow">Get Your Initiatives Funded</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  banner  -->
<div class="container-fluid p-0 white-bg pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 rounded box-shadow" style="background-image: url(img/dremfund/banner_bg.png); background-repeat: no-repeat; background-size: cover">
                <div class="row pt-5 pb-5 align-items-center text-center text-lg-left">
                    <div class="col-sm-12 col-lg-7 offset-lg-1 white-text text-shadow">
                        <h1>GET YOUR INITIATIVES</br>FUNDED NOW!</h1>
                    </div>
                    <div class="col-sm-12 col-lg-3"><a href="http://dream.volunteermalaysia.my/login.php" target="_blank" class="btn btn-white">APPLY DREAM FUND</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!--  what is  -->
<div id="whatis" class="container-fluid p-0 bluesea-bg pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-5">
                <h1 class="blue-text">WHAT IS DREAM FUND</h1>
                <p class="blue-text">The DREAM Fund provides seed funding support for youth to carry out volunteer activities that are aimed at helping targeted communities and social groups. The fund cover initiatives that are centered on our
                    four pillars:</p>
            </div>
        </div>
        <div class="row mt-4 pb-5">
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/arts-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Arts, Culture & Innovation</h4>
                            <p class="blue-text">We promote Malaysian culture and diversity, encouraging communities to pursue their interests in arts and culture which will drive innovation.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/community-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Community Well-Being</h4>
                            <p class="blue-text">We promote inclusiveness and embrace harmony by reaching out to communities in need to build a better living environment.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/sport-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Sports</h4>
                            <br/>
                            <p class="blue-text">We promote physical and mental well-being while fostering team spirit to build a healthier and united nation.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/knowledge-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Knowledge Building</h4>
                            <br>
                            <p class="blue-text">We engage in learning and educational activities to enhance knowledge and skills of the local communities.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!-- apply for   -->
<div id="apply" class="container-fluid blue-bg p-5">
    <div class="container pt-5 pb-5">
        <div class="row no-gutters">
            <div class="col-sm-12">
                <div class="row justify-content-center">
                    <div class="col-sm-12 col-lg-6 text-center text-lg-left">
                        <h2 class="yellow-text mb-4">APPLY FOR DREAM FUND TODAY</h2>
                        <p class="white-text">The DREAM Fund is a great platform for youth to unleash their potential, build a sense of community, character, wholeness and unity among the youths of Malaysia.</p>
                        <p class="white-text">Apply for DREAM Fund today and champion your very own volunteering program.</p>
                        <div class="col-sm-12 m-0 p-0 mt-5">
                            <div class="row no-gutters">
                                <div class="col-sm-12 mb-3 mr-lg-3 col-lg-5"><a href="http://dream.volunteermalaysia.my/login.php" target="_blank" class="btn btn-yellow btn-med btn-block">APPLY FOR DREAM FUND</a></div>
                             <!--   <div class="col-sm-12 mb-3 col-lg-5"><a href="" class="btn btn-yellow btn-med btn-block">TERMS & CONDITIONS</a></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 m-4 col-lg-3">
                        <img src="img/dremfund/howtoapply_icon.png" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!--  process  -->

<div class="container-fluid p-5 bluesea-bg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-lg-5 m-3 border bg-white pb-5 pt-4 rounded">
                <img src="img/dremfund/applyprocess.png" class="img-fluid" />
            </div>
            <div class="col-sm-12 col-lg-5 m-3 border bg-white pb-5 pt-4 rounded">
                <img src="img/dremfund/Eligibility.png" class="img-fluid" />
            </div>
        </div>
    </div>
</div>

<!-- end  -->

<!--  projects  -->
<div id="projects" class="container-fluid gray-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mt-5 mb-2 p-0">
                <h1 class="blue-text">DREAM FUND PROJECT EXAMPLES</h1>
            </div>
        </div>
        <div class="row mb-5">
            <div class="slick-collab">
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/dremfund/rumah/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 pb-3 dotdotdot-title p-3 text-center">Rumah Putih</h5>
                    <div class="col-md-12 d-flex flex-wrap justify-content-center">
                        <a href="" data-toggle="modal" data-target="#rumah" class="btn btn-yellow mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/dremfund/green/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 pb-3 dotdotdot-title p-3 text-center">Program Green Tea..</h5>
                    <div class="col-md-12 d-flex flex-wrap justify-content-center">
                        <a href="" data-toggle="modal" data-target="#green" class="btn btn-yellow mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/dremfund/madac/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 pb-3 dotdotdot-title p-3 text-center">MADAC 2017</h5>
                    <div class="col-md-12 d-flex flex-wrap justify-content-center">
                        <a href="" data-toggle="modal" data-target="#madac" class="btn btn-yellow mb-3">READ STORY</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url(img/dremfund/csr/1.jpg); background-size: cover; background-repeat: no-repeat; background-position: center;"></div>
                    <h5 class="pt-3 pb-3 dotdotdot-title p-3 text-center">CSR Rumah Anak Ya..</h5>
                    <div class="col-md-12 d-flex flex-wrap justify-content-center">
                        <a href="" data-toggle="modal" data-target="#csr" class="btn btn-yellow mb-3">READ STORY</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!--  modal  -->
 <div class="modal fade" id="rumah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="position: relative;">
            <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                <div class="row" style="position: absolute; right: 20px">
                    <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row no-gutters justify-content-center align-items-center">
                    <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/rumah/1.jpg" class="img-fluid" /></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-4 mt-3">
                        <h2 class="blue-text">RUMAH PUTIH</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">INITIATED BY</h4>
                        <p>Unit Penyelarasan Dan Pemantauan Institusi Akar Umbi Negeri Sembilan</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">OBJECTIVES</h4>
                        <p>To repair places of worship (mosque) with minimal expenses</p>
                        <p>Creating a platform for government organisations to give back to local community</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">TARGET AUDIENCES</h4>
                        <p>Local community of Kampung Lambar, Negeri Sembilan</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">ACHIEVEMENTS</h4>
                        <p>A total of 30 volunteers from the local community of Kampung Lambar, Negeri Sembilan and Unit Penyelarasan Dan Pemantauan Institusi Akar Umbi Negeri Sembilan participated in the program.</p>
                        <p>The volunteers conducted refurbishment works at the surau, extending new spaces and repainting walls.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3 mt-3">
                        <h4 class="blue-text">PHOTO GALLERY</h4>
                    </div>
                    <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/rumah/2.jpg" class="img-fluid" /></div>
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/rumah/3.jpg" class="img-fluid" /></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="green" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="position: relative;">
            <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                <div class="row" style="position: absolute; right: 20px">
                    <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row no-gutters justify-content-center align-items-center">
                    <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/green/1.jpg" class="img-fluid" /></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-4 mt-3">
                        <h2 class="blue-text">PROGRAM GREEN TEAMS GENERATION X</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">INITIATED BY</h4>
                        <p>Pertubuhan Aspirasi Prihatin Malaysia</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">OBJECTIVES</h4>
                        <p>Provide exposure to the volunteers through community related activities. Foster the spirit of volunteerism among the youth and encourage uptake of social welfare related activities</p>
                        <p>Assist in coastal clean ups and planting of mangrove trees around areas affected by the tsunami</p>
                        <p>Provide encouragement and support to the surrounding communities</p>
                        <p>Build good relationships between the youth, volunteers, agencies, surrounding community and local residents.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">TARGET AUDIENCES</h4>
                        <p>The program was targeted at the Sea Bajau community in Omadal island and several other local communities made up of children and the elderly within Omadal island that needed medical treatment.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">ACHIEVEMENTS</h4>
                        <p>A total of 36 volunteers from the Youth Club members Tinagayan, Alumni MYCorps Malaysia, the Malaysian Dental Relief and Association of Women Omadal Island Semporna (WAPO) participated in the program.</p>
                        <p>A health screening was conducted by the Malaysian Dental Relief and several hygiene care activities were carried out for the kids and local community.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3 mt-3">
                        <h4 class="blue-text">PHOTO GALLERY</h4>
                    </div>
                    <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/green/2.jpg" class="img-fluid" /></div>
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/green/3.jpg" class="img-fluid" /></div>
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/green/4.jpg" class="img-fluid" /></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="madac" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="position: relative;">
            <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                <div class="row" style="position: absolute; right: 20px">
                    <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row no-gutters justify-content-center align-items-center">
                    <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/madac/1.jpg" class="img-fluid" /></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-4 mt-3">
                        <h2 class="blue-text">MEDICAL & DENTAL AWARENESS (M.A.D.A.C) 2017</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">INITIATED BY</h4>
                        <p>Kelab Belia Tinagayan Semporna</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">OBJECTIVES</h4>
                        <p>Spread the spirit of volunteerism to the youths and inspire them to carry out educational and awareness activities.</p>
                        <p>Instill the importance of personal hygiene and the environment to the Sea Bajau community.</p>
                        <p>Develop an understanding and partnership between Youth Club Tinagayan and other NGOs to carry out volunteering works within or beyond Semporna</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">TARGET AUDIENCES</h4>
                        <p>The program was targeted at the Sea Bajau community in Omadal island and several other local communities made up of children and the elderly within Omadal island that needed medical treatment.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">ACHIEVEMENTS</h4>
                        <p>A total of 36 volunteers from the Youth Club members Tinagayan, Alumni MYCorps Malaysia, the Malaysian Dental Relief and Association of Women Omadal Island Semporna (WAPO) participated in the program.</p>
                        <p>A health screening was conducted by the Malaysian Dental Relief and several hygiene care activities were carried out for the kids and local community.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3 mt-3">
                        <h4 class="blue-text">PHOTO GALLERY</h4>
                    </div>
                    <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/madac/2.jpg" class="img-fluid" /></div>
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/madac/3.jpg" class="img-fluid" /></div>
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/madac/4.jpg" class="img-fluid" /></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="csr" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="position: relative;">
            <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                <div class="row" style="position: absolute; right: 20px">
                    <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="row no-gutters justify-content-center align-items-center">
                    <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/csr/1.jpg" class="img-fluid" /></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 mb-4 mt-3">
                        <h2 class="blue-text">CSR RUMAH ANAK YATIM PAYASUM 2017</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">INITIATED BY</h4>
                        <p>Politeknik Melaka</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">OBJECTIVES</h4>
                        <p>Spread the spirit of volunteerism to the youths and inspire them to carry out educational and awareness activities</p>
                        <p>To create a sense of responsibility amongst the volunteers and collaborative organisation for a common purpose.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">TARGET AUDIENCES</h4>
                        <p>Orphanages from Payasum.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3">
                        <h4 class="blue-text mb-3">ACHIEVEMENTS</h4>
                        <p>A total of 63 students and 5 lecturers from Polytechnic of Malacca CODES participated in the program. The Volunteer Division of Polytechnic Malacca was also in support of the program conducting mural paintings on the school walls.</p>
                        <p>The student volunteers were empowered as they contributed their time and effort to the local community. The student volunteers were given the opportunity to give back to the community through refurbishment works and beautification at the school.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mb-3 mt-3">
                        <h4 class="blue-text">PHOTO GALLERY</h4>
                    </div>
                    <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/csr/2.jpg" class="img-fluid" /></div>
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/dremfund/csr/3.jpg" class="img-fluid" /></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--  end  -->

@endsection @section('js') @parent @endsection