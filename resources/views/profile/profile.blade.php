<div class="row">
    <div class="col-md-12 p-0">
        <div class="card">
            <div class="card-header bold">
                <i class="fas fa-user-circle"></i> My Profile
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        {{ Form::model($user, ['route' => ['profile.update'], 'method' => 'PUT']) }}
                
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Full Name</b></p>
                            {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px">
                                @if($user->identification_type == 'ic')
                                <b>IC Number</b>
                                @else
                                <b>Passport Number</b>
                                @endif
                            </p>
                            {{ Form::text('identification_number', null, array('class' => 'form-control', 'required')) }}
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Marital Status</b></p>
                            <select class="form-control mx-0">
                                <option value="" @if($user->marital_status == 'single') selected @endif>Single</option>
                                <option value="" @if($user->marital_status == 'married') selected @endif>Married</option>
                                <option value="" @if($user->marital_status == 'divorced') selected @endif>Divorced</option>
                            </select>
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Date of Birth</b></p>
                            
                            {{ Form::date('date_of_birth', null ,array('class'=> 'form-control', 'required')) }}
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Address</b></p>
                            <div class="row">
                                {{ Form::text('address_1', null ,array('class'=> 'form-control', 'placeholder'=> "Address 1")) }}
                                {{ Form::text('address_2', null ,array('class'=> 'form-control', 'placeholder'=> "Address 2")) }}
                                {{ Form::text('city', null ,array('class'=> 'form-control', 'placeholder'=> "City")) }}

                                <select id="state" name="state" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }} mx-0" required>
                                    @include('shared._state_dropdown', ['model'=>$user])
                                </select>
                            </div>
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Postcode</b></p>
                            {{ Form::text('postcode',null ,array('class'=> 'form-control', 'required')) }}
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Country</b></p>
                            <select id="country" name="country" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }} mx-0" required>
                                @include('shared._country_dropdown', ['model'=>$user])
                            </select>
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Highest Education</b></p>
                            <select id="education" name="education" class="form-control{{ $errors->has('education') ? ' is-invalid' : '' }} mx-0" required>
                                @include('shared._education_dropdown', ['model'=>$user])
                            </select>
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Profession</b></p>
                            <select id="profession" name="profession" class="profession form-control{{ $errors->has('interest') ? ' is-invalid' : '' }} mx-0" required>
                                @include('shared._profession_dropdown', ['model'=>$user])
                            </select>
                        </div>
                        <div class="d-flex align-items-center my-3">
                            <p class="mb-0" style="min-width:150px"><b>Interest</b></p>
                            <select  multiple="multiple" id="interest" name="interest[]" class="interestdropdown form-control{{ $errors->has('interest') ? ' is-invalid' : '' }} mx-0">
                                @include('shared._interest_dropdown', ['model'=>$user])
                            </select>
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} 

                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('js')
@parent

<script>
    $('#profession').val($('#profession').find(':selected').text());
    $('#interest').val(interests);
</script>

@endsection