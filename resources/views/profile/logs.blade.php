<div class="row">
    <div class="col-md-12 p-0">
        <div class="card">
            <div class="card-header bold">
                <i class="fas fa-clipboard-list"></i> Activities
            </div>
            <div class="card-body">

                <table class="table table-bordered">
                    <tr>
                        <td>Time</td>
                        <td>Description</td>
                    </tr>
                    @foreach($logs as $log)
                    <tr>
                        <td>
                            {{ $log->created_at }}
                        </td>
                        <td>
                            @if($log->description == 'updated' or $log->description == 'deleted' or $log->description == 'created')
                                @if($log->subject_type == 'App\User' and $log->subject_id == Auth::user()->id)
                                    Profile updated
                                @else
                                    {{ str_replace('App\\', '', $log->subject_type) . ' ' . $log->subject_id . ' ' .  $log->description }}
                                @endif
                            @else
                                {{ ucfirst(trans($log->description)) }}
                            @endif
                        </td>

                        {{-- <td>
                            {{ $log->subject_type }}
                        </td>
                        <td>
                            {{ $log->description }}
                        </td>
                        <td>
                            {{ $log->subject_id }}
                        </td> --}}
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
