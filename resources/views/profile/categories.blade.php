<div class="row">
    <div class="col-md-12 p-0">
        <div class="card">
            <div class="card-header bold">
                <i class="fas fa-tag"></i> Categories
            </div>
            <div class="card-body">

                {!! Form::model($user, ['route' => ['profile.update.categories', $user->id], 'method' => 'PUT']) !!}

                @foreach($categories as $category)
                <div class="custom-control custom-checkbox mr-3">
                    <input type="checkbox" class="custom-control-input" name="category[]" {{ $user->categories->contains($category->id)  ? 'checked' : ''}}  value="{{ $category->id }}" id="cat_{{ $loop->iteration }}">
                    <label class="custom-control-label" for="cat_{{ $loop->iteration }}">{{ $category->name }}</label>
                </div>
                @endforeach

                <br />
                {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>