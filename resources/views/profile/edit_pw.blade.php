<div class="row">
        <div class="col-md-12 p-0">
            <div class="card">
                <div class="card-header bold">
                    <i class="fas fa-key"></i> Password
                </div>
                <div class="card-body">

                    <div class="row">
                        <div class='col-lg-12'>

                        {{ Form::model($user, ['route' => ['profile.pw_update'], 'method' => 'PUT']) }}

                        <div class="form-group">
                            {{ Form::label('old_password', 'Old Password') }}
                            {{ Form::password('old_password', array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('password', 'Password') }}
                            {{ Form::password('password', array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('password_confirmation', 'Confirm Password') }}
                            {{ Form::password('password_confirmation', array('class' => 'form-control', 'required')) }}
                        </div>

                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-tools btn-xxx')) }}

                        {{ Form::close() }}

                        </div>
                    </div>

            </div>
        </div>
    </div>
</div>