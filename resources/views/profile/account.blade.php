<div class="row">
    <div class="col-md-12 p-0">
        <div class="card">
            <div class="card-header bold">
                <i class="fas fa-wrench"></i> Account
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-lg-12'>
                        {{ Form::model($user, ['route' => ['account.update'], 'method' => 'PUT']) }}

                        <div class="form-group">
                            {{ Form::label('name', 'Full Name') }} 
                            {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('email', 'E-Mail Address') }} {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
                            <p class="mt-1" style="font-size:0.7em; font-style:italic; font-weight:bold">If you change your email, you are required to verify your new email address.</p>
                        </div>

                        <div class="form-group">
                            {{ Form::label('phone_number', 'Phone Number') }} {{ Form::text('phone_number', null, array('class' => 'form-control', 'required')) }}
                            <p class="mt-1" style="font-size:0.7em; font-style:italic; font-weight:bold">If you change your phone number, you are required to verify your new phone number.</p>
                        </div>

                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} 
                        {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row mt-3">
    <div class="col-md-12 p-0">
        <div class="card">
            <div class="card-header bold">
                <i class="fas fa-exclamation-triangle"></i> Disaster Response
            </div>
            <div class="card-body">
                <div class="row">
                    <div class='col-lg-12'>
                        {{ Form::model($user, ['route' => ['profile.disaster'], 'method' => 'PUT']) }}

                        <div class="form-group">

                            <div class="custom-control custom-checkbox">
                                {{-- <input type="checkbox" class="custom-control-input" id="emergency" name="emergec"> --}}
                                
                                {!! Form::hidden('emergency', 0) !!}
                                {{ Form::checkbox('emergency', 1, null, ["id"=>"emergency", "class"=>"custom-control-input"]) }}
                                <label class="custom-control-label" for="emergency">I want to be contacted to offer aid in times of emergencies</label>
                            </div>
                        </div>
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} {{ Form::close() }}

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>