@extends('layouts.front.master') @section('title', 'My Profile') @section('subtitle', '') @section('content')

<div class="container mt-5">
    <div class="row">
        <div class="col-sm-12 col-md-5 col-lg-3 p-0 border rounded white-bg m-2">
            <div class="row">
                @if(!$user->getMedia('profile')->first())
                <img class="card-img-top rounded img-fluid mb-3" src="http://via.placeholder.com/300x300"> @else
                <img class="card-img-top rounded img-fluid mb-3" src="{{ $user->getMedia('profile')->first()->getUrl()}} "> @endif
                <div class="row p-3">
                    @include('shared._upload_image', ['collection' => 'profile', 'model_name' => 'User', 'model_id' => $user->id])
                </div>
                 
            </div>
            <div class="row p-3">
                <div class="col-md-12">
                    <h3 class="mb-3">{{ $user->name }}</h3>
                    <p class="my-3">
                        <i class="fas fa-phone-square mr-1"></i> {{ $user->phone_number }}
                    </p>
                    <p class="my-3">
                        <i class="fas fa-envelope-open mr-1"></i> {{ $user->email }}
                        @if($notverify)
                        <br />
                        <small>
                        Email not verify yet. <a href="{{ route('email.resend') }}">Send Email</a>
                        </small>
                        @endif
                    </p>
                        
                    <p class="my-3">
                        <i class="fas fa-certificate mr-1"></i>
                        <b>Volunteer Hours</b>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 col-lg-8 p-2 border rounded white-bg m-2">
            <div class="row border-bottom">
                <div class="col-md-12 p-3">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home"
                                aria-selected="true">My Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile"
                                aria-selected="false">Account</a>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" id="pills-password-tab" data-toggle="pill" href="#pills-password" role="tab" aria-controls="pills-password"
                                    aria-selected="false">Password</a>
                            </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact"
                                aria-selected="false">Categories</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-cert-tab" data-toggle="pill" href="#pills-cert" role="tab" aria-controls="pills-contact" aria-selected="false">Certificates</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-act-tab" data-toggle="pill" href="#pills-act" role="tab" aria-controls="pills-contact" aria-selected="false">Activity</a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="row">
                <div class="col-md-12 p-3">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            @include('profile.profile')
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                            @include('profile.account')
                        </div>
                        <div class="tab-pane fade" id="pills-password" role="tabpanel" aria-labelledby="pills-password-tab">
                            @include('profile.edit_pw')
                        </div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                            @include('profile.categories')              
                        </div>
                        <div class="tab-pane fade" id="pills-cert" role="tabpanel" aria-labelledby="pills-contact-tab">
                            @include('profile.certificate')
                        </div>
                        <div class="tab-pane fade" id="pills-act" role="tabpanel" aria-labelledby="pills-act-tab">
                            @include('profile.logs')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
@parent
<script>
    $(document).ready(function() {
        $('#interest').select2({
            multiple:true,
            placeholder: 'Select value...'
        });
    });

     $(document).ready(function() {
        $('#profession').select2();
    });
</script>

@endsection