<div class="row">
    <div class='col-lg-12'>
        {{ Form::model($user, ['route' => ['profile.update'], 'method' => 'PUT']) }}

        <div class="form-group">
            {{ Form::label('name', 'Full Name') }} {{ Form::text('name', null, array('class' => 'form-control', 'required')) }}
        </div>

        <div class="form-group">
            {{ Form::label('email', 'E-Mail Address') }} {{ Form::email('email', null, array('class' => 'form-control', 'required')) }}
        </div>

         {{ Form::submit('Save', array('class' => 'btn btn-primary btn-tools btn-xxx')) }} {{ Form::close() }}

    </div>
</div>