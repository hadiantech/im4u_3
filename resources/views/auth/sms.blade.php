@extends('layouts.auth.master')

@section('content')
<div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-2 mb-5">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3" style="position:relative;">
                <div class="row justify-content-between align-items-center mb-3">
                    <div class="col-xs-6 pt-3 pr-3 pl-3 pb-0" style="z-index:1">
                        <h3 class="mb-0 white-text">VERIFY PHONE</h3>
                    </div>
                    <div class="col-xs-2 pr-3" style="z-index:1">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="width: 70px">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>
                <div class="row mb-5 px-3">
                    <div class="col-sm-12 p-0 mb-3">
                        <a href="{{route('sms.resend')}}" class="mb-1">Resend SMS</a>
                        <br />
                        <a href="{{route('phone.change')}}" class="">Change Number Phone</a>
                    </div>
                </div>

                <div class="row pb-5">
                    <form class="col-sm-12 mb-5 pb-5" method="POST" action="{{ route('sms.confirm.code') }}">
                        @csrf
                        
                        <div class="form-group">
                        <p class="mb-3">We have sent you an SMS with a code to your registered mobile number. Please enter the code below.</p>
                            <input id="code" name="code"  type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" placeholder="SMS Code" required>
                            @if ($errors->has('code'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('code') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <button type="submit" class="btn btn-green2 btn-block mb-2">Verify</button>
                    </form> 
                </div> 
            </div>
        </div>
    </div>
</div>

@endsection
