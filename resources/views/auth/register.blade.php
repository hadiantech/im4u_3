@extends('layouts.auth.master')

@section('content')
<div class="container p-0">
    <div class="container-fluid m-0 mt-5 mb-4 p-0">
        <div class="row p-1">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3">
                <div class="row justify-content-between align-items-center mb-0 px-3" style="position:relative;">
                    <div class="col-xs-6" style="z-index:1">
                        <h3 class="mb-0 white-text">REGISTER</h3>
                    </div>
                    <div class="col-xs-2 pt-3 pr-4 pb-0" style="z-index:1">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto;">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>

                <div class="row mb-5 px-3">
                    <div class="col-sm-12 p-0">
                        <p class="mb-0 white-text">Already Have An Account?</p>
                        <a href="{{route('login')}}" class="bold">Sign In</a>
                    </div>
                </div>

                <div class="row">
                    <form class="col-sm-12" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group">
                            <input id="name" name="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="FULL NAME" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="email" name="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}"  placeholder="EMAIL" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-sm-4">
                                    <select id="identification_type" name="identification_type" class="form-control {{ $errors->has('identification_type') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                                          <option value="ic">IC/MYKAD</option>
                                          <option value="passport">PASSPORT NUMBER</option>
                                    </select>
                                    @if ($errors->has('identification_type'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('identification_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-sm-8">
                                    <input id="identification_number" name="identification_number" type="text" class="form-control {{ $errors->has('identification_number') ? ' is-invalid' : '' }}" placeholder="IC NUMBER" value="{{ old('identification_number') }}" required>
                                    @if ($errors->has('identification_number'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('identification_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <select id="marital_status" name="marital_status" class="form-control {{ $errors->has('marital_status') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                                  <option value="">MARITAL STATUS</option>
                                  <option value="single">SINGLE</option>
                                  <option value="married">MARRIED</option>
                                  <option value="divorced">DIVORCED</option>
                            </select>
                            @if ($errors->has('marital_status'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('marital_status') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="phone_number" name="phone_number"  type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="MOBILE NUMBER (eg. 0123456789)" value="{{ old('phone_number') }}" required>
                            @if ($errors->has('phone_number'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <select id="state" name="state" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                                @include('shared._state_dropdown')
                            </select>
                            @if ($errors->has('state'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('state') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password" name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="PASSWORD" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" name="password_confirmation" type="password" class="form-control"  placeholder="CONFIRM PASSWORD" required>
                        </div>

                        <button type="submit" class="btn btn-green2 btn-block">REGISTER</button>
                    </form>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-12 text-center mt-4">
                        <p class="bold">or register with:</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <a href="{{ url('auth/facebook') }}" class="hover-zoom">
                        <div class="fa-layers fa-fw fa-3x" style="">
                            <i class="fas fa-circle" style="color:gainsboro"></i>
                            <i class="fa-inverse fab fa-facebook-f green2-text" data-fa-transform="shrink-6"></i>
                        </div>
                    </a>
                    <a href="{{ url('auth/google') }}" class="hover-zoom">
                        <div class="fa-layers fa-fw fa-3x" style="">
                            <i class="fas fa-circle" style="color:gainsboro"></i>
                            <i class="fa-inverse fab fa-google green2-text" data-fa-transform="shrink-6"></i>
                        </div>
                    </a>
                </div>
                <div class="row mt-5 mb-1">
                    <div class="col-sm-12 text-center">
                        <p>By signing up with Volunteer Malaysia, you agree to comply with the <span class="bold">terms & conditions</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
