@extends('layouts.auth.master')

@section('content')
<div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-2" style="height:650px">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3">
                <div class="row justify-content-between align-items-center mb-3 px-3" style="position:relative;">
                    <div class="col-xs-6" style="z-index:1">
                        <h3 class="mb-0 white-text">FORGOT <br> PASSWORD</h3>
                    </div>
                    <div class="col-xs-2 pt-3 pr-4 pb-0" style="z-index:1">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto;">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>

                <div class="row mb-5 px-3">
                    <div class="col-sm-12 p-0">
                        <a href="{{route('login')}}" class="bold"><i class="fas fa-arrow-left mr-2"></i> Back</a>
                    </div>
                </div>
                <div class="row" style="margin-top:100px">
                    <form class="col-sm-12" method="POST" action="{{ route('password.email') }}">
                        
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
                        @csrf

                        <div class="form-group">
                            <p>Enter your registered email below</p>
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  placeholder="Enter email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-green2 btn-block">Send Password Reset Link</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-2">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3">
                <div class="row justify-content-between align-items-center mb-3">
                    <div class="col-xs-6">
                        <h3 class="mb-0">FORGOT PASSWORD</h3>
                    </div>
                    <div class="col-xs-2">
                        <img src="{{asset('img/logo/footerlogo.png')}}" class="img-fluid" style="width: 70px">
                    </div>
                </div>

                <div class="row">
                    <form class="col-sm-12" method="POST" action="{{ route('password.email') }}">
                        
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        
                        @csrf

                        <div class="form-group">
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  placeholder="Enter email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-green btn-block">Send Password Reset Link</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

@endsection
