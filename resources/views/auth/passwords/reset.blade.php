@extends('layouts.auth.master')

@section('content')

<div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-2">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3">
                <div class="row justify-content-between align-items-center mb-3">
                    <div class="col-xs-6">
                        <h3 class="mb-0">RESET <br>PASSWORD</h3>
                    </div>
                    <div class="col-xs-2 pt-3 pr-4 pb-0">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto;">
                    </div>
                </div>

                <div class="row">
                    <form class="col-sm-12" method="POST" action="{{ route('password.request') }}">
                        @csrf
    
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email or old('email') }}" placeholder="Enter email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Enter New Password"  required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" placeholder="Confirm New Password" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-green btn-block">Reset Password</button>

                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
