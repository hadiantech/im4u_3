@extends('layouts.auth.master')

@section('content')
<div class="container p-0">
    <div class="container-fluid m-0 mt-5 mb-4 p-0">
        <div class="row p-1">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3">
                <div class="row justify-content-between align-items-center mb-0 px-3" style="position:relative;">
                    <div class="col-xs-6" style="z-index:1">
                        <h3 class="pt-3 mb-5 white-text">COMPLETE YOUR <br>PROFILE</h3>
                    </div>
                    <div class="col-xs-2 pr-4" style="z-index:1">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto;">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>


                <div class="row mt-5 mb-5 pb-5">
                    <form class="col-sm-12" method="POST" action="{{ route('auth.update') }}">
                        @csrf
                      
                      <div class="form-group">
                            <div class="form-row">
                                <div class="col-sm-4">
                                    <select id="identification_type" name="identification_type" class="form-control {{ $errors->has('identification_type') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                                          <option value="ic">IC/MYKAD</option>
                                          <option value="passport">PASSPORT NUMBER</option>
                                    </select>
                                    @if ($errors->has('identification_type'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('identification_type') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-sm-8">
                                    <input id="identification_number" name="identification_number" type="text" class="form-control {{ $errors->has('identification_number') ? ' is-invalid' : '' }}" placeholder="IC NUMBER" value="{{ old('identification_number') }}" required>
                                    @if ($errors->has('identification_number'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('identification_number') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <select id="marital_status" name="marital_status" class="form-control {{ $errors->has('marital_status') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                                  <option value="">MARITAL STATUS</option>
                                  <option value="single">SINGLE</option>
                                  <option value="married">MARRIED</option>
                                  <option value="divorced">DIVORCED</option>
                            </select>
                            @if ($errors->has('marital_status'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('marital_status') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="phone_number" name="phone_number"  type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="MOBILE NUMBER (eg. 0123456789)" value="{{ old('phone_number') }}" required>
                            @if ($errors->has('phone_number'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <select id="state" name="state" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" required style="margin:7px 0px !important">
                                @include('shared._state_dropdown')
                            </select>
                            @if ($errors->has('state'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('state') }}</strong>
                                </span>
                            @endif
                        </div>

                        <button type="submit" class="btn btn-green2 btn-block">SAVE</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
