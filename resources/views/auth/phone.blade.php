@extends('layouts.auth.master')

@section('content')
<div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-2">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-3">
                <div class="row justify-content-between align-items-center mb-0 px-3" style="position:relative;">
                    <div class="col-xs-6" style="z-index:1">
                        <h3 class="pt-3 mb-5 white-text">CHANGE MOBILE <br>NUMBER</h3>
                    </div>
                    <div class="col-xs-2 pr-4" style="z-index:1">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto;">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>

                <div class="row">
                    <form class="col-sm-12 mt-5 mb-5 pt-3 pb-5" method="POST" action="{{ route('phone.update') }}">
                        @csrf
                        
                        <div class="form-group">
                            <input id="phone_number" name="phone_number"  type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="MOBILE NUMBER" value="{{ $user->phone_number or old('phone_number') }}" required>
                            @if ($errors->has('phone_number'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                </span>
                            @endif
                        </div>
                        
                        <button type="submit" class="btn btn-green2 btn-block mb-5">Save</button>
                    </form>
                </div>
                
            </div>
        </div>
    </div>
</div>

@endsection
