@extends('layouts.auth.master')

@section('content')

<div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-2">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow">
                <div class="row justify-content-between align-items-center mb-3" style="position: relative;">
                    
                    <div class="col-xs-6 p-3 mb-0 mt-2" style="z-index: 10; ">
                        <h3 class="mb-0 text-white">SIGN IN</h3>
                    </div>
                    <div class="col-xs-2 pt-3 pr-4 pb-0" style="z-index: 99;">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto; ">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>
                <div class="row mb-5" style="margin-top: -30px">
                    <div class="col-sm-12 p-3">
                        <p class="mb-0 text-white">Do Not Have An Account?</p>
                        <a href="{{route('register')}}" class="bold">Register Now</a>
                    </div>
                </div>
                <div class="row">
                    <form class="col-sm-12" method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"  placeholder="Enter email" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-green2 btn-block">SIGN IN</button>
                    </form>
                </div>
                <div class="row mb-3">
                    <div class="col-sm-12 text-center mt-4">
                        <p class="bold">or sign in with:</p>
                    </div>
                </div>
                <div class="row justify-content-center">
                    <a href="{{ url('auth/facebook') }}" class="hover-zoom">
                        <div class="fa-layers fa-fw fa-3x" style="">
                            <i class="fas fa-circle" style="color:gainsboro"></i>
                            <i class="fa-inverse fab fa-facebook-f green2-text" data-fa-transform="shrink-6"></i>
                        </div>
                    </a>
                    <a href="{{ url('auth/google') }}" class="hover-zoom">
                        <div class="fa-layers fa-fw fa-3x" style="">
                            <i class="fas fa-circle" style="color:gainsboro"></i>
                            <i class="fa-inverse fab fa-google green2-text" data-fa-transform="shrink-6"></i>
                        </div>
                    </a>
                </div>
                <div class="row mt-5 mb-3">
                    <div class="col-sm-12 text-center">
                        <a href="{{ route('password.request') }}" class="bold"><p>Forgot Password?</p></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
