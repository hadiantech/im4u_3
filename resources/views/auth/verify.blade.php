@extends('layouts.auth.master')

@section('content')

<div class="container p-0">
    <div class="container-fluid m-0 mt-5 p-0">
        <div class="row p-1">
            <div class="col-sm-12 col-md-6 offset-md-3 col-lg-5 white-bg rounded box-shadow p-5">
                <div class="row justify-content-between align-items-center mb-3" style="position: relative;">
                    
                    <div class="col-xs-6 mb-3 mt-2" style="z-index: 10; ">
                        <h3 class="mb-0 text-white">VERIFY EMAIL</h3>
                    </div>
                    <div class="col-xs-2 pt-1 pr-1 pb-0" style="z-index: 99;">
                        <img src="{{asset('img/logo/vmlogoblacktext.png')}}" class="img-fluid" style="height: 60px; width:auto; ">
                    </div>
                </div>
                <div class="top-register-img" style="background-image: url({{asset('img/top-01.png')}}); z-index:0;"></div>
                <div class="bot-register-img" style="background-image: url({{asset('img/bottom-01.png')}}); z-index:0;"></div>

                <div class="row mb-5 mt-5 pt-5 pb-5" style="min-height:200px">
                    {{$status}}
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
