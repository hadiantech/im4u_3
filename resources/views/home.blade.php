@extends('layouts.front.master') @section('content')

<!--    Main Slider -->
<div class="container-fluid p-0 mb-n-2">
    <div class="main-slider slick-fullscreen  border border-primary">
        <!--      repeat item      -->
        @foreach($banners as $banner)
        <a href="{{ $banner->link }}" target="_blank">
        <img class="item img-fluid" src="{{ url($banner->getMedia('banners')->first()->getUrl()) }}" />
        </a>
        @endforeach
        <!--      end: repeat item      -->
    </div>
</div>
<!-- end   -->

<!-- Main Banner Callout   -->
<div class="container-fluid white-bg p-lg-5 pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-4 pb-4 rounded box-shadow" style="background-image: url(img/front/becomevolunteernow_bg.png); background-repeat: no-repeat; background-size: cover">
                <div class="row pt-5 pb-5 align-items-center text-center text-lg-left">
                    <div class="col-sm-12 col-lg-7 offset-lg-1 white-text text-shadow">
                        <h1>BECOME A VOLUNTEER NOW!</h1>
                    </div>
                    <div class="col-sm-12 col-lg-3">
                        <a href="{{route('login')}}" class="btn btn-white">REGISTER HERE</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!--  vm goal   -->
<div class="container-fluid" style="background-image: url(img/front/goal_bg.jpg); background-repeat:no-repeat; background-size:cover; height:100%; background-position:center;">
    <div class="container">
        <div class="row goal pt-5 pb-5">
            <div class="col-md-5 mb-1">
                <img class="img-fluid" src="/img/logo/whitevmlogo.png" style="max-width:280px;" />
            </div>
            <div class="col-md-12">
<!--                <h4 class="white-text text-shadow">OUR GOAL</h4> -->
            </div>
            <div class="col-md-9">
                <p class="white-text text-shadow">Volunteer Malaysia is known as the catalyst that empowers and builds resilient youth with engaging, unconventional and inspiring activities. It is through these activities that Volunteer Malaysia aims to build sustainable and stronger community with economic impact
                    and foster greater leaders of tomorrow.</p>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!--    Recent Highlights   -->
<!--
<div class="container-fluid pt-5 pb-5 bg-white">
    <div class="container">
        <div class="row slick-navi align-items-center pt-4 pb-4">
            <div class="col-12 col-md-12 item pl-0 d-flex align-items-end ">
                <h1 class="blue-text mb-0">RECENT HIGHLIGHTS</h1>
                <a href="http://more.im4u.my" class="link blue-text ml-3">/ VIEW ALL HIGHLIGHTS <i class="fas fa-arrow-alt-circle-right"></i></a>
            </div>
            <div class="col-12 col-md-4 d-none d-md-block p-0">
                
            </div>
        </div>
        <div class="row">
            <div class="d-xs-none d-sm-none d-md-block slider-nav">

                @foreach($blogs as $blog)
                <div class="nav rouded" style="background-image:url({{ $blog->better_featured_image->media_details->sizes->medium->source_url }})"></div>
                @endforeach

            </div>
            <div class="slider-for mt-4" style="flex-direction: row !important">

                @foreach($blogs as $blog)
                <div class="d-md-flex flex-row">
                    <div class="col-sm-12 col-lg-6 mb-3 pl-0 pr-0">
                        <img class="img-fluid" src="{{ $blog->better_featured_image->media_details->sizes->medium_large->source_url }}" />
                    </div>
                    <div class="col-sm-12 col-lg-6 mb-3">
                        <p>By: {{ $blog->_embedded->author[0]->name }} | {{ Carbon\Carbon::parse($blog->date )->format('F j, Y') }}</p>
                        <h5 class="blue-text ">{{ $blog->title->rendered }}</h5>
                        <p class="dotdotdot-text-m">{!! $blog->excerpt->rendered !!}</p>
                        <div class="col-md-12">
                            <a target="_blank" href="{{ $blog->guid->rendered }}" class="btn btn-default btn-block">Read More</a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </div>

</div>
-->
<!-- end -->

<!--  success stories  -->
    <div id="stories" class="container-fluid p-0 pt-4 pb-4" style="background:#1488c8;">
        <div class="container pt-5 pb-4">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="text-white">OUR SUCCESS STORIES</h1>
                    <p class="text-white">We are proud to share some of the success stories that we’ve had throughout the years. Since it was first launched in 2012, Volunteer Malaysia has continued to inspire new communities to be actively involved in volunteer activities through our
                        successful and meaningful programs.</p>
                </div>
            </div>
    
            <div class="row">
                <div class="slick-item-success">
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/reachout2017/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#reachout2017">
                                <h3 class="white-text">Reach Out Convention & Celebration 2017</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Volunteer Malaysia Reach Out Convention & Celebration celebrated its 5th year milestone back in February when they were in Batu Pahat, Johor for the first round of Reach Out 2017. The program made its way through several other states in Malaysia until the 18th of March including Pahang, Kedah, Negeri Sembilan & Melaka.</p>    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#reachout2017" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/reachoutrun2016/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#reachoutrun2016">
                                <h3 class="white-text">Reach Out Run 2016</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Reach Out Run: Run For A Cause, Volunteer Malaysia’s charity run, returned for its second year in 2016. Close to 4,000 people took part in this year’s run that aimed to raise a total of RM80,000 for four charity organizations; Juara Turtle Project, Sabah Wetlands Conservation Society, Project WHEE! and Malaysian Rare Disorders Society. Each organization is expected to receive RM20,000.</p>    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#reachoutrun2016" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/vma2016/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#vma2016">
                                <h3 class="white-text">Volunteer Malaysia Awards 2016</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Volunteer Malaysia celebrates volunteerism with its first ever Volunteer Malaysia Awards, a Prime Minister’s Award ceremony held at Sunway Resort Hotel and Spa. This prestigious award ceremony sees various people from different backgrounds coming together to celebrate the outstanding achievements of those who have volunteered and contributed positively towards the betterment of our community.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#vma2016" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/vm2015/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#vm2015">
                                <h3 class="white-text">Volunteer Malaysia 2015</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">Volunteer Malaysia (VM) is an event that signifies the extraordinary effort made by Volunteer Malaysia to engage great participation in volunteerism. Held in the month of September every year, VM offers a platform and opportunity for youth to participate in a variety of the platform’s signature volunteer activities like Green Team, Public Art Movement, beach and underwater clean up and War on Dengue.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#vm2015" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/asean4u/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#asean4u">
                                <h3 class="white-text">ASEAN4U</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">ASEAN4U was Volunteer Malaysia’s (VM) first volunteer network program that saw VM hosting 70 delegates from Malaysia, Indonesia, Thailand, Cambodia, Laos, Myanmar, Brunei, the Philippines, Singapore and Vietnam from 29 October to 3 November 2015.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#asean4u" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 p-0 m-2 border border-secondary rounded box-shadow">
                        <div class="success-box" style="background-image: url(img/front/success/ihyaramadan/1.jpg); background-size: cover;"></div>
                        <div class="success-text p-4" style="background-color: #2A2A2A;">
                            <a href="" data-toggle="modal" data-target="#ihyaramadan">
                                <h3 class="white-text">IHYA Ramadan 2015</h3>
                            </a>
                            <p class="dotdotdot-text-s white-text">IHYA Ramadan is an interfaith program, initiated by Volunteer Malaysia in conjunction with the month of Ramadan. The program hopes to share the blessings of Ramadan with individuals from different backgrounds, through volunteer activities.</p>
    
                            <div class="row mb-2">
                                <a href="" data-toggle="modal" data-target="#ihyaramadan" class="futura-bold yellow-text">SEE DETAILS <i class="far fa-arrow-alt-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--  end  -->

    <!--  modal  -->
    <div class="modal fade" id="reachout2017" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">REACHING OUT TO NEW HEIGHTS</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>Volunteer Malaysia Reach Out Convention &amp; Celebration celebrated its 5th year milestone back in February when they were in&nbsp;<strong><em>Batu Pahat, Johor</em></strong>for the first round of Reach Out 2017. The program made its way&nbsp;through several other states in Malaysia until the 18th&nbsp;of March including&nbsp;<strong><em>Pahang, Kedah, Negeri Sembilan &amp; Melaka</em></strong>.</p>
                            <p>The convention, which aims to promote volunteerism to Malaysian youth through the sharing of experiences by a selected group of successful individuals featured big and inspiring names including&nbsp;<strong><em>Datuk Jake Abdullah, Tengku Dato' Sri Zafrul Aziz, Azran Osman Rani, Joe Flizzow, Raviraj Sawlani & Siti Aishah from Project TRY, Kavin Jay, Anita Yusof and AJ &lsquo;Pyro&rsquo; Lias Mansor</em></strong><em>.</em></p>
                            <p>The programme that took place in 5 different institutions -&nbsp;<strong><em>Universiti Tun Hussein Onn, Universiti Malaysia Pahang,&nbsp;Universiti Utara Malaysia,&nbsp;Politeknik Nilai,&nbsp;Politeknik Merlimau</em></strong>&nbsp;was hosted by iM4U fm&rsquo;s very own radio announcers;&nbsp;<strong><em>Jiggy, TV, Amrita, Fiza, Tyler and Ira</em>.</strong></p>
                            <p>Performances by&nbsp;<strong><em>Masdo, Pitahati, Ernie Zakri, Syamel, Oh Chentaku, Harris Baba, Awi Rafael&nbsp;</em></strong>were the highlight of the tour as the crowd got to sit back and chill after a day of knowledge and stimulation<strong><em>.</em></strong>&nbsp;</p>
                            <p>All in all, we're glad that this year's event was a success and that everyone who came had a good time and an unforgettable experience. We also hope that this event will inspire even more youth to go out there and be inspired by those who have made it before them.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=radkdwckaZ8" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=nwwoABX26hk" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=4GvQWp_zWl4" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=ePqdfYhXQAA" target="_blank">Video</a></p>
                            <p><a href="https://www.youtube.com/watch?v=4x4MVsS9pwo" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachout2017/4.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="reachoutrun2016" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">REACH OUT RUN 2016</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                                <p>Reach Out Run: Run For A Cause, Volunteer Malaysia&rsquo;s charity run, returned for its second year in 2016. Close to 4,000 people took part in this year&rsquo;s run that aimed to raise a total of RM80,000 for four charity organizations; Juara Turtle Project, Sabah Wetlands Conservation Society, Project WHEE! and Malaysian Rare Disorders Society. Each organization is expected to receive RM20,000.</p>
                                <p>The run, which was first introduced in 2015, aims to inculcate a more balanced and healthier life among Malaysians while helping the four organizations that champion the different causes. Besides that, Volunteer Malaysia also hopes to promote unity through the run.</p>
                                <p>This year&rsquo;s run was divided into four different categories: 15km (men's open &amp; women's open), 10km (men's open &amp; women's open), 10km (college students &amp; university students) and 5km Fun Run.</p>
                                <p>The top three winners for the 15km category men and women open received cash prizes of RM2,000, RM1,500 and RM1,000 while those of the 10km categories took home cash prizes of RM1,500, RM1,000 and RM500 respectively. The winners from the 15km and 10km categories also pledged 10 per cent of their winnings to one of the four selected organizations. The winners for the college and university category walked away with RM3,000, RM2,000 and RM1,000 from DRe1m Fund to implement their initiatives for the community. There was no cash prize for the 5km Fun Run category.</p>
                                <p>2016 also saw the participation of local celebrities; Siti Saleha, Pushpa Narayan, Faizdickie and Sean Lee as Reach Out Run ambassadors. They were joined by iM4U fm announcers; Jiggy, Tyler, Fiza and TV at the event that was held at Dataran Putrajaya. Besides taking part in the run, the ambassadors also participated in &lsquo;Reach Out Run Volunteer Activities&rsquo;, a volunteer program that was held in conjunction with Reach Out Run 2016.</p>
                                <p>The ambassadors took part in volunteer activities for the four participating organizations. Fiza and Pushpa Narayan took part in the Juara Turtle Project that was held in Pulau Tioman where volunteers had the opportunity to participate in night surveys (beach & boat patrol), monitor nesting sea turtles, and help with local environmental education, PAM & beach clean up. Sabah Wetlands Conservation Society on the other hand saw volunteers working together with Siti Saleha and Tyler to carry out mangrove plant seedings, tree plantings and clean up in an effort to preserve and rehabilitate Malaysia&rsquo;s natural wetlands in Sabah. Project WHEE! brought Faizdickie and Jiggy as well as the volunteers to Bario, Sarawak to help convert an old hostel into a resource centre for the local students.  The team got to work with the local Kelabit community there while experiencing the wonderful culture of the Bario Highlands. Sean Lee and TV had the opportunity to explore the Family City Farm in Seri Kembangan with the children from the Malaysian Rare Disorder Society (MRDS).</p>
                                <p>To sum it all up, Reach Out Run 2016 was indeed a spectacular event. Thank you all who participated and we&rsquo;ll see you again soon!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=S8WHRPvS0zc" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/reachoutrun2016/4.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vma2016" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vma2016/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">VOLUNTEER MALAYSIA AWARDS 2016</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                                <p>Volunteer Malaysia celebrates volunteerism with its first ever Volunteer Malaysia Awards, a Prime Minister&rsquo;s Award ceremony held at Sunway Resort Hotel and Spa. This prestigious award ceremony sees various people from different backgrounds coming together to celebrate the outstanding achievements of those who have volunteered and contributed positively towards the betterment of our community.</p>
                                <p>There were 13 awards presented on the night, which include for Best Volunteer Initiative (Private Sector, Public Sector, Media Organisation, NGO/NPO, School, DRe1m Fund, Personality and People&rsquo;s Choice), Lifetime Achievement Award, Superelawan of the Year, Best Social Media Campaign for a Volunteer Initiative, Special Jury Award and iM4U fm Award for Best Humanitarian Song.</p>
                                <p>Submission of entries was open from 1<sup>st</sup>&nbsp;September 2016 until 30<sup>th</sup>&nbsp;October 2016, where individuals and organizations from around the country obtained the opportunity to submit their best volunteer initiatives. The nominations and winners were selected by a panel of notable jury that consists of prominent individuals from various relevant industries.</p>
                                <p>Besides those awards, Volunteer Malaysia volunteers from all walks of life who have volunteered a certain number of hours in a year were also recognized with Volunteer Malaysia Awards for Bronze, Silver and Gold Achievements.</p>
                                <p>Acknowledged and officiated by the Prime Minister of Malaysia, the Volunteer Malaysia Awards has been conceptualized to celebrate, honour and pay tribute to individuals and groups of volunteers who have inspired and promoted volunteerism via their remarkable contributions. This award ceremony highlights the best practices executed at minimum costs with optimum positive impact that is sustainable and long term. They must be able to reach out to a wide spread of people and benefit the intended community. This is a way of recognizing and rewarding the volunteers for their undivided time and effort, and to encourage people of all ages to volunteer and make a difference in their community.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vma2016/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vma2016/3.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="vm2015" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">VOLUNTEER MALAYSIA 2015</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>Volunteer Malaysia (VM) is an event that signifies the extraordinary effort made by Volunteer Malaysia to engage great participation in volunteerism. Held in the month of September every year, VM offers a platform and opportunity for youth to participate in a variety of platform’s signature volunteer activities like Green Team, Public Art Movement, beach and underwater clean up and War on Dengue.<br /><br />VM is one of Volunteer Malaysia&rsquo;s signature programs that have successfully brought together volunteers from a cross section of community of different backgrounds and interests since its inception in 2014. These are individuals who have volunteered their time and effort for a noble cause.</p>
                            <p>The significant increase in the number of volunteers and the number of hours spent on the activities show that VM has successfully garnered the support and interest of fellow Malaysians.<br /><br />Another highlight from VM 2015 was the introduction of underwater clean up, where passionate divers came together to help clear up marine waste.<br /><br />VM 2015 also witnessed another milestone with the participation of volunteers in Korea, France and Australia, through Volunteer Malaysia international Outreach Centres. The activities were carried out simultaneously in those countries on 12 September 2015.<br /><br />One of the winning factors for VM is the support and participation from various government ministries that helped to facilitate the arrangement and access in certain areas. These included the Ministry of Tourism and Culture, Ministry of Higher Education, Ministry of Health, Ministry of Urban Wellbeing, Housing and Local Authority and NBOS.<br /><br />Support and participation also came from corporations like Astro that had signed up to volunteer at VM as part of their CSR initiative. VM is definitely the program that helps to bring change to the community. Be part of VM to inspire others!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=BBr9xKvfoMQ" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/vm2015/4.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="asean4u" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">ASEAN4U</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>ASEAN4U was Volunteer Malaysia’s (VM) first volunteer network program that saw VM hosting 70 delegates from Malaysia, Indonesia, Thailand, Cambodia, Laos, Myanmar, Brunei, the Philippines, Singapore and Vietnam in 2015.<br /><br />This program was initiated by Volunteer Malaysia to create a platform that fostered and strengthened the spirit of volunteerism among the ASEAN communities. The participants had the opportunity to share their experience and impart their knowledge to their peers. Besides that, they also gained hands-on experience of carrying out volunteer activities in Malaysia.<br /><br />It started off with a leadership training conducted by Michael Teoh from Thriving Talents and a talk by Jonathan Yabut, the winner of &lsquo;The Apprentice Asia&rsquo; on 29th and 30th October. On the third day, they took part in &lsquo;Eco Basket&rsquo;, an activity where they weaved baskets to raise funds for single parents. In the evening, they cooked and distributed food for 500 homeless in 10 locations around Kuala Lumpur.<br /><br />The following day, the delegates worked hand in hand with Volunteer Malaysia volunteers in &lsquo;Ecocentric Transitions Workshop&rsquo;, where they helped to restore nature by planting trees in Taman Tun Dr. Ismail. They visited an orphanage on 2nd November where they repainted and beautified the premise.</p>
                            <p>The program ended on a high note when the delegates had the chance to meet a Minister in the Prime Minister’s Department, YB Senator Datuk Seri Abdul Wahid Omar in Putrajaya on 3rd November. They also received their certificate of participation from the Senator who is also Volunteer Malaysia&rsquo;s mentor.<br /><br />ASEAN4U is truly an inspiring program that has torn down all barriers in the aim to inculcate volunteerism.</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=Ag3DMYMIoRo" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/4.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/asean4u/5.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ihyaramadan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="position: relative;">
                <div class="modal-header leader-modal-header white-bg pb-0 px-4">
                    <div class="row" style="position: absolute; right: 20px">
                        <button type="button" class="close blue-text" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="row no-gutters justify-content-center align-items-center">
                        <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/1.jpg" class="img-fluid" /></div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 mb-4 mt-3">
                            <h2 class="blue-text">IHYA Ramadan 2015</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <p>iHYA Ramadan is an interfaith program, initiated by Volunteer Malaysia in conjunction with the month of Ramadan. The program hopes to share the blessings of Ramadan with individuals from different backgrounds, through volunteer activities.<br /><br />Six meaningful activities were carried out under iHYA Ramadan 2015 :<br /><br />1. Perkampungan Orang Asli &ndash; A get-together for converts from the &lsquo;Orang Asli&rsquo; community to welcome the Ramadan.<br /><br />2. Beauty of Ramadan with Ustaz Nouman Ali Khan &ndash; A religious discourse conducted by Nouman Ali Khan, a well-known Islamic preacher and the founder of The Bayyinah Institute for Arabic and Qur&rsquo;anic Studies from USA.<br /><br />3. iHYA Ramadan &ndash; Fabrik Kasih &ndash; A collaboration with GIATMARA Wilayah Persekutuan Kuala Lumpur to prepare baju Raya for the underprivileged children.<br /><br />4. iQRA&rsquo; &ndash; A nationwide initiative where 100 volunteers from Volunteer Malaysia Outreach Centres in each state across the country, gathered and recited the Al-Quran together.<br /><br />5. Ramadan Rangers &ndash; Volunteers helped to distribute iftar packs to those were on-duty in the hours of berbuka at 70 locations.<br /><br />6. We look forward to seeing you at this year&rsquo;s iHYA Ramadan!</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-4">
                            <h4 class="blue-text mb-3">LINKS</h4>
                            <p><a href="https://www.youtube.com/watch?v=CjYwxOxXT0s" target="_blank">Video</a></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3 mt-3">
                            <h4 class="blue-text">PHOTO GALLERY</h4>
                        </div>
                        <div class="col-sm-12 col-lg-12 gray-bg mb-3 pb-3">
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/2.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/3.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/4.jpg" class="img-fluid" /></div>
                            <div class="col-sm-12 col-lg-12 p-0 mt-4"><img src="img/front/success/ihyaramadan/5.jpg" class="img-fluid" /></div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- end modal -->

<!--  apply dremfund/create project  -->
<div class="container-fluid white-bg">
    <div class="container">
        <div class="row pt-5 pb-5 justify-content-center">
            <div class="col-lg-6 col-sm-4 col-md-4 justify-content-center rounded pb-5" style="background-image: url(img/front/1.jpg); background-repeat: no-repeat; background-size: cover; background-position: center; min-height:100%;">
 <!--           <img class="img-fluid" src="img/front/dre1m_icon.png" />
                <h4 class="white-text">APPLY FOR DRe1M FUND</h4>
                <p class="white-text">The Dana Sukarelawan 1Malaysia (DRe1M Fund) provides seed funding support for youth to carry out volunteer activities that are aimed at helping targeted communities and social groups.</p>
                <a href="javascript:;" class="btn btn-white-o btn-block"> APPLY FOR DRe1M FUND</a> -->
            </div>
            <div class="col-lg-6 justify-content-center blue3-bg rounded p-5">
                <img class="img-fluid offset-lg-2" src="img/front/project_icon.png" />
                <h4 class="white-text text-center">CREATE VOLUNTEERING PROJECT</h4>
                <p class="white-text text-center">Register with us and create volunteering activities. Invite volunteers from all over Malaysia to participate in your volunteering event!</p>
                <a href="{{route('login')}}" class="btn btn-white-o btn-block"> CREATE A PROJECT</a>
            </div>
        </div>
    </div>
</div>
<!-- end -->

<!--  Upcoming Projects   -->
<!--
<div class="container-fluid bg-white pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="row slick-navi align-items-center">
                <div class="col-12 col-md-6 item pt-4 pb-4">
                    <h1 class="blue-text">UPCOMING PROJECTS</h1>
                </div>
                <div class="col-12 col-md-4 d-none d-md-block p-0 pt-4 pl-0 pb-4">
                    <a href="{{ route('explore') }}" class="link blue-text">/ VIEW ALL PROJECTS <i class="fas fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="slick-item">
                @forelse($projects as $project)
                <div class="col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-1 m-2 box-shadow">
                    <img class="img-fluid" src="{{ $project->getMedia('thumbnail')->first()->getUrl() }}" />
                    <div class="blue-bg col-12 p-3"><i class="white-text fas fa-calendar-alt"></i>
                        <span class="white-text opensans p-1" style="font-size:0.9em;"> {{ Carbon\Carbon::parse($project->started_at)->format('F j, Y') }} - {{ Carbon\Carbon::parse($project->ended_at)->format('F
                                j, Y') }}</span>
                    </div>
                    <h5 class="pt-3 dotdotdot-title p-3">{{ $project->title }}</h5>
                    <div class="dotdotdot-text p-3">
                        <p>{{ $project->tagline }}</p>
                    </div>
                    <a href="{{ route('projects.show', $project->id) }}" class="btn btn-green mb-3"> REGISTER NOW</a>
                </div>
                @empty
                    <p>No projects</p>
                @endforelse

            </div>
        </div>

    </div>
</div>
                        -->
<!-- end -->


<!-- subscribe  -->
<div class="container-fluid blue-bg p-5">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-3 col-lg-2 col-sm-3"><img class="img-fluid" src="img/front/subscribe%20_icon.png" /></div>
            <div class="col-md-12 col-lg-6 pt-4 align-self-center">
                <h3 class="white-text">SUBSCRIBE TO OUR NEWSLETTER</h3>
                <p class="white-text">To get the latest news and updates about volunteerism.</p>
                <form data-toggle="validator" role="form">
                    <div class="form-inline">
                        <div class="form-group col-md-12 col-lg-6 p-0">
                            <input type="email" class="form-control col-md-12 col-sm-12 mb-sm-2 mb-md-0" placeholder="email" required>
                        </div>
                        <button type="submit" class="btn btn-green btn-med m-md-3 col-lg-3 btn-block">Subscribe</button>             
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- end   -->

<!-- social icons  -->
<div class="container-fluid white-bg p-5">
    <div class="container">
        <div class="row mb-4 mt-4">
            <div class="col-md-3 p-5 d-flex flex-wrap justify-content-center">
                <a href="https://www.facebook.com/volunteermy/" target="_blank"><button type="button" class="btn btn-white btn-lg" style="color:#3B5998"><i class="fab fa-facebook-f fa-5x"></i></button></a>
            </div>
            <div class="col-md-3 p-5 d-flex flex-wrap justify-content-center">
                <a href="https://twitter.com/volunteermy" target="_blank"><button type="button" class="btn btn-white btn-lg" style="color:#1DA1F2"><i class="fab fa-twitter fa-5x"></i></button></a>
            </div>
            <div class="col-md-3 p-5 d-flex flex-wrap justify-content-center">
                <a href="https://www.instagram.com/volunteermalaysia/" target="_blank"><button type="button" class="btn btn-white btn-lg" style="color:#bc2a8d"><i class="fab fa-instagram fa-5x"></i></button></a>
            </div>
            <div class="col-md-3 p-5 d-flex flex-wrap justify-content-center">
                <a href="https://www.youtube.com/channel/UCdzKT8BJn225_0UdGt1NaeA" target="_blank">    <button type="button" class="btn btn-white btn-lg" style="color:#ff0000"><i class="fab fa-youtube fa-5x"></i></button></a>
            </div>
    </div>
</div>
</div>
<!-- end   -->

<div class="container-fluid p-0 map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3984.2733038968477!2d101.6091663145158!3d3.0210839977998756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31cdb3596b01f5d3%3A0x4835b0b0f06b940a!2siM4U+Sentral!5e0!3m2!1sen!2smy!4v1500871349358"
        width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen=""></iframe>
</div>

@endsection 

@section('js') 
@parent 

<!-- Sweet-Alert  -->
<script src="{!! asset('js/sweetalert/sweetalert.min.js') !!}"></script>


<script>
$('.slider-for-video').on('init', function(event, slick){
    //init code goes here
}).on('afterChange',function(e,o){
    //on change slide = do action
    $('iframe').each(function(){
        $(this)[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');    
    });
}).slick();



</script>



@endsection