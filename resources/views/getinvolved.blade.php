@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/dremfund/dre1m_header.jpg)">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">GET INVOLVED</h1>
            <h5 class="white-text text-shadow">Join our cause to do something good.</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  why volunteer  -->
<div id="whyvolunteer" class="container-fluid p-0 blue-bg pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 pt-5">
                <h1 class="white-text">WHY VOLUNTEER WITH US?</h1>
                <p class="white-text pt-3">People choose to volunteer for a variety of reasons. For some it offers the chance to give something back to the community or make a difference to the people around them. For others it provides an opportunity to develop new skills or build on existing experience and knowledge. Regardless of the motivation, what unites them all is that they find it both challenging and rewarding. Our volunteer initiatives are centered on our
                    four pillars:</p>
            </div>
        </div>
        <div class="row mt-4 pb-5">
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/arts-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Arts, Culture & Innovation</h4>
                            <p class="blue-text">We promote Malaysian culture and diversity, encouraging communities to pursue their interests in arts and culture which will drive innovation.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/community-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Community Well-Being</h4>
                            <p class="blue-text">We promote inclusiveness and embrace harmony by reaching out to communities in need to build a better living environment.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/sport-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Sports</h4>
                            <br/>
                            <p class="blue-text">We promote physical and mental well-being while fostering team spirit to build a healthier and united nation.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                <div class="drem-box rounded border box-shadow white-bg pb-4 pt-3 hover-zoom">
                    <div class="row">
                        <div class="col-sm-4 offset-sm-4 col-lg-7 offset-lg-2">
                            <img src="img/dremfund/knowledge-01.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center">
                            <h4 class="red-text mb-4">Knowledge Building</h4>
                            <br/>
                            <p class="blue-text">We engage in learning and educational activities to enhance knowledge and skills of the local communities.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!-- how to be   -->
<div id="howtobe" class="container-fluid white-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="blue-text">HOW TO BE A VOLUNTEER</h1>
            </div>
        </div>
        <div class="row align-items-center mb-3">
            <div class="col-sm-12 col-md-8 pt-3 blue-text">
                <p>Volunteering and helping others can help you maintain a healthy lifestyle, expand your network, keep you mentally stimulated, and provide a sense of purpose. While it’s true that the more you volunteer, the more benefits you’ll experience, volunteering doesn’t have to involve a long-term commitment or take a huge amount of time out of your busy day. Simple ways of giving can help others or those in need and contribute towards creating a postive impact to our communitties.</p>
                <p>Come join us today and be a part of our initiatives and worthwhile causes to benefit the people in need, the community and even greater benefits for you, the volunteer.</p>
            </div>
            <div class="col-sm-4 col-md-3 d-sm-none d-md-block">
                <img src="img/logo/vmlogoblacktext.png" class="img-fluid" />
            </div>
        </div>
        <div class="row pb-3">
            <div class="col-sm-12">
                <a href="{{route('login')}}" class="btn btn-red">Register Now</a>
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!-- benefit   -->
<div id="benefit" class="container-fluid bluesea-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="blue-text">BENEFITS OF VOLUNTEERING</h1>
            </div>
        </div>
        <div class="row align-items-center mb-3 pt-3">
            <div class="col-sm-4 col-md-12 drem-box rounded border white-bg pt-5">
                <img src="img/getinvolved/benefit.png" class="img-fluid offset-md-1" />
            </div>
        </div>
    </div>
</div>
<!--  end  -->


@endsection @section('js') @parent @endsection