@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/media/newbackground.jpg)">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">MEDIA CENTRE</h1>
            <h5 class="white-text text-shadow">Media collection throughout the years. Check out the stories.</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  press release  -->
<div id="press" class="container-fluid p-0 red-bg pt-5 pb-5">
    <div class="container">
        <div class="row ">
            <div class="col-sm-12 d-flex justify-content-between align-items-center pb-4 pt-4">
                <h1 class="white-text">PRESS RELEASE HIGHLIGHTS</h1>
                <a href="{{route('media.press')}}" class="link white-text futura-bold">VIEW ALL PRESS RELEASES <i class="fas fa-arrow-alt-circle-right"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="slick-press">

                @foreach ($presses as $press)
                    <div class="col-sm-12 col-md-12 d-flex flex-wrap align-items-center justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                        
                        @if(empty($press->getMedia('press')->first()))
                        <img class="img-fluid" src="{{ url('img/no_image.png') }}" />
                        @else
                        <img class="img-fluid" src="{{ $press->getMedia('press')->first()->getUrl() }}" />
                        @endif
                        
                        <div class="col-md-12 col-sm-12 text-center text-truncate">
                            <h5 class="pt-4 p-3 text-truncate font-weight-bold">{{ $press->title }}</h5>
                        </div>
                        <div class="row justify-content-center">
                            <a href="{{ $press->link }}" target="_blank" class="btn btn-red mb-3">DOWNLOAD</a>
                        </div>
                    </div>
                @endforeach
                
            </div>
        </div>
    </div>
</div>
<!--  end  -->

<!-- factsheet   -->
<div id="factsheet" class="container-fluid white-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="blue-text">FACTSHEET</h1>
            </div>
        </div>
    <div class="col-sm-12 col-md-12 pt-3">
        <div class="row align-items-center mb-3 pt-3">
            <div class="col-sm-12 col-md-12 drem-box rounded border white-bg p-5" style="min-height: 150px">
                <h4 class="blue-text pb-3">ABOUT VOLUNTEER MALAYSIA</h4>
                <h4 class="blue-text pb-3 futura-book">Proudly carrying the good work of Malaysian youth volunteers since 2012, Volunteer Malaysia continues to lead the nation’s volunteer development.</h4>
                <a class="btn btn-red btn-lg" href="/vm_factsheet.pdf" download="Volunteer Malaysia Factsheet"><i class="fa fa-download" style="font-size: 18px; margin-right: 10px"></i>DOWNLOAD OUR FACTSHEET HERE</a>
            </div>
        </div>   
    </div>

    </div>
</div>
<!--  end  -->

<!-- faq   -->
<div id="faq" class="container-fluid bluesea-bg pt-5 pb-5">
    <div class="container pt-4 pb-4">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="blue-text">FREQUENTLY ASKED QUESTIONS</h1>
            </div>
        </div>
        <div class="row d-flex pt-4">
            <div class="col-sm-12 col-lg-6">
                <div class="d-flex rounded p-4" style="background-color: #27ae60">
                    <div class="col-sm-12 d-flex flex-column justify-content-center">
                        <h5 class="white-text pb-2" style="font-weight: 600">Who are we?</h5>
                        <p class="white-text">We are the catalyst for volunteerism.</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6">
                    <div class="d-flex rounded p-4" style="background-color: #2980b9">
                        <div class="col-sm-12 d-flex flex-column justify-content-center">
                            <h5 class="white-text pb-2" style="font-weight: 600">What do we do?</h5>
                            <p class="white-text">We help inculcate and spread the spirit of volunteerism.</p>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row d-flex pt-4">
                <div class="col-sm-12 col-lg-6">
                    <div class="d-flex rounded p-4" style="background-color: #e74c3c">
                        <div class="col-sm-12 d-flex flex-column justify-content-center">
                            <h5 class="white-text pb-2" style="font-weight: 600">Why do we do it?</h5>
                            <p class="white-text">We do it to instill the spirit of caring for others.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-6">
                        <div class="d-flex rounded p-4" style="background-color: #f39c12">
                            <div class="col-sm-12 d-flex flex-column justify-content-center">
                                <h5 class="white-text pb-2" style="font-weight: 600">What is our tone and manner?</h5>
                                <p class="white-text">We are unconventional, unaffected and up to date.</p>
                            </div>
                        </div>
                </div>
            </div>
            <div class="row d-flex pt-4">
                    <div class="col-sm-12 col-lg-6">
                        <div class="d-flex rounded p-4" style="background-color: #34495e">
                            <div class="col-sm-12 d-flex flex-column justify-content-center">
                                <h5 class="white-text pb-2" style="font-weight: 600">How do we get you to volunteer?</h5>
                                <p class="white-text">By producing great volunteering activities that are engaging and enlightening.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                            <div class="d-flex rounded p-4" style="background-color: #9b59b6">
                                <div class="col-sm-12 d-flex flex-column justify-content-center">
                                    <h5 class="white-text pb-2" style="font-weight: 600">What is our end goal?</h5>
                                    <p class="white-text">To build a caring society that will give rise to leaders of tomorrow.</p>
                                </div>
                            </div>
                    </div>
                </div>
    </div>
</div>
<!--  end  -->



<!-- photo/video -->
<!--
<div class="container-fluid p-0">
    <div class="row">
        <div class="media-box col-sm-12 col-md-6 p-0" style="position: relative; overflow: hidden;">
            <div class="" style="background-image: url(img/media/photo.jpg); background-size: cover; position: absolute; width: 100%; height: 100%; overflow: hidden;"></div>
            <div class="row p-5">
                <div class="col-sm-12">
                    <h2 class="white-text text-shadow">PHOTO ARCHIVE</h2>
                    <p class="white-text text-shadow">Pictures collection throughout the years, precious moments captured through our lens.</p>
                    <a href="javascript:;" class="btn btn-white btn-med">View All</a>
                </div>
            </div>
        </div>
        <div class="media-box col-sm-12 col-md-6 p-0" style="position: relative; overflow: hidden;">
            <div class="" style="background-image: url(img/media/videoarchive.jpg); background-size: cover; position: absolute; width: 100%; height: 100%; overflow: hidden;"></div>
            <div class="row p-5">
                <div class="col-sm-12">
                    <h2 class="white-text text-shadow">VIDEO ARCHIVE</h2>
                    <p class="white-text text-shadow">Videos collection throughout the years, precious moments captured through our lens.</p>
                    <a href="javascript:;" class="btn btn-white btn-med">View All</a>
                </div>
            </div>
        </div>
    </div>
</div>
-->
<!--  end  -->

@endsection @section('js') @parent @endsection