@extends('layouts.admin.master') @section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1 class="">USER PROJECT</h1>
            <h3 class="futura-heavy">[username]</h3>
        </div>
    </div>
    <div class="row no-gutters mb-4">
        <div class="card col-sm-12 col-md-6 col-lg-3">
            <div class="card-body d-flex flex-column justify-content-between">
                <h1>10</h1>
                <p>Project Joined</p>
                <a href="vms_user_project_joined.html" class="btn btn-blue btn-sm" style="font-size: 0.7em">View all <i class="far fa-arrow-alt-circle-right ml-1"></i></a>
            </div>
        </div>
        <div class="card col-sm-12 col-md-6 col-lg-3">
            <div class="card-body d-flex flex-column justify-content-between">
                <h1>2</h1>
                <p>Project Created </p>
                <a href="{{route('admin.userproject.created')}}" class="btn btn-blue btn-sm" style="font-size: 0.7em">View all <i class="far fa-arrow-alt-circle-right ml-1"></i></a>
            </div>
        </div>
        <div class="card col-sm-12 col-md-6 col-lg-3">
            <div class="card-body d-flex flex-column justify-content-between">
                <h1>0</h1>
                <p>Project Suspended</p>
                <a href="" class="btn btn-blue btn-sm" style="font-size: 0.7em">View all <i class="far fa-arrow-alt-circle-right ml-1"></i></a>
            </div>
        </div>
        <div class="card col-sm-12 col-md-6 col-lg-3">
            <div class="card-body d-flex flex-column justify-content-between">
                <h1>0</h1>
                <p>Project Rejected</p>
                <a href="" class="btn btn-blue btn-sm" style="font-size: 0.7em">View all <i class="far fa-arrow-alt-circle-right ml-1"></i></a>
            </div>
        </div>

    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                <i class="fa fa-fw fa-clipboard-check"></i> PROJECT CREATED
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-sm-12 mt-4 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" style="min-width: 100px">Project Name</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Organisation</th>
                                    <th scope="col">Created By</th>
                                    <th scope="col">Start-End Date</th>
                                    <th scope="col">Participation</th>
                                    <th scope="col">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td><a href="">CAT Walk 2: Protect Tiger and their habitat in Pahang (June 2018)</a></td>
                                    <td>Volunteer</td>
                                    <td>MYCAT</td>
                                    <td><a href="">MYCAT</a></td>
                                    <td>30/3/2018 - 1/4/2018</td>
                                    <td><a href="">2/10</a></td>
                                    <td>
                                        <a class="btn btn-warning btn-sm dropdown-toggle px-3 py-1" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size:0.7em;">
                                            Action
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">View page</a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <a href="{{route('admin.userproject.created')}}" class="btn btn-blue">View all <i class="far fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="card col-sm-12 mt-4">
            <div class="card-header bold">
                <i class="fa fa-fw fa-clipboard-check"></i> PROJECT JOINED
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-sm-12 mt-4 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" style="min-width: 100px">Project Name</th>
                                    <th scope="col">Type</th>
                                    <th scope="col">Organisation</th>
                                    <th scope="col">Created By</th>
                                    <th scope="col">Start-End Date</th>
                                    <th scope="col">Participation</th>
                                    <th scope="col">Action</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td><a href="">CAT Walk 2: Protect Tiger and their habitat in Pahang (June 2018)</a></td>
                                    <td>Volunteer</td>
                                    <td>MYCAT</td>
                                    <td><a href="">MYCAT</a></td>
                                    <td>30/3/2018 - 1/4/2018</td>
                                    <td><a href="">2/10</a></td>
                                    <td>
                                        <a class="btn btn-warning btn-sm dropdown-toggle px-3 py-1" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size:0.7em;">
                                                        Action
                                                    </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="#">View page</a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <a href="vms_user_project_joined.html" class="btn btn-blue">View all <i class="far fa-arrow-alt-circle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('js') @parent @endsection