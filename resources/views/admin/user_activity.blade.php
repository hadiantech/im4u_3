@extends('layouts.admin.master') @section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1 class="">USER ACTIVITY</h1>
            <h3 class="futura-heavy">List</h3>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                <i class="fa fa-fw fa-clipboard-check"></i> ACTIVITY LIST
            </div>
            <div class="card-body">
                <div class="row">
                    <p class="futura-heavy">List of activities by [username]</p>
                </div>
                <div class="row no-gutters">
                    <div class="col-sm-12 mt-4 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col" style="width: 10%">Time</th>
                                    <th scope="col" style="width: 50%">Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>2018-03-03 02:30:00</td>
                                    <td>
                                        <p class="dotdotdot-text">OttoNot a word, not a word, not a word now. Quiet, uh, donations, you want me to make a donation to the coast guard youth auxiliary? (he pulls the suction cup off his forehead) Doc, I'm from the future. I came here
                                            in a time machine that you invented.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2018-03-03 02:30:00</td>
                                    <td>
                                        <p class="dotdotdot-text">you were the only one who knows how your time machine works. Time machine, I haven't invented any time machine.</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2018-03-03 02:30:00</td>
                                    <td>
                                        <p class="dotdotdot-text">Now, I need your help to get back to the year 1985. (grabs Marty's shoulders) My god, do you know what this means? It means that this damn thing doesn't work at all. (takes off the contraption.) Doc, you gotta help
                                            me.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Pagination -->
                <div class="row">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('js') @parent @endsection