@extends('layouts.admin.master') @section('content')
<!-- Volunteer Stats -->
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1 class="">USER MANAGEMENT</h1>
            <h3 class="futura-heavy">List</h3>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                USER
            </div>
            <div class="card-body">
                <div class="row">
                    <a href="{{route('admin.usercreate')}}" class="btn btn-green btn-med">ADD NEW <i class="fas fa-plus ml-1"></i></a>
                </div>

                <div class="row no-gutters mt-4">
                    <div class="col-sm-12 form-inline">
                        <div class="form-group mr-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Search..." aria-label="">
                            </div>

                        </div>
                        <div class="form-group">
                            <a class="btn btn-warning btn-sm" href="#" style="font-size: 0.7em">Search</a>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-sm-12 mt-4 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Profession</th>
                                    <th scope="col">Location</th>
                                    <th scope="col">Hours</th>
                                    <th scope="col"><i class="far fa-edit"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td><a href="vms_user_edit.html">Muhammad Muhaimin bin Juhari</a></td>
                                    <td>muhaiminjuhari@gmail.com</td>
                                    <td>System Analyst</td>
                                    <td>Wilayah Persekutuan Kuala Lumpur</td>
                                    <td>00.00</td>
                                    <td>
                                        <a class="btn btn-warning btn-sm dropdown-toggle px-3 py-1" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size:0.7em;">
                                            <i class="fas fa-cogs"></i>
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{route('admin.userupdate')}}">View</a>
                                            <a class="dropdown-item" href="#">Remove</a>

                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- Pagination -->
                <div class="row">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- end -->

@endsection @section('js') @parent @endsection