@extends('layouts.admin.master') @section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1 class="">USER MANAGEMENT</h1>
            <h3 class="futura-heavy">Create</h3>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                <i class="fas fa-user-circle"></i> User Information
            </div>
            <div class="card-body">
                <div class="form-group border p-4 mx-3 rounded">
                    <p class="futura-book mb-2">Please select user role</p>
                    <div class="row">
                        <div class="custom-control custom-checkbox m-2">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
                            <label class="custom-control-label" for="customCheck1">Volunteer</label>
                        </div>
                        <div class="custom-control custom-checkbox m-2">
                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                            <label class="custom-control-label" for="customCheck2">Project Manager</label>
                        </div>
                        <div class="custom-control custom-checkbox m-2">
                            <input type="checkbox" class="custom-control-input" id="customCheck3">
                            <label class="custom-control-label" for="customCheck3">Volunteer Manager</label>
                        </div>
                        <div class="custom-control custom-checkbox m-2">
                            <input type="checkbox" class="custom-control-input" id="customCheck4">
                            <label class="custom-control-label" for="customCheck4">Administrator</label>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-sm-12">
                        <form class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" id="" aria-describedby="" placeholder="FULL NAME">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="EMAIL">
                            </div>
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-sm-4">
                                        <select class="form-control" id="exampleFormControlSelect1">
                                                          <option>IC/MYKAD</option>
                                                          <option>PASSPORT NUMBER</option>
                                                    </select>
                                    </div>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="IC NUMBER">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                                  <option>MARITAL STATUS</option>
                                                  <option>SINGLE</option>
                                                  <option>MARRIED</option>
                                                  <option>DIVORCED</option>
                                            </select>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="MOBILE NUMBER">
                            </div>
                            <div class="form-group">
                                <select class="form-control" id="exampleFormControlSelect1">
                                                  <option>STATE</option>
                                                  <option>WILAYAH PERSEKUTUAN KUALA LUMPUR</option>
                                                  <option>WILAYAH PERSEKUTUAN PUTRAJAYA</option>
                                                  <option>SELANGOR</option>
                                                  <option>KEDAH</option>
                                                  <option>KELANTAN</option>
                                                  <option>MELAKA</option>
                                                  <option>NEGERI SEMBILAN</option>
                                                  <option>PAHANG</option>
                                                  <option>PENANG</option>
                                                  <option>PERAK</option>
                                                  <option>PERLIS</option>
                                                  <option>SABAH</option>
                                                  <option>SARAWAK</option>
                                                  <option>TERENGGANU</option>
                                                  <option>WILAYAH PERSEKUTUAN LABUAN</option>
                                            </select>
                            </div>
                            <div class="form-group border rounded p-4">
                                <p class="futura-book mb-2">Date of birth</p>
                                <div class="form-row">
                                    <div class="col-sm-2">
                                        <label>Day</label>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                                          <option>1</option>
                                                    </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Month</label>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                                          <option>1</option>
                                                    </select>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>Year</label>
                                        <select class="form-control" id="exampleFormControlSelect1">
                                                          <option>1930</option>
                                                    </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group border rounded p-4">
                                <p class="futura-book mb-2">Select Profession</p>
                                <div class="row">
                                    <select class="form-control js-example-basic-single" name="state">
                                                    <option value="">Web Developer</option>
                                                    <option value="">Front-end Developer</option>
                                                    <option value="">Back-end Developer</option>
                                                    <option value="">Project Manager</option>
                                                    <option value="">Designer</option>
                                                </select>
                                </div>
                            </div>
                            <div class="form-group border rounded p-4">
                                <p class="futura-book mb-2">Address</p>
                                <div class="row no-gutters">
                                    <div class="col-sm-12 col-lg-12 no-gutters">
                                        <div class="form-group form-inline col-sm-12 col-lg-12 mb-3">
                                            <input type="text" class="form-control col-sm-12 col-lg-5 mb-1 mr-2" id="" aria-describedby="" placeholder="ADDRESS 1">
                                            <input type="text" class="form-control col-sm-12 col-lg-5 mb-1 mr-2" id="" aria-describedby="" placeholder="ADDRESS 2">
                                        </div>
                                        <div class="form-group col-lg-12 form-inline mb-3">
                                            <input type="text" class="form-control col-sm-12 col-lg-5 mb-1 mr-2" id="" aria-describedby="" placeholder="CITY">
                                            <input type="text" class="form-control col-sm-12 col-lg-5 mb-1 mr-2" id="" aria-describedby="" placeholder="STATE">
                                        </div>
                                        <div class="form-group col-lg-12 form-inline mb-3">
                                            <input type="text" class="form-control col-sm-12 col-lg-2 mb-1 mr-2" id="" aria-describedby="" placeholder="POSTCODE">
                                            <input type="text" class="form-control col-sm-12 col-lg-8 mb-1 mr-2" id="" aria-describedby="" placeholder="COUNTRY">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group border p-4 rounded">
                                <p class="futura-book mb-2">Please select interest</p>
                                <div class="row">
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Windows Central</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                        <label class="custom-control-label" for="customCheck2">Polygon</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck3">
                                        <label class="custom-control-label" for="customCheck3">Kotaku</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck4">
                                        <label class="custom-control-label" for="customCheck4">Gizmodo</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck5">
                                        <label class="custom-control-label" for="customCheck5">Engadget</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck6">
                                        <label class="custom-control-label" for="customCheck6">Sebenarnya.my</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck7">
                                        <label class="custom-control-label" for="customCheck7">Engadget</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck8">
                                        <label class="custom-control-label" for="customCheck8">Win Beta</label>
                                    </div>
                                    <div class="custom-control custom-checkbox m-2">
                                        <input type="checkbox" class="custom-control-input" id="customCheck9">
                                        <label class="custom-control-label" for="customCheck9">Tifo Arena</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group border rounded p-4">
                                <p class="futura-book mb-2">Profile Picture</p>
                                <div class="row justify-content-sm-center justify-content-lg-start align-items-center">
                                    <div class="col-lg-4 col-md-12 p-3 ">
                                        <div class="row mb-3 justify-content-center">
                                            <img src="img/default_user.png" class="img-fluid" />
                                        </div>
                                        <div class="row justify-content-center">
                                            <a href="javascript:;" class="btn btn-blue btn-block btn-sm m-1">Browse Image</a>

                                        </div>
                                        <div class="row justify-content-center">
                                            <a href="javascript:;" class="btn btn-red btn-block btn-sm px-3 m-1">Remove Image</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <a href="vms_user_list.html" type="submit" class="btn btn-green btn-sm">Add</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('js') @parent @endsection