@extends('layouts.admin.master') @section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1 class="">PROJECT CREATED</h1>
            <h3 class="futura-heavy">List</h3>
            <p>Note: Nak guna table sama dgn page project list pun boleh</p>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                <i class="fa fa-fw fa-clipboard-check"></i> PROJECT CREATED
            </div>
            <div class="card-body">
                <div class="row">
                    <p class="futura-heavy">List of project created by [username] </p>
                </div>
                <div class="row no-gutters">
                    <div class="col-sm-12 mt-4 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col" style="width: 17%">Project Title</th>
                                    <th scope="col" style="width: 50%">Description</th>
                                    <th scope="col" style="width: 20%">Date Start-End</th>
                                    <th scope="col">Participation</th>
                                    <th scope="col">Type</th>
                                    <th scope="col" style="width: 11%">Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>
                                        <a href="javascript:;" class="blue-text">MarkYou bet</a> by
                                        <a href="javascript:;" class="blue-text">[organiser]</a>
                                    </td>
                                    <td>
                                        <p class="dotdotdot-text">OttoNot a word, not a word, not a word now. Quiet, uh, donations, you want me to make a donation to the coast guard youth auxiliary? (he pulls the suction cup off his forehead) Doc, I'm from the future. I came here
                                            in a time machine that you invented. Now, I need your help to get back to the year 1985. (grabs Marty's shoulders) My god, do you know what this means? It means that this damn thing doesn't work at all. (takes
                                            off the contraption.) Doc, you gotta help me. you were the only one who knows how your time machine works. Time machine, I haven't invented any time machine.</p>
                                    </td>
                                    <td>12/3/2018 - 23/3/2018</td>
                                    <td>10/30</td>
                                    <td>Event</td>
                                    <td>2018-03-03 02:30:00</td>
                                </tr>
                                <tr>
                                    <th scope="row">2</th>
                                    <td><a href="javascript:;" class="blue-text">Jacob</a> by
                                        <a href="javascript:;" class="blue-text">[organiser]</a>
                                    </td>
                                    <td>
                                        <p class="dotdotdot-text">You damned fool! (The two get out of the car.) Never, never leave this book lying around! Don't you have a safe? (realizes) No, you don't have a safe. Get a safe! Keep it locked up, and until then keep it on you
                                            like this. (He puts the book in 1955 Biff's back jeans pocket.) Hey, what're ya doing? And don't tell anyone about it either. Oh, and there's one more thing. (During the following 1955 Biff closes the garage
                                            door, and the Biffs leave the garage.) One day, a kid, or a crazy wild-eyed scientist who claims to be a scientist is gonna come around asking about that book... (They walk off, continuing their conversation.
                                            Marty tries to open the door - but it's locked! And we see the outside padlock to prove it. Marty looks up at the windows, but they're far too small for him to climb out of.) I'm trapped! (He gets his walkie-talkie
                                            out.) Doc, come in Doc!</p>
                                    </td>
                                    <td>12/3/2018 - 23/3/2018</td>
                                    <td>10/30</td>
                                    <td>Vounteer Project</td>
                                    <td>2018-03-03 02:30:00</td>
                                </tr>
                                <tr>
                                    <th scope="row">3</th>
                                    <td><a href="javascript:;" class="blue-text">Larry</a> by
                                        <a href="javascript:;" class="blue-text">[organiser]</a>

                                    </td>
                                    <td>
                                        <p class="dotdotdot-text">I'll take a one way ticket. (Saloon) (Marty is trying to get Doc to drink the coffee, but it's not working.) You want to sober him up in a hurry, son, you're gonna have to use something a lot stronger than coffee.
                                            Yeah, what do you suggest?</p>
                                    </td>
                                    <td>12/3/2018 - 23/3/2018</td>
                                    <td>10/30</td>
                                    <td>Event</td>
                                    <td>2018-03-03 02:30:00</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Pagination -->
                <div class="row">
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                    <span class="sr-only">Previous</span>
                                </a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection @section('js') @parent @endsection