@extends('layouts.admin.master') @section('content')
<!-- Volunteer Stats -->
<div class="container">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1>Volunteer</h1>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                VOLUNTEER STATISTIC
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-sm-12 col-lg-3">
                        <div class="card bg-primary text-white">
                            <div class="card-body">
                                <div class="col-sm-12 p-0">
                                    <p class="mb-2 bold">TOTAL VOLUNTEER</p>
                                </div>
                                <div class="col-sm-12 p-0">
                                    <h2 class="card-title">21462</h2>
                                    <p class="mb-0">Since 2016</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-9">
                        <div class="card">
                            <div class="card-body d-flex flex-wrap">
                                <div class="col-sm-12 p-0">
                                    <p class="mb-2 bold">New Registration</p>
                                </div>
                                <div class="col-sm-3 p-0">

                                    <h2 class="card-title">154</h2>
                                    <p class="mb-0">February 2018</p>
                                </div>
                                <div class="col-sm-3 p-0">
                                    <h2 class="card-title">5</h2>
                                    <p class="mb-0">Yesterday</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="canvas-holder" style="width:40%">
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end -->

<!-- Project Stats -->
<div class="container mt-5">
    <div class="row">
        <div class="col-sm-12 p-0 mb-3">
            <h1>Project</h1>
        </div>
    </div>
    <div class="row no-gutters">
        <div class="card col-sm-12">
            <div class="card-header bold">
                VOLUNTEER STATISTIC
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-sm-12 col-lg-3">
                        <div class="card bg-primary text-white">
                            <div class="card-body">
                                <div class="col-sm-12 p-0">
                                    <p class="mb-2 bold">TOTAL PROJECTS</p>
                                </div>
                                <div class="col-sm-12 p-0">
                                    <h2 class="card-title">3021</h2>
                                    <p class="mb-0">Since 2016</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-9">
                        <div class="card">
                            <div class="card-body d-flex flex-wrap">
                                <div class="col-sm-12 p-0">
                                    <p class="mb-2 bold">New Project Created</p>
                                </div>
                                <div class="col-sm-3 p-0">

                                    <h2 class="card-title">22</h2>
                                    <p class="mb-0">February 2018</p>
                                </div>
                                <div class="col-sm-2 p-0">
                                    <h2 class="card-title">1</h2>
                                    <p class="mb-0">Yesterday</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end -->

@endsection @section('js') @parent @endsection