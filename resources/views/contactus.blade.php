@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/contact/contactbg.jpg)">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">CONTACT US</h1>
            <h5 class="white-text text-shadow">Contact us for any inquiries of feedback.</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  get in touch  -->
<div id="getin" class="container-fluid p-0 bluesea-bg pt-5 pb-5">
    <div class="container">
        <div class="row mt-4">
            <div class="col-sm-12 col-md-6 col-lg-6 p-2 m-0">
                <div class="contact-box rounded border white-bg pb-4 pt-3">
                    <div class="row mt-4">
                        <div class="col-sm-4 offset-sm-4 col-lg-3 offset-lg-4">
                            <img src="img/contact/line.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center mt-4">
                            <h4 class="red-text mb-3">General Line</h4>
                            <a class="blue-text">+603 8064 4488</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6 p-2 m-0">
                <div class="contact-box rounded border white-bg pb-4 pt-3">
                    <div class="row mt-4">
                        <div class="col-sm-4 offset-sm-4 col-lg-3 offset-lg-4">
                            <img src="img/contact/address.png" class="img-fluid m-3" />
                        </div>
                        <div class="col-sm-12 text-center mt-4">
                            <h4 class="red-text mb-3">Address</h4>
                            <a class="blue-text">Rubix, Jalan TPP 1/7 <br> Taman Perindustrian Puchong <br> 47100 Selangor</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pb-5">
                <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                    <div class="contact-box rounded border white-bg pb-4 pt-3">
                        <div class="row mt-5">
                            <div class="col-sm-4 offset-sm-4 col-lg-6 offset-lg-2">
                                <img src="img/contact/brand.png" class="img-fluid m-3" />
                            </div>
                            <div class="col-sm-12 text-center mt-4">
                                <h4 class="red-text mb-3">Branding & PR</h4>
                                <a class="blue-text" href="mailto:bmg@volunteermalaysia.my" style="font-size: 10pt">bmg@volunteermalaysia.my</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                        <div class="contact-box rounded border white-bg pb-4 pt-3">
                            <div class="row mt-5">
                                <div class="col-sm-4 offset-sm-4 col-lg-6 offset-lg-2">
                                    <img src="img/contact/csd.png" class="img-fluid m-3" />
                                </div>
                                <div class="col-sm-12 text-center mt-4">
                                    <h4 class="red-text mb-3">Corporate Services</h4>
                                    <a class="blue-text" href="mailto:csd@volunteermalaysia.my" style="font-size: 10pt">csd@volunteermalaysia.my</a>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                        <div class="contact-box rounded border white-bg pb-4 pt-3">
                            <div class="row mt-5">
                                <div class="col-sm-4 offset-sm-4 col-lg-6 offset-lg-2">
                                    <img src="img/contact/outreach.png" class="img-fluid m-3" />
                                </div>
                                <div class="col-sm-12 text-center mt-4">
                                    <h4 class="red-text mb-3">Outreach</h4>
                                    <a class="blue-text text-center" href="mailto:outreach@volunteermalaysia.my" style="font-size: 10pt">outreach@volunteermalaysia.my</a>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-3 p-2 m-0">
                        <div class="contact-box rounded border white-bg pb-4 pt-3">
                            <div class="row mt-5">
                                <div class="col-sm-4 offset-sm-4 col-lg-6 offset-lg-2">
                                    <img src="img/contact/hr.png" class="img-fluid m-3" />
                                </div>
                                <div class="col-sm-12 text-center mt-4">
                                    <h4 class="red-text mb-3">Human Resources</h4>
                                    <a class="blue-text" href="mailto:hr@volunteermalaysia.my" style="font-size: 10pt">hr@volunteermalaysia.my</a>
                                </div>
                            </div>
                        </div>
                </div>
        </div>
    </div>
</div>
<!--  end  -->


@endsection @section('js') @parent @endsection