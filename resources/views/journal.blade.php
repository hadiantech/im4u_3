@extends('layouts.front.master') @section('content')
<!--  header image      -->
<div class="container-fluid p-0 ">
    <div class="row header-img align-items-center justify-content-center" style="background-image: url(/img/journal/background.jpg); background-position:bottom;">
        <div class="col-md-12 text-center">
            <h1 class="white-text text-shadow">THE PEOPLE'S JOURNAL</h1>
            <h5 class="white-text text-shadow">Unique, well-told stories/essays/write-ups shared by the community!</h5>
        </div>
    </div>
</div>
<!--  end  -->

<!--  featured  -->
@if($featured)
<div id="featured" class="container-fluid p-0 pink-bg pt-5 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 mb-3">
                <h1 class="text-white">ARTICLE OF THE MONTH</h1>
            </div>
        </div>
        <div class="row no-gutters" style="max-height:800px; overflow:hidden">
            <div class="col-md-8">
                <img src="{{ $featured->getMedia('journals')->first()->getUrl() }}" class="img-fluid rounded-left" style="width:100%; height:100%"  />
            </div>
            <div class="col-md-4 white-bg rounded-right">
                <div class="row pb-3 pt-5 pl-4 pr-4">
                    <p class="bold blue-text">{{ Carbon\Carbon::parse($featured->createad_at)->format('F Y') }}</p>
                </div>
                <div class="row pr-4 pl-4 pb-2">
                    <h3 class="dotdotdot-title mb-3" style="overflow:hidden">{{ $featured->title }}</h3>
                </div>
                <div class="row pt-1 pr-4 pl-4 pb-3">
                    <p class="dotdotdot">{{ $featured->content }}</p>
                </div>
                <div class="row p-4">
                    <div class="col-md-12">
                        <a href="{{ route('journals.show', $featured->name) }}" class="btn btn-blue btn-block">READ STORY</a>
                    </div>
                    
                </div>
                
            </div>
        </div>
    </div>
</div>
@endif
<!--  end  -->


<!-- tpj  -->
@if(!$journals->isEmpty())
<div id="tpj" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container">
        <div class="row">
            <h1 class="blue-text">THE PEOPLE'S JOURNAL</h1>
        </div>
        <div class="row p-3">
            
            <div class="slick-people-journal">
                @foreach($journals as $journal)
                <div class="pj-box col-sm-12 col-md-12 d-flex flex-wrap justify-content-center border rounded p-0 m-2 box-shadow white-bg">
                    <div class="col-sm-12 img" style="background-image: url({{ $journal->getMedia('journals')->first()->getUrl() }}); background-size: cover; background-repeat: no-repeat; background-position: center; height:200px"></div>
                    <div class="text-center" style="max-height:150px; overflow:hidden">   
                        <h5 class="font-weight-bold pt-4 pl-4 pr-4 pb-3 d-inline-block text-truncate" style="overflow:hidden;
                        min-height: 60px; max-height: 120px; max-width: 250px">{{ $journal->title }}</h5>
                        <div class="row justify-content-center mb-3"><a href="{{ route('journals.show', $journal->name) }}" class="btn btn-blue mb-2">READ STORY</a></div>
                    </div>
                    
                </div>
                @endforeach
            </div>
           
        </div>
    </div>
</div>
@endif
<!--  end  -->

{{-- Call to hantar article --}}
<div id="share" class="container-fluid white-bg pt-5 pb-5">
    <div class="container">
        <div class="row p-4 mx-2 mt-4 white-bg">
            <div class="col-md-10">
                <div class="row">
                    <div class="col-sm-1b2">
                        <h1 class="pink-text mb-4">HAVE GOOD STORIES TO SHARE?</h1>
                        <h3 class="blue-text mb-4">Submit Your Articles</h3>
                    </div>
                    <div class="col-sm-12 pr-5 pl-0 blue-text">
                        <p>The theme for this month is <span class="pink-text bold">"{{ $theme->title or ' ' }}"</span>.</p>
                        <p>We are looking for unique, well-told stories/essays/write-ups that you would like to share! We are open to receiving stories in various formats from graphic essays, photo essays, personal opinion pieces
                            to even comedic articles. There is no strict word count, but the 800 to 1200 word range would be preferable. Both Malay & English articles are accepted so submit your stories today!</p>
                        <br/>
                        
                        <p class="mb-3">What you need to do is donwload the article form then submit below</p>
                        <a href="/tpj_form.docx" download="TPJ Form" class="btn btn-pink-o m-2" style="width:160px">DOWNLOAD</a>
                        <a href="mailto:BMG@volunteermalaysia.my" target="_blank" class="btn btn-blue m-2" style="width:160px">SUBMIT</a>
                    </div>
                </div>
            </div>
            <div class="col-md-2 align-self-center">
                <img src="{{asset('img/journal/articleimage.png')}}" class="img-responsive" style="height:200px;" />
            </div>
            
        </div>
    </div>
</div>

{{-- Archive --}}
<div id="archive" class="container-fluid gray-bg pt-5 pb-5">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <h1 class="blue-text">THE PEOPLE'S JOURNAL ARCHIVE</h1>
            <a href="{{route('journal.archive')}}" class="pink-text bold mb-0">VIEW ARCHIVES <i class="fas fa-arrow-alt-circle-right"></i></a>
        </div>
        <div class="row mt-4">
            @php
            $now = Carbon\Carbon::now();
            $this_month = $now->format('n');
            if($this_month < 4){
                $month = 1;
            }else if($this_month < 7){
                $month = 4;
            }else if($this_month < 10){
                $month = 7;
            }else{
                $month = 10;
            }

            $diff_month = $this_month - $month;
            @endphp
            
            @for($i = '0'; $i < 4; $i++)   
            
            @php
                $j = $diff_month + ($i*3);
                $loop_date = Carbon\Carbon::now()->startOfMonth();
            @endphp

            <div class="col-md-3">
                <div class="journal-cont white-bg p-2 rounded">
                    <div class="row justify-content-center">
                        <div class="col-md-12 text-center py-5" style="background-color:#9BF0E1;">
                            <h1 class="blue-text m-0">
                                {{ $loop_date->subMonths($j)->format('M') }} - {{ $loop_date->addMonths(2)->format('M') }}
                                <br/>
                                {{ $loop_date->format('Y') }}
                            </h1>
                        </div>
                    </div>
                    <div class="row justify-content-center text-center">
                        <div class="col-md-12 p-4">
                            <a href="{{ route('journal.archive', ['year' => Carbon\Carbon::now()->subMonth($i)->format('Y'), 'month' => Carbon\Carbon::now()->subMonth($j)->format('n')]) }}" class="btn btn-blue">VIEW ARCHIVE</a>
                        </div>
                    </div>
                </div>
            </div>
            @endfor
        </div>
    </div>
</div>



@endsection 
@section('js') 
@parent 
<script>
    $(document).ready(function () {
        $(".dotdotdot").dotdotdot({

            ellipsis: "\u2026",
            height: 150,
            truncate: "word",

        });

         $(".dotdotdot-title-tpj").dotdotdot({

        ellipsis: "\u2026 ",
        truncate: "word",

        });

        $(".dotdotdot-title").dotdotdot({

        ellipsis: "\u2026 ",
        height: 100,
        truncate: "word",

        });
});

</script>
@endsection